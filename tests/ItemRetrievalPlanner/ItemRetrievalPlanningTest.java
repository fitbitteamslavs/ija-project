package ItemRetrievalPlanner;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.RequestPart;
import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.goods.GoodsItem;
import com.ija.warehouse.business.map.*;
import com.ija.warehouse.business.planning.*;
import com.ija.warehouse.business.utils.PlanningUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class ItemRetrievalPlanningTest {
    public BasicWarehouseMap warehouseMap;
    public ForkliftRoadPlanner planner;

    public Goods goodsA;
    public Goods goodsB;
    public Goods goodsC;

    @Before
    public void initializeBasicWarehouse() {
        this.goodsA = new Goods("A");
        this.goodsB = new Goods("B");
        this.goodsC = new Goods("C");

        this.warehouseMap = new BasicWarehouseMap(4,5);

        Road bottomLeft = Road.withMultipleDirections(new Coordinates(0, 3), RoadDirection.E, RoadDirection.N);
        this.warehouseMap.placeBlock(bottomLeft);

        this.warehouseMap.placeBlock(Road.withMultipleDirections(new Coordinates(0, 2), RoadDirection.N, RoadDirection.S));
        this.warehouseMap.placeBlock(Road.withMultipleDirections(new Coordinates(0, 1), RoadDirection.N, RoadDirection.S));

        Road topLeft = Road.withMultipleDirections(new Coordinates(0, 0), RoadDirection.E, RoadDirection.S);
        this.warehouseMap.placeBlock(topLeft);

        this.warehouseMap.placeBlock(Road.withMultipleDirections(new Coordinates(1, 0), RoadDirection.E, RoadDirection.W));
        this.warehouseMap.placeBlock(Road.withMultipleDirections(new Coordinates(2, 0), RoadDirection.E, RoadDirection.W));

        Road topRight = Road.withMultipleDirections(new Coordinates(3, 0), RoadDirection.S, RoadDirection.W);
        this.warehouseMap.placeBlock(topRight);

        this.warehouseMap.placeBlock(Road.withMultipleDirections(new Coordinates(3, 1), RoadDirection.S, RoadDirection.N));
        this.warehouseMap.placeBlock(Road.withMultipleDirections(new Coordinates(3, 2), RoadDirection.S, RoadDirection.N));

        Road bottomRight = Road.withMultipleDirections(new Coordinates(3, 3), RoadDirection.W, RoadDirection.N);
        this.warehouseMap.placeBlock(bottomRight);

        this.warehouseMap.placeBlock(Road.withMultipleDirections(new Coordinates(2, 3), RoadDirection.E, RoadDirection.W));
        this.warehouseMap.placeBlock(Road.withMultipleDirections(new Coordinates(1, 3), RoadDirection.E, RoadDirection.W));

        ItemStorageBlock rack1 = new Rack(new Coordinates(1, 1), 5);
        this.warehouseMap.placeStorageBlock(rack1);

        ItemStorageBlock rack2 = new Rack(new Coordinates(2, 1), 5);
        this.warehouseMap.placeStorageBlock(rack2);

        ItemStorageBlock rack3 = new Rack(new Coordinates(1, 2), 5);
        this.warehouseMap.placeStorageBlock(rack3);

        ItemStorageBlock rack4 = new Rack(new Coordinates(2, 2), 5);
        this.warehouseMap.placeStorageBlock(rack4);

        // Will be contained: R3=1ks, R2=1ks
        Goods goodsA = new Goods("A");
        GoodsItem jam1 = goodsA.createNewItem();
        rack3.storeItem(jam1);
        GoodsItem jam2 = goodsA.createNewItem();
        rack2.storeItem(jam2);

        // Will be contained: R1=3ks, R4=2ks
        Goods goodsB = new Goods("B");
        for (int i = 0; i < 3; i++) rack1.storeItem(goodsB.createNewItem());
        for (int i = 0; i < 2; i++) rack4.storeItem(goodsB.createNewItem());

        // Will be contained: R1=2ks, R4=2ks
        Goods goodsC = new Goods("C");
        for (int i = 0; i < 2; i++) rack1.storeItem(goodsC.createNewItem());
        for (int i = 0; i < 2; i++) rack4.storeItem(goodsC.createNewItem());

        this.warehouseMap.placeItemExportBlock(new OutputPort(new Coordinates(0, 4)));

        this.planner = new ForkliftRoadPlanner(this.warehouseMap, new ArrayList<>());
    }
    @Test
    public void basicTest() {
        BasicWarehouseMap m = this.warehouseMap;

        var goodsARequest = new RequestPart(goodsA, 2);
        var goodsBRequest = new RequestPart(goodsB, 2);
        var goodsCRequest = new RequestPart(goodsC, 1);
        var requests = Arrays.asList(goodsARequest, goodsBRequest, goodsCRequest);

        Forklift lift = new Forklift(10, new Coordinates(0, 3), 19);
        this.planner.addAvailableForklift(lift);

        var createdPlan = planner.createGoodsRetrievalPlan(requests);
        Assert.assertNotNull("The created retrieval plan should not be null", createdPlan);
        Assert.assertTrue("The count of stops should be at least 3 (to get Goods A + return).", createdPlan.getStopCount() > 2);

        ForkliftPlanStop lastStop = createdPlan.getStops().get(createdPlan.getStopCount() - 1);
        MapBlock maybeExportBlock = lastStop.getTargetBlock();
        Assert.assertTrue("The last stop should be an item export block.", maybeExportBlock instanceof ItemExportBlock);

        Assert.assertTrue("The last stop action should be to unload items.", lastStop.getAction() instanceof ForkliftUnloadAllAction);

        var stopCount = createdPlan.getStopCount();
        HashMap<Goods, Integer> overallRetrievedItems = new HashMap<>();
        createdPlan.getStops().subList(0, stopCount - 1).forEach(forkliftPlanStop -> {
            var action = forkliftPlanStop.getAction();
            Assert.assertTrue("All actions except the last one are Load actions.", action instanceof ForkliftLoadFromStorageBlockAction);
            var loadAction = (ForkliftLoadFromStorageBlockAction) action;
            var itemsToLoad = loadAction.getItemsToLoad();

            // Create sum of all retrieved items.
            itemsToLoad.forEach((currentStopGoods, currentStopAmount) -> {
                Assert.assertTrue("Every action at stop should load a positive amount of goods into the forklift", currentStopAmount > 0);
                if (overallRetrievedItems.containsKey(currentStopGoods)) {
                    var actualAmount = overallRetrievedItems.get(currentStopGoods);
                    overallRetrievedItems.put(currentStopGoods, actualAmount + currentStopAmount);
                } else {
                    overallRetrievedItems.put(currentStopGoods, currentStopAmount);
                }
            });
        });

        Assert.assertEquals("We should collect the items we requested.", overallRetrievedItems.get(goodsA).intValue(), 2);
        Assert.assertEquals("We should collect the items we requested.", overallRetrievedItems.get(goodsB).intValue(), 2);
        Assert.assertEquals("We should collect the items we requested.", overallRetrievedItems.get(goodsC).intValue(), 1);

        PlanningUtils.visualizeCreatedPlan(m, createdPlan);
    }

    @Test
    public void testLowCapacityForkliftRetrieval() {
        Forklift forklift = new Forklift(2, new Coordinates(0, 3), 10);
        this.planner.addAvailableForklift(forklift);

        var goodsARequest = new RequestPart(goodsA, 2);
        var goodsBRequest = new RequestPart(goodsB, 2);
        var goodsCRequest = new RequestPart(goodsC, 1);
        var requests = Arrays.asList(goodsARequest, goodsBRequest, goodsCRequest);

        var createdPlan = planner.createGoodsRetrievalPlan(requests);

        long unloadStops = createdPlan.getStops().stream()
                .filter(stop -> stop.getAction() instanceof ForkliftUnloadAllAction)
                .count();

        Map<Goods, Integer> overallItems = new HashMap<>();
        createdPlan.getStops().stream()
                .filter(stop -> stop.getAction() instanceof ForkliftLoadFromStorageBlockAction)
                .forEach(stop -> {
                    var action = (ForkliftLoadFromStorageBlockAction) stop.getAction();
                    action.getItemsToLoad().forEach((goods, integer) -> {
                        if (overallItems.containsKey(goods)) overallItems.put(goods, overallItems.get(goods) + integer);
                        else overallItems.put(goods, integer);
                    });
                });

        // Retrieving 5 items, cart has the capacity 2 --> 3 stops
        Assert.assertEquals( 3, unloadStops);

        Assert.assertEquals(2, (long) overallItems.get(this.goodsA));
        Assert.assertEquals(2, (long) overallItems.get(this.goodsB));
        Assert.assertEquals(1, (long) overallItems.get(this.goodsC));

        // Go through the paths and assert that they are continuous.
        this.assertPlanPathsContinuous(createdPlan);
    }

    @Test
    public void testRequestsCannotBeSatisfied() {
        Forklift forklift = new Forklift(10, new Coordinates(0, 3), 10);
        this.planner.addAvailableForklift(forklift);

        var requestA = new RequestPart(this.goodsA, 2);
        var requestB = new  RequestPart(this.goodsB, 10);
        var plan = this.planner.createGoodsRetrievalPlan(Arrays.asList(requestA, requestB));
        Assert.assertNull(plan);
    }

    @Test
    public void testRequestCannotBeSatisfiedAfterAnotherRequest() {
        Forklift forklift1 = new Forklift(10, new Coordinates(0, 3), 10);
        Forklift forklift2 = new Forklift(10, new Coordinates(0, 3), 11);
        this.planner.addAvailableForklift(forklift1);
        this.planner.addAvailableForklift(forklift2);

        // There are 5 items of goods B
        var requestB = new RequestPart(this.goodsB, 4);
        var plan = this.planner.createGoodsRetrievalPlan(Arrays.asList(requestB));
        Assert.assertNotNull(plan);

        // There is 1 item of goods B remaining - the rest should be reserved to the plan created before.
        plan = this.planner.createGoodsRetrievalPlan(Arrays.asList(requestB));
        Assert.assertNull(plan);
    }

    @Test
    public void testCreatePlanUnavailableForklift() {
        var requestB = new RequestPart(this.goodsB, 4);
        var plan = this.planner.createGoodsRetrievalPlan(Arrays.asList(requestB));
        Assert.assertNull(plan);
    }

    @Test
    public void testObstacleRePlanning__ObstacleDoesNotChangePlan() {
        /* Map:    Place obstacle on (3, 4), so some must be rerouted.
        rrrr    rrrr
        rRRr    rRRr
        rRRr    rRRr
        rrrr    rrrX   <--- obstacle
        E       E
        */

        Forklift lift = new Forklift(10, new Coordinates(0, 3), 10);
        planner.addAvailableForklift(lift);
        var goodsARequest = new RequestPart(goodsA, 2);
        var goodsBRequest = new RequestPart(goodsB, 2);
        var goodsCRequest = new RequestPart(goodsC, 1);

        AbstractForkliftPlan plan = planner.createGoodsRetrievalPlan(Arrays.asList(goodsARequest, goodsBRequest, goodsCRequest));
        Assert.assertNotNull(plan);

        System.out.println("Created plan before obstacle:");
        PlanningUtils.visualizeCreatedPlan(this.warehouseMap, plan);

        var road = (Road) warehouseMap.getBlockAtPosition(new Coordinates(3, 3));
        road.placeObstacle();

        plan = planner.adaptPlan(plan, new Coordinates(3, 3));
        Assert.assertNotNull(plan);

        System.out.println("Created plan after obstacle:");
        PlanningUtils.visualizeCreatedPlan(this.warehouseMap, plan);
    }

    @Test
    public void testObstacleRePlanning__ObstacleAffectsPlan() {
        /* Map:    Place obstacle on (3, 4), so some must be rerouted.
        rrrr    rrrr
        rRRr    XRRr   <--- obstacle
        rRRr    rRRr
        rrrr    rrrr
        E       E
        */

        Forklift lift = new Forklift(10, new Coordinates(0, 3), 10);
        planner.addAvailableForklift(lift);

        var goodsARequest = new RequestPart(goodsA, 2);
        var goodsBRequest = new RequestPart(goodsB, 2);
        var goodsCRequest = new RequestPart(goodsC, 1);

        var plan = planner.createGoodsRetrievalPlan(Arrays.asList(goodsARequest, goodsBRequest, goodsCRequest));
        Assert.assertNotNull(plan);

        System.out.println("Created plan before obstacle:");
        PlanningUtils.visualizeCreatedPlan(this.warehouseMap, plan);

        var road = (Road) warehouseMap.getBlockAtPosition(new Coordinates(0, 1));
        road.placeObstacle();

        var replacementPlan = planner.adaptPlan(plan, new Coordinates(0, 1));
        Assert.assertNotNull(replacementPlan);

        System.out.println("Created plan after obstacle:");
        PlanningUtils.visualizeCreatedPlan(this.warehouseMap, plan);

        // This holds only because the cart has not made any loads/unloads
        // Otherwise the returned plan would only contain Goods that have not been yet received by the cart
        Assert.assertTrue(
                areRequestItemListsEqual(plan.getOverallProcessedItems(), replacementPlan.getOverallProcessedItems()));
    }

    @Test
    public void testObstacleRePlanning__ForkliftInMovement() {
        Forklift lift = new Forklift(10, new Coordinates(0, 3), 10);
        planner.addAvailableForklift(lift);

        var goodsARequest = new RequestPart(goodsA, 2);
        var goodsBRequest = new RequestPart(goodsB, 2);
        var goodsCRequest = new RequestPart(goodsC, 1);

        var plan = planner.createGoodsRetrievalPlan(Arrays.asList(goodsARequest, goodsBRequest, goodsCRequest));
        Assert.assertNotNull(plan);

        // Move the cart to one stop after.
        forkliftSimulateWholeStop(plan);
    }

    public static void assertPlanPathsContinuous(AbstractForkliftPlan plan) {
        Coordinates coordsBefore = null;
        for (var stop : plan.getStops()) {
            for (Coordinates c : stop.getPathFromLastStop()) {
                if (coordsBefore == null) coordsBefore = c;

                int deltaX = c.getX() - coordsBefore.getX();
                int deltaY = c.getY() - coordsBefore.getY();
                Assert.assertTrue(Math.abs(deltaX) + Math.abs(deltaY) <= 1);

                coordsBefore = c;
            }
        }
    }

    private boolean areRequestItemListsEqual(Map<Goods, Integer> a, Map<Goods, Integer> b) {
        if (a.size() != b.size()) return false;
        for (Map.Entry<Goods, Integer> aEntry : a.entrySet()) {
            Goods aGoods = aEntry.getKey();
            Integer aAmount = aEntry.getValue();

            if (!b.containsKey(aGoods)) return false;
            if (!b.get(aGoods).equals(aAmount)) return false;
        }

        return true;
    }

    private void forkliftSimulateWholeStop(ForkliftGoodsRetrievalPlan plan) {
        var actualStop = plan.getCurrentStop();
        var lift = plan.getAssociatedForklift();
        for (var c : actualStop.getPathFromLastStop()) {
            lift.setPosition(c);
        }
        Assert.assertEquals(lift.getActualPosition(), actualStop.getStopCoordinates());

        var rack = (Rack) actualStop.getTargetBlock();
        var action = actualStop.getAction();

        int itemsInRackBeforeRetrieval = rack.getAvailableItems().values().stream().mapToInt(List::size).sum();
        action.perform(lift, actualStop.getTargetBlock());

        int retrievedItems = lift.getInventoryItemCount();
        int itemsInRackAfterRetrieval = rack.getAvailableItems().values().stream().mapToInt(List::size).sum();
        Assert.assertEquals((long) retrievedItems + itemsInRackAfterRetrieval, itemsInRackBeforeRetrieval);

        plan.getNextStop();
    }

}
