package ItemRetrievalPlanner;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.map.*;
import com.ija.warehouse.business.planning.*;
import com.ija.warehouse.business.utils.PlanningUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TestGoodsSupplyPlan {
    BasicWarehouseMap map;
    Goods goodsA;
    Goods goodsB;
    Goods goodsC;

    ForkliftRoadPlanner planner;

    @Before
    public void createMap() {
        map = new BasicWarehouseMap(4, 3);
        /* ------- R=rack, r=road, I=importBlock
          RRRR
          rrrr
          rrrI
         */

        for (int y = 1; y < 3; y++) {
            for (int x = 0; x < 4; x++) {
                Road road = Road.newOmnidirectional(new Coordinates(x, y));
                map.placeBlock(road);
            }
        }

        for (int x = 0; x < 4; x++) {
            Rack rack = new Rack(new Coordinates(x, 0), 5);
            rack.setMaxCapacityOfEachShelf(1);
            map.placeStorageBlock(rack);
        }

        map.placeBlock(new InputPort(new Coordinates(3, 2)));

        goodsA = new Goods("Goods A");
        goodsB = new Goods("Goods B");
        goodsC = new Goods("Goods C");

        planner = new ForkliftRoadPlanner(map, new ArrayList<>());
    }

    @Test
    public void testGoodsSupplyPlanCreation() {
        Forklift forklift = new Forklift(3, new Coordinates(0, 2), 10);
        // Every rack has exactly one item of goods A
        this.planner.addAvailableForklift(forklift);
        for (int x = 0; x < 4; x++) {
            Rack rack = (Rack) this.map.getBlockAtPosition(new Coordinates(x, 0));
            rack.storeItem(goodsA.createNewItem());
        }

        Map<Goods, Integer> requestSummary = new HashMap<>();
        requestSummary.put(goodsA, 5);

        InputPort inputPort = (InputPort) map.getBlockAtPosition(new Coordinates(3, 2));
        var plan = planner.createGoodsResupplyPlan(requestSummary, inputPort, forklift, null);

        Assert.assertNotNull(plan);
        ItemRetrievalPlanningTest.assertPlanPathsContinuous(plan);

        PlanningUtils.visualizeCreatedPlan(map, plan);

        Map<Goods, Integer> suppliesDescription = new HashMap<>();
        suppliesDescription.put(goodsA, 5);
        assertAllRequestedItemsDistributed(plan, suppliesDescription);
        assertAllSuppliedItemsLoadedIntoForklift(plan, suppliesDescription);
    }

    public static void assertAllRequestedItemsDistributed(ForkliftPlan plan, Map<Goods, Integer> suppliedItems) {
        var maybeRetrievedItems = plan.getStops().stream()
                .filter(forkliftPlanStop -> forkliftPlanStop.getAction() instanceof ForkliftInsertItemsIntoStorageAction)
                .map(stop -> (ForkliftInsertItemsIntoStorageAction) stop.getAction())
                .map(ItemTransferAction::getTransferedItems)
                .reduce(PlanningUtils::sumRequests);

        Assert.assertTrue(maybeRetrievedItems.isPresent());
        var retrievedItems = maybeRetrievedItems.get();
        Assert.assertEquals(retrievedItems.size(), suppliedItems.size());

        retrievedItems.forEach((goods, integer) -> {
            Assert.assertTrue(suppliedItems.containsKey(goods));
            Assert.assertEquals(suppliedItems.get(goods), integer);
        });

    }

    public static void assertAllSuppliedItemsLoadedIntoForklift(ForkliftPlan plan, Map<Goods, Integer> suppliedItems) {
        var maybeRetrievedItems = plan.getStops().stream()
                .filter(forkliftPlanStop -> forkliftPlanStop.getAction() instanceof ForkliftLoadFromStorageBlockAction)
                .map(stop -> (ForkliftLoadFromStorageBlockAction) stop.getAction())
                .map(ItemTransferAction::getTransferedItems)
                .reduce(PlanningUtils::sumRequests);

        Assert.assertTrue(maybeRetrievedItems.isPresent());
        var retrievedItems = maybeRetrievedItems.get();
        Assert.assertEquals(retrievedItems.size(), suppliedItems.size());

        retrievedItems.forEach((goods, integer) -> {
            Assert.assertTrue(suppliedItems.containsKey(goods));
            Assert.assertEquals(suppliedItems.get(goods), integer);
        });
    }
}
