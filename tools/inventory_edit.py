import PySimpleGUI as sg
import xml.etree.ElementTree as ET
import os
from xml.dom import minidom


class InventoryEditor:
    map = ""
    item_list = []
    total_items = 0
    rack_list = []
    total_rack_shelves = 0
    shelf_capacity = 0

    # Change some colors here
    bg_color = "#3a3a3a"
    text_color = "#fff"
    button_color = ("#000", "#349012")

    def __init__(self):
        """
        Creates the main window and initiates the main loop.
        """
        layout = self.create_layout()
        self.window = sg.Window("Inventory editor", background_color=self.bg_color, button_color=self.button_color).Layout(layout)
        self.event_loop()
        self.window.close()

    def event_loop(self):
        """
        Main application loop
        """
        while True:
            event, values = self.window.read()
            if event == "Import racks":
                # Check if map is file
                map_path = values["import"]
                if not os.path.isfile(map_path):
                    self.display_message("Invalid map path!",type="E")
                    continue

                # Check if map wasn't defined already
                if self.map != "":
                    self.display_message("Reimporting racks!", type="W")
                    self.rack_list = []
                    self.total_rack_shelves = 0

                # Import racks from the map
                self.import_racks(map_path)
                self.map = map_path
                self.update_racks_status(status="YES")

            elif event == "Reset":
                self.map = ""
                self.item_list = []
                self.total_items = 0
                self.rack_list = []
                self.total_rack_shelves = 0
                self.shelf_capacity = 0
                self.update_racks_status(status="NO ")
                self.display_message("Items list reset\nRack list reset")

            elif event == "Add item types":
                items_string = values["item_list"]
                self.shelf_capacity = int(values["capacity"])
                if ";" not in items_string:
                    self.display_message("Specify item types!", type="E")
                    continue
                self.add_items(items_string)
            elif event == "Generate XML":
                print("Total item types: " + str(len(self.item_list)))
                print("Total items: " + str(self.total_items))
                print("Totoal racks: " + str(len(self.rack_list)))
                print("Total shelves: " + str(self.total_rack_shelves))
                self.generate_inventory()
            elif event == sg.WIN_CLOSED:
                break

    def add_items(self, items_string):
        """
        Adds parsed items from item_string to the self.items_list. Also counts the total items in self.total_items.
        :param items_string: String containing array of item types separated by ";" item types are
                             represented by attributes separated by ", ".
                             Syntax of single item type:"NAMEOFTHEITEMTYPE, QUANTITY, MANUFACTURER, OPTIONALDESCRITION;"
        """
        print(items_string)
        items_string = items_string.replace("\n", "")
        items_list = items_string.split(";")[:-1]

        line = 0
        for item_type in items_list:
            line += 1
            item_type = item_type.split(", ")
            item_type[1] = int(item_type[1])
            if len(item_type) == 3:
                item_type.append("")
            elif len(item_type) != 4:
                self.display_message(f"Too many item attributes on line={line}!", type="E")
                self.item_list = []
                break
            self.total_items += item_type[1]
            self.item_list.append(item_type)

    def generate_inventory(self):
        """
        Disaster
        """
        if self.map == "" or self.rack_list == [] or self.total_rack_shelves == 0:
            self.display_message("Import valid map first!", type="E")
            return
        if self.total_items == 0 or not self.item_list:
            self.display_message("Add some item types first!", type="E")
            return

        # Starts creation of xml
        root = ET.Element("inventory")

        # Fill the xml with item types list as sub-element of element goods
        goods = ET.SubElement(root, "goods")
        for item_type in self.item_list:
            item_type_id = self.item_list.index(item_type)
            ET.SubElement(goods, "item_type", { "name": str(item_type[0]),
                                                "id": str(item_type_id),
                                                "quantity": str(item_type[1]),
                                                "manufacturer": str(item_type[2]),
                                                "description": str(item_type[3])})

        # Add rack contents to the xml
        racks_contents = ET.SubElement(root, "racks", {"shelf_capacity": str(self.shelf_capacity)})

        for rack in self.rack_list:
            # Create rack element
            rack_element = ET.SubElement(racks_contents, "rack", {"x": str(rack[1]),
                                                                  "y": str(rack[2]),
                                                                  "height": str(rack[0])})

            if len(self.item_list) != 0:
                # Add items to shelves of this element (as children)
                for i in range(1, rack[0]+1):
                    shelf_cap = self.shelf_capacity
                    if len(self.item_list) == 0:
                        break
                    while shelf_cap > 0:
                        if len(self.item_list) == 0:
                            self.save_xml(root)
                            break
                        item_type = self.item_list[-1]
                        current_id = self.item_list.index(item_type)

                        if item_type[1] <= shelf_cap: # shelf can hold more than this item_type provides
                            shelf_cap -= item_type[1]
                            quantity = item_type[1]
                            self.item_list = self.item_list[:-1]
                        else: # shelf cant hod that much so fill the shelf
                            item_type[1] -= shelf_cap
                            quantity = shelf_cap
                            shelf_cap = 0

                        ET.SubElement(rack_element, "item_type", {"id": str(current_id),
                                                                  "quantity": str(quantity),
                                                                  "shelf": str(i)})
        if len(self.item_list) != 0:
            print("The shelf capacity is too low for specified warehouse and item quantities!")
            self.display_message("Insufficient capacity of the Warehouse! Please increase the capacity of shelves or "
                                 "remove some items.", type="E")
        self.save_xml(root)


    def save_xml(self, root):
        rough_string = ET.tostring(root, 'utf-8')
        xmlstr = minidom.parseString(rough_string).toprettyxml(indent="  ")
        print(xmlstr)
        with open(self.map[:-4]+"_inventory.xml", "w") as f:
            f.write(xmlstr)
        print("Saved the file!")

    def import_racks(self, path):
        """
        Given path of a warehouse map, stores racks form it in the self.racks_list

        :param path: path to xml file containing map with racks
        """
        map_tree = ET.parse(path)
        map_root = map_tree.getroot()

        for element in map_root:
            if element.tag == "rack":
                rack_height = int(element.attrib["height"])
                rack_x = int(element.attrib["x"])
                rack_y = int(element.attrib["y"])
                rack = [rack_height, rack_x, rack_y]

                self.rack_list.append(rack)
                self.total_rack_shelves += rack_height

    def create_layout(self):
        """
        Creates the main gui layout of the application.

        :return layout: nested array of simpleGui elements
        """
        button_column = [[sg.Button("Add item types", size=(19, 1),tooltip="Stores item types. [rewrites]")],
                         [sg.Button("Reset", size=(19, 1), tooltip="Voids item types, and imported racks!")],
                         [sg.Button("Generate XML", size=(19, 1), tooltip="Generates Warehouse Inventory XML in the same folder as the selected map.")],
                         [sg.HorizontalSeparator(pad=(0, 30))],
                         [sg.Text("How to use this thing:", background_color=self.bg_color, text_color=self.text_color)],
                         [sg.Text("1. Import racks from a map\n"
                                  "2. Specify the item types\n"
                                  "3. Specify shelf capacity\n"
                                  "4. Add item types\n"
                                  "5. Generate XML\n"
                                  "6. Profit!", background_color=self.bg_color, text_color=self.text_color)]]

        layout = [[sg.Text("Array of item types:\nNAME, QUANTITY, MANUFACTURER, (DESCRIPTION);", background_color=self.bg_color, text_color=self.text_color),
                   sg.VerticalSeparator(pad=(0,0), color="#000"), sg.Text("Shelf capacity:", background_color=self.bg_color, text_color=self.text_color),
                   sg.Input(key="capacity", size=(10, 1)), sg.VerticalSeparator(pad=(0, 0), color= "#000"),
                   sg.Text("Map path:", background_color=self.bg_color, text_color=self.text_color), sg.Input(key="import", size=(28, 1)),
                   sg.Button("Import racks", size=(19, 1), tooltip="Given a Warehouse map, imports its racks."),
                   sg.Text("Imported: No ", key="-RACKSIMPORT-", background_color=self.bg_color, size=(30,1))],
                  [sg.Multiline(key="item_list", size=(99, 20), font=("Arial", 14)), sg.Column(button_column, background_color=self.bg_color)]]

        return layout

    def update_racks_status(self, status):
        self.window["-RACKSIMPORT-"].update(value=f"Imported: {status}")

    def display_message(self, msg, type=""):
        """
        Creates new window with modified layout that contains the message,
        since the pySimpleGui library doesn't support dynamic changes.

        :param msg: string containing a message one wants to display
        """
        color = self.text_color
        if type == "E":
            msg = f"[E]: {msg}"
            color = "#fb4934"
        elif type == "W":
            msg = f"[W]: {msg}"
            color = "#fabd2f"

        location = list(self.window.CurrentLocation())
        location[0] += 500
        location[1] += 150

        layout = [[sg.Text(msg, text_color=color, background_color=self.bg_color)],
                  [sg.Button("OK", button_color=self.button_color)]]
        msg_window = sg.Window("Message", location=tuple(location), background_color=self.bg_color).Layout(layout)

        while True:
            event, values = msg_window.read()
            if event == "OK" or event == sg.WIN_CLOSED:
                break
        msg_window.close()


InventoryEditor()

