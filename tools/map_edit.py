#!/bin/python3
import tkinter as tk
import logging
import math
from typing import Tuple, Dict, Any, List, Optional
from dataclasses import dataclass
from enum import Enum, IntFlag
import functools
import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom
import tkinter.filedialog as tkf


class CellType(Enum):
    NONE = 'Blue'
    WALL = 'Red'
    RACK = 'Gray'
    PATH = 'Green'
    PORT = 'Orange'
    CART = 'Brown'


class PathDirection(IntFlag):
    NONE = 0x00
    NORTH = 0x01
    WEST = 0x02
    SOUTH = 0x04
    EAST = 0x08


class PortType(IntFlag):
    NONE = 0x00
    OUT = 0x01
    IN = 0x02


@dataclass
class CellContents:
    type: CellType
    canvas_handle: List[int]
    extra_info: Any = None


class EditorUI():
    def __init__(self, window: tk.Tk, grid_size: int = 20):
        self.window = window
        self.grid_size = grid_size
        self.curr_cell_type = CellType.NONE
        self.path_direction_chk_btn_vals: Dict[PathDirection, tk.IntVar] = {}
        self.port_type_chk_btn_vals: Dict[PathDirection, tk.IntVar] = {}
        self.map_cells: Dict[Tuple[int, int], CellContents] = {}
        self.path_direction = PathDirection.NORTH
        self.selected_cells: List[Tuple[int, int]] = []
        self.in_selection = False
        self.map_bounding_box_handle: Optional[int] = None
        self.port_type: PortType = PortType.IN
        self.rack_height: int = 4
        self.racks: Dict[Tuple[int, int], int] = {}

        self.opened_filename: Optional[str] = None

        self.setup_ui(window)

    def change_grid_size(self, grid_size: int):
        self.grid_size = grid_size

    def redraw_grid(self):
        self.canvas.delete('all')

        width = int(self.canvas.winfo_width())
        height = int(self.canvas.winfo_height())

        for x in range(self.grid_size, width, self.grid_size):
            self.canvas.create_line((x, 0), (x, height), fill='black')

        for y in range(self.grid_size, height, self.grid_size):
            self.canvas.create_line((0, y), (width, y), fill='black')

        for cell_pos in self.map_cells:
            r, c = cell_pos
            cell_type = self.map_cells[cell_pos].type
            rect_left_top = (r * self.grid_size, c * self.grid_size)
            rect_right_bottom = (rect_left_top[0] + self.grid_size, rect_left_top[1] + self.grid_size)

            handle = self.canvas.create_rectangle(rect_left_top, rect_right_bottom, fill=cell_type.value)
            self.map_cells[cell_pos].canvas_handle = [handle]

    def notify_grid_size_changed(self):
        try:
            new_grid_size = int(self.grid_size_entry.get())
            self.grid_size = new_grid_size
            self.redraw_grid()
        except ValueError:
            logging.warn('Requesting grid size change to a non int value!')

    def setup_ui(self, window: tk.Tk):
        window.title('IPDK IJA Map edit')

        window.columnconfigure(0, weight=1)
        window.columnconfigure(1, weight=0)
        window.rowconfigure(0, weight=1)

        self.canvas = tk.Canvas(window, bg='white')
        self.canvas.grid(column=0, row=0, sticky='NSEW')
        self.canvas.rowconfigure(0, weight=1)

        self.canvas.bind('<Configure>', lambda event: self.redraw_grid())

        self.tool_column = tk.Frame(window, width=300)
        self.tool_column.grid(column=1, row=0, sticky='NSEW')

        self.tool_column.rowconfigure(0, weight=1)
        self.tool_column.columnconfigure(0, weight=1)

        grid_info_frame = tk.Frame(self.tool_column)
        self.grid_size_label = tk.Label(grid_info_frame,
                                        text='Grid size:')
        self.grid_size_label.pack(side=tk.LEFT)
        self.grid_size_entry = tk.Entry(grid_info_frame)
        self.grid_size_entry.pack(side=tk.LEFT, anchor=tk.N)
        self.grid_size_set_button = tk.Button(grid_info_frame,
                                              text='Set',
                                              command=self.notify_grid_size_changed)
        self.grid_size_set_button.pack(side=tk.LEFT)
        grid_info_frame.pack(side=tk.TOP, pady=10)

        self.setup_cell_type_selection(self.tool_column)

        self.setup_port_type_selection()
        self.setup_rack_height_selection()
        self.setup_map_info_and_actions()

        self.canvas.bind('<Button-1>',
                         self.left_mouse_btn_pressed)
        self.canvas.bind('<B1-Motion>',
                         self.left_mouse_btn_pressed_motion)
        self.canvas.bind('<ButtonRelease-1>',
                         self.left_mouse_btn_released)

        window.bind('<Delete>', lambda event: self.remove_selection())

        self.canvas.bind('<B3-Motion>', self.canvas_remove_cell)
        self.canvas.bind('<Button-3>', self.canvas_remove_cell)

    def calc_cell_coords_from_event(self, e: tk.Event) -> Tuple[int, int]:
        return self.calc_cell_coords_from_xy(e.x, e.y)

    def calc_cell_coords_from_xy(self, x: int, y: int) -> Tuple[int, int]:
        cell_x = math.floor(x / self.grid_size)
        cell_y = math.floor(y / self.grid_size)
        return (cell_x, cell_y)

    def left_mouse_btn_pressed(self, event: tk.Event):
        if self.curr_cell_type == CellType.NONE:
            self.in_selection = True
            self.selection_start = (event.x, event.y)
            self.selection_rectangle = self.canvas.create_rectangle(event.x,
                                                                    event.y,
                                                                    event.x + 1,
                                                                    event.y + 1,
                                                                    outline='blue')
        else:
            cell_x, cell_y = self.calc_cell_coords_from_event(event)
            self.place_cell(cell_x, cell_y)

    def left_mouse_btn_pressed_motion(self, event: tk.Event):
        if self.in_selection:
            self.canvas.coords(self.selection_rectangle,
                               self.selection_start[0],
                               self.selection_start[1],
                               event.x,
                               event.y)
        else:
            r, c = self.calc_cell_coords_from_event(event)
            self.place_cell(r, c)

    def left_mouse_btn_released(self, event: tk.Event):
        if self.in_selection:
            self.in_selection = False
            self.canvas.delete(self.selection_rectangle)
            self.highlight_selected_cells(event)

    def place_cell(self, r: int, c: int, cell_type=None):
        if cell_type is None:
            cell_type = self.curr_cell_type

        cell_pos = (r, c)
        if self.curr_cell_type == CellType.NONE:
            return

        if cell_pos in self.map_cells:
            self.canvas.delete(self.map_cells[cell_pos].canvas_handle)

        if self.curr_cell_type == CellType.PATH:
            self.draw_path_cell(r, c, self.path_direction)
        elif self.curr_cell_type == CellType.PORT:
            self.draw_port_cell(r, c, self.port_type)
        else:
            rect_left_top = (r * self.grid_size, c * self.grid_size)
            rect_right_bottom = (rect_left_top[0] + self.grid_size, rect_left_top[1] + self.grid_size)

            handle = self.canvas.create_rectangle(rect_left_top, rect_right_bottom, fill=cell_type.value)
            handles = [handle]

            if self.curr_cell_type == CellType.RACK:
                t = self.canvas.create_text(
                    rect_left_top[0] + self.grid_size // 2,
                    rect_left_top[1] + self.grid_size // 2,
                    text=f'{self.rack_height}'
                )
                self.racks[(r, c)] = self.rack_height
                handles.append(t)

            self.map_cells[cell_pos] = CellContents(cell_type, handles)

        self.update_map_info()
        self.update_bounding_box()

    def draw_path_cell(self, r: int, c: int, direction: PathDirection):
        top_left = (r * self.grid_size, c * self.grid_size)
        handles = self.render_path_cell(top_left,
                                        (self.grid_size, self.grid_size),
                                        self.path_direction,
                                        self.canvas)
        self.map_cells[(r, c)] = CellContents(CellType.PATH, handles, self.path_direction)

    def draw_port_cell(self, r: int, c: int, port_type: PortType):
        top_left = (r * self.grid_size, c * self.grid_size)
        handles = self.render_port_cell(top_left,
                                        (self.grid_size, self.grid_size),
                                        port_type,
                                        self.canvas)
        self.map_cells[(r, c)] = CellContents(CellType.PORT, handles, self.port_type)

    def render_path_cell(self,
                         top_left: Tuple[int, int],
                         cell_dimensions: Tuple[int, int],
                         path_direction: PathDirection,
                         canvas: tk.Canvas
                         ) -> List[int]:

        cell_width, cell_height = cell_dimensions

        cell_center = (top_left[0] + cell_width // 2,
                       top_left[1] + cell_height // 2)

        bottom_right = (top_left[0] + cell_width,
                        top_left[1] + cell_height)

        rect_handle = canvas.create_rectangle(top_left, bottom_right, fill=CellType.PATH.value)

        arrow_heads = {
            PathDirection.NORTH: ((+0, -1), (-1, +1), (+1, +1)),
            PathDirection.EAST:  ((+1, +0), (-1, -1), (-1, +1)),
            PathDirection.SOUTH: ((+0, +1), (-1, -1), (+1, -1)),
            PathDirection.WEST:  ((-1, +0), (+1, -1), (+1, +1))
        }

        half_height = cell_height // 2
        half_width = cell_width // 2

        def draw_arrow(arrow_info, arrowhead_size):
            tip, arrow0, arrow1 = arrow_info
            arrow_tip = (cell_center[0] + tip[0]*(half_width),
                         cell_center[1] + tip[1]*(half_height))

            handle0 = canvas.create_line(cell_center,
                                         arrow_tip,
                                         width=arrowhead_size // 4,
                                         fill='yellow')

            arrow_start0 = (arrow_tip[0] + arrow0[0] * arrowhead_size,
                            arrow_tip[1] + arrow0[1] * arrowhead_size)

            arrow_start1 = (arrow_tip[0] + arrow1[0] * arrowhead_size,
                            arrow_tip[1] + arrow1[1] * arrowhead_size)

            handle1 = canvas.create_line(arrow_start0,
                                         arrow_tip,
                                         width=arrowhead_size // 4,
                                         fill='yellow')

            handle2 = canvas.create_line(arrow_start1,
                                         arrow_tip,
                                         width=arrowhead_size // 4,
                                         fill='yellow')

            return [handle0, handle1, handle2]

        path_handles: List[int] = [rect_handle]
        for possible_direction in arrow_heads:
            if (possible_direction & path_direction):
                if possible_direction == PathDirection.NORTH or possible_direction == PathDirection.SOUTH:
                    arrowhead_size = cell_height // 5
                else:
                    arrowhead_size = cell_width // 5
                handles = draw_arrow(arrow_heads[possible_direction], arrowhead_size)
                path_handles.append(handles)
        return path_handles

    def render_port_cell(self, top_left: Tuple[int, int],
                         cell_dimensions: Tuple[int, int], port_type: PortType,
                         canvas: tk.Canvas) -> List[int]:

        bottom_right = (top_left[0] + cell_dimensions[0], top_left[1] + cell_dimensions[1])

        rect_handle = canvas.create_rectangle(top_left, bottom_right, fill='Yellow')
        handles = [rect_handle]

        cell_center = (top_left[0] + cell_dimensions[0] // 2, top_left[1] + cell_dimensions[1] // 2)

        if (port_type & PortType.IN) and (port_type & PortType.OUT):
            handle = canvas.create_text(cell_center, text='I/O')
            handles.append(handle)
        elif port_type & (PortType.IN):
            handle = canvas.create_text(cell_center, text='I')
            handles.append(handle)
        elif port_type & (PortType.OUT):
            handle = canvas.create_text(cell_center, text='O')
            handles.append(handle)

        return handles

    def canvas_remove_cell(self, e: tk.Event):
        cell_pos = self.calc_cell_coords_from_event(e)
        if cell_pos in self.map_cells:
            if cell_pos in self.selected_cells:
                self.selected_cells.remove(cell_pos)
            cell_contents = self.map_cells[cell_pos]
            for handle in cell_contents.canvas_handle:
                self.canvas.delete(handle)
            del self.map_cells[cell_pos]

    def remove_selection(self):
        for cell in self.selected_cells:
            for handle in self.map_cells[cell].canvas_handle:
                self.canvas.delete(handle)
            del self.map_cells[cell]
        self.selected_cells = []

    def setup_cell_type_selection(self, tool_column: tk.Frame):
        self.current_cell_type_label = tk.Label(self.tool_column, text=f'Current cell type: {self.curr_cell_type.name}')
        self.current_cell_type_label.pack(side=tk.TOP, anchor='w')

        label_select_tool = tk.Label(self.tool_column, text='Select tool:')
        label_select_tool.pack(side=tk.TOP, anchor='w')

        self.select_cell_type_frame = tk.Frame(tool_column)
        self.select_cell_type_frame.pack(side=tk.TOP, anchor=tk.W)

        for i, cell_type in enumerate(CellType):
            label = tk.Label(self.select_cell_type_frame, text=f'{cell_type.name}')

            btn = tk.Button(self.select_cell_type_frame,
                            bg=cell_type.value,
                            command=functools.partial(self.notify_cell_type_changed, cell_type))

            if i % 2:
                label.grid(column=0, row=i // 2, pady=5, padx=10)
                btn.grid(column=1, row=i // 2, pady=5)
            else:
                label.grid(column=2, row=i // 2, pady=5, padx=10)
                btn.grid(column=3, row=i // 2, pady=5)

        # SECTION - START: Path configuration
        path_dir_configuration_label = tk.Label(tool_column,
                                                text='Select path type:')
        path_dir_configuration_label.pack(side=tk.TOP, anchor='sw')

        path_dir_configuration_frame = tk.Frame(tool_column)
        path_dir_configuration_frame.pack(side=tk.TOP, anchor=tk.W, fill='x')
        path_dir_configuration_frame.columnconfigure(0, weight=0)
        path_dir_configuration_frame.columnconfigure(1, weight=1)

        for i, path_direction in enumerate(PathDirection):
            if (path_direction & self.path_direction):
                value = 1
            else:
                value = 0
            btn_var = tk.IntVar(value=value)
            chk_button = tk.Checkbutton(path_dir_configuration_frame,
                                        variable=btn_var,
                                        command=self.nofify_path_type_changed,
                                        text=f'{path_direction.name}'
                                        )

            self.path_direction_chk_btn_vals[path_direction] = btn_var
            chk_button.grid(row=i, column=0, sticky='W')

        self.path_preview_canvas = tk.Canvas(path_dir_configuration_frame, width=100, height=100, bg='red')
        self.path_preview_canvas.grid(row=0, rowspan=4, column=1)
        self.render_path_type_preview(self.path_preview_canvas)

    def setup_port_type_selection(self):
        tc = self.tool_column
        frame = tk.Frame(tc)
        frame.pack(side=tk.TOP, fill='x')
        label = tk.Label(frame, text='Select port type:')
        label.pack(side=tk.TOP)
        for port_type in PortType:
            var = tk.IntVar()
            if port_type & self.port_type:
                var.set(1)
            btn = tk.Checkbutton(frame,
                                 text=f'{port_type.name}',
                                 command=self.port_type_changed,
                                 variable=var)
            self.port_type_chk_btn_vals[port_type] = var

            btn.pack(side=tk.TOP, anchor=tk.W)

    def setup_rack_height_selection(self):
        tc = self.tool_column
        frame = tk.Frame(tc)
        frame.pack(side=tk.TOP, fill='x')
        label = tk.Label(frame, text='Rack height:')

        label.pack(side=tk.LEFT)

        entry_var = tk.StringVar(frame, str(self.rack_height))
        entry_var.trace('w', self.notify_rack_height_changed)
        entry = tk.Entry(frame, textvariable=entry_var)
        entry.pack(side=tk.RIGHT)
        self.rack_height_var = entry_var
        self.rack_height_entry = entry

        inc_btn = tk.Button(frame, text='+', command=functools.partial(self.add_to_rack_heihgt, 1))
        dec_btn = tk.Button(frame, text='-', command=functools.partial(self.add_to_rack_heihgt, -1))
        inc_btn.pack(side=tk.RIGHT)
        dec_btn.pack(side=tk.RIGHT)

    def notify_rack_height_changed(self, *args):
        new_value: str = self.rack_height_var.get()
        valid = True
        for c in new_value:
            if not c.isdigit():
                valid = False
        if valid:
            self.rack_height = int(new_value)
            self.rack_height_entry.configure(bg='white')
        else:
            self.rack_height_entry.configure(bg='red')

    def add_to_rack_heihgt(self, mod: int):
        self.rack_height += mod
        self.rack_height_var.set(str(self.rack_height))

    def port_type_changed(self):
        new_port_type = PortType.NONE
        for port_type, port_var in self.port_type_chk_btn_vals.items():
            if port_var.get() == 1:
                new_port_type |= port_type
        self.port_type = new_port_type

    def setup_map_info_and_actions(self):
        tc = self.tool_column

        root_frame = tk.Frame(tc)
        root_frame.pack(side=tk.TOP, fill='x')

        section_label = tk.Label(root_frame, text='Map info:')
        section_label.pack(side=tk.TOP)

        info_frame = tk.Frame(root_frame)
        info_frame.pack(side=tk.TOP, fill='x')
        info_frame.columnconfigure(1, weight=1)

        cell_count_label = tk.Label(info_frame, text='Cell count:')
        cell_count_label.grid(row=0, column=0)
        self.tile_count_info = tk.Label(info_frame, text=f'{len(self.map_cells)}')
        self.tile_count_info.grid(row=0, column=1)

        map_width_label = tk.Label(info_frame, text='Map width:')
        map_width_label.grid(row=1, column=0)
        self.map_width_info = tk.Label(info_frame, text='0')
        self.map_width_info.grid(row=1, column=1)

        map_height_label = tk.Label(info_frame, text='Map height:')
        map_height_label.grid(row=2, column=0)
        self.map_height_info = tk.Label(info_frame, text='0')
        self.map_height_info.grid(row=2, column=1)

        actions_frame = tk.Frame(root_frame)
        actions_frame.pack(side=tk.TOP, fill='x')

        clear_map_btn = tk.Button(actions_frame,
                                  text='Clear map',
                                  command=self.clear_map)
        clear_map_btn.pack(side=tk.TOP, fill='x')

        self.display_bounding_box = tk.IntVar()

        display_bounding_box_btn = tk.Checkbutton(
            actions_frame,
            text='Calc. bounding box.',
            variable=self.display_bounding_box,
            command=self.update_bounding_box)
        display_bounding_box_btn.pack(side=tk.TOP, fill='x')

        export_btn = tk.Button(actions_frame,
                               text='Export/Save Map',
                               command=self.export_to_xml)
        export_btn.pack(side=tk.TOP, fill='x')

    def render_path_type_preview(self, canvas: tk.Canvas):
        w = canvas.winfo_reqwidth()
        h = canvas.winfo_reqheight()
        self.render_path_cell((0, 0), (w, h), self.path_direction, canvas)

    def notify_cell_type_changed(self, new_type: CellType):
        self.curr_cell_type = new_type
        self.current_cell_type_label.configure(text=f'Current cell type: {self.curr_cell_type.name}')

    def nofify_path_type_changed(self):
        new_direction: PathDirection = PathDirection.NONE
        for direction, btn_val in self.path_direction_chk_btn_vals.items():
            if btn_val.get() == 1:
                new_direction |= direction

        self.path_direction = new_direction

        self.path_preview_canvas.delete('all')
        self.render_path_type_preview(self.path_preview_canvas)

    def clear_map(self):
        self.map_cells: Dict[Tuple[int, int], CellContents] = {}
        self.redraw_grid()
        self.selected_cells = []
        self.map_bounding_box_handle = None
        self.update_map_info()

    def update_map_info(self):
        self.tile_count_info.config(text=f'{len(self.map_cells)}')

        _min, _max = self.get_map_bounding_box_coordinates()
        width = _max[0] - _min[0]
        height = _max[1] - _min[1]

        self.map_width_info.config(text=f'{width}')
        self.map_height_info.config(text=f'{height}')

    def highlight_cell(self, cell):
        box_paddind = 2
        hl_box_top_left = (cell[0] * self.grid_size + box_paddind,
                           cell[1] * self.grid_size + box_paddind)

        hl_box_bottom_right = (hl_box_top_left[0] + self.grid_size - 2*box_paddind,
                               hl_box_top_left[1] + self.grid_size - 2*box_paddind)

        handle = self.canvas.create_rectangle(hl_box_top_left,
                                              hl_box_bottom_right,
                                              outline='orange',
                                              width=2)

        self.map_cells[cell].canvas_handle.append(handle)

    def unhighlight_cell(self, cell):
        self.canvas.delete(self.map_cells[cell].canvas_handle[-1])

    def highlight_selected_cells(self, selection_end: tk.Event):
        for cell in self.selected_cells:
            self.unhighlight_cell(cell)
        self.selected_cells = []

        start_coords = self.calc_cell_coords_from_xy(*self.selection_start)
        end_coords = self.calc_cell_coords_from_event(selection_end)

        for x in range(start_coords[0], end_coords[0] + 1):
            for y in range(start_coords[1], end_coords[1] + 1):
                if (x, y) in self.map_cells:
                    self.selected_cells.append((x, y))
                    self.highlight_cell((x, y))

    def update_bounding_box(self):
        if self.display_bounding_box.get() == 1:
            self.redraw_map_bounding_box()
        else:
            self.clear_bounding_box()

    def redraw_map_bounding_box(self):
        self.clear_bounding_box()

        # min_x, max_x, min_y, max_y
        _min, _max = self.get_map_bounding_box_coordinates()
        min_x, min_y = _min
        max_x, max_y = _max

        if _min == (0, 0) and _max == (0, 0):
            return

        top_left = (min_x * self.grid_size, min_y * self.grid_size)
        bottom_right = (max_x * self.grid_size + self.grid_size, max_y * self.grid_size + self.grid_size)

        handle = self.canvas.create_rectangle(top_left, bottom_right, outline='blue', width=3)
        self.map_bounding_box_handle = handle

    def get_map_dimensions(self) -> Tuple[int, int]:
        _min, _max = self.get_map_bounding_box_coordinates()
        min_x, min_y = _min
        max_x, max_y = _max

        return (max_x - min_x, max_y - min_y)

    def get_map_bounding_box_coordinates(self) -> Tuple[Tuple[int, int], Tuple[int, int]]:
        if len(self.map_cells) == 0:
            return ((0, 0), (0, 0))

        some_cell: Tuple[int, int] = tuple(self.map_cells.keys())[0]
        min_x, max_x = some_cell[0], some_cell[0]
        min_y, max_y = some_cell[1], some_cell[1]

        for cell_coords in self.map_cells:
            x, y = cell_coords

            if x < min_x:
                min_x = x
            elif x > max_x:
                max_x = x

            if y < min_y:
                min_y = y
            elif y > max_y:
                max_y = y

        return ((min_x, min_y), (max_x, max_y))

    def clear_bounding_box(self):
        if self.map_bounding_box_handle is not None:
            self.canvas.delete(self.map_bounding_box_handle)

    def export_to_xml(self):
        file = tkf.asksaveasfile()

        width, height = self.get_map_dimensions()
        _map = ET.Element('map', attrib={'width': str(width), 'height': str(height)})

        cell_type_to_tag_name: Dict[CellType, str] = {
            CellType.WALL: 'wall',
            CellType.PATH: 'pathway',
            CellType.RACK: 'rack',
            CellType.CART: 'cart',
            CellType.PORT: 'port',
        }

        path_dir_to_attr: Dict[PathDirection, str] = {
            PathDirection.NORTH: 'N',
            PathDirection.SOUTH: 'S',
            PathDirection.WEST: 'W',
            PathDirection.EAST: 'E',
        }

        port_type_to_attr: Dict[PortType, str] = {
            PortType.IN: 'in',
            PortType.OUT: 'out'
        }

        rack_id = 0  # ID counter for racks
        cart_id = 0  # ID counter for carts
        top_left, _ = self.get_map_bounding_box_coordinates()

        for cell_coords, cell_info in self.map_cells.items():
            x, y = cell_coords

            # Offset the map so that its stored starting at (0, 0)
            x -= top_left[0]
            y -= top_left[1]

            tag_name = cell_type_to_tag_name[cell_info.type]
            elem = ET.SubElement(_map, tag_name, attrib={'x': str(x), 'y': str(y)})

            if cell_info.type == CellType.RACK:
                elem.set('id', str(rack_id))
                height = self.racks[cell_coords]
                elem.set('height', str(height))
                rack_id += 1
            elif cell_info.type == CellType.CART:
                elem.set('id', str(cart_id))
                cart_id += 1
            elif cell_info.type == CellType.PATH:
                path_dir_attr = ''
                for path_direction in PathDirection:
                    if path_direction & cell_info.extra_info:
                        path_dir_attr += path_dir_to_attr[path_direction]
                elem.set('direction', path_dir_attr)
            elif cell_info.type == CellType.PORT:
                port_type_attr = ''
                for port_type in PortType:
                    if port_type & cell_info.extra_info:
                        port_type_attr += port_type_to_attr[port_type]
                elem.set('type', port_type_attr)

        file.write(minidom.parseString(ET.tostring(_map)).toprettyxml())

# @dataclass
# class CellContents:
#     type: CellType
#     canvas_handle: List[int]
#     extra_info: Any = None


window = tk.Tk()
ui = EditorUI(window)
window.mainloop()
