#! /bin/bash
if [ ! -f "model_importer.zip" ]; then
    wget -O model_importer.zip http://www.interactivemesh.org/models/download/JFX3DModelImporters_EA_2014-02-09.zip
fi
unzip -u -d . model_importer.zip jimObjModelImporterJFX.jar

if [ ! -f "openjfx.zip" ]; then
    wget -O openjfx.zip "https://download2.gluonhq.com/openjfx/17.0.13/openjfx-17.0.13_linux-x64_bin-sdk.zip"
fi
unzip -u -j -d . openjfx.zip 'javafx-sdk-17.0.13/lib/*'
