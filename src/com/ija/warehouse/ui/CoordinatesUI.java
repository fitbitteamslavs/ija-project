/*
Author: Michal Hečko (xhecko02)
Purpose: Helper class for keeping the structured information about a block position.
 */
package com.ija.warehouse.ui;

import java.util.Objects;

/**
 * Class representing coordinates for UI
 * (It is separate from the coordinates of backend)
 */
public class CoordinatesUI {
    private int x;
    private int y;

    /**
     * Creates coordinates with provided x and y
     */
    public CoordinatesUI(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CoordinatesUI that = (CoordinatesUI) o;
        return x == that.x && y == that.y;
    }

    @Override
    public String toString() {
        return "(" + "x=" + x + ", y=" + y + ')';
    }

    @Override
    public int hashCode() {

        return Objects.hash(x, y);
    }
}
