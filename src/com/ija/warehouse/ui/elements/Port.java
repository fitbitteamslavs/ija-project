/*
Author: Dávid Lacko (xlacko09)
Purpose: Port object manager
 */
package com.ija.warehouse.ui.elements;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.MeshView;

/** Represents port meshes */
public class Port extends AbstractWarehouseElement {
    /**
     * @param scene parent Group to add meshes to
     * @param x
     * @param y
     * @param type bitmap of values: 1 - Input and 2 - Output
     */
    public Port(Group scene, int x, int y, int type) {
        super(scene, x, y);

        MeshView platform = new MeshView(Port.platformMesh.getMesh());
        platform.setMaterial(Port.platformMesh.getMaterial());

        MeshView arrowUp = new MeshView(Port.arrowUpMesh.getMesh());
        arrowUp.setMaterial(Port.arrowUpMesh.getMaterial());

        MeshView arrowDown = new MeshView(Port.arrowDownMesh.getMesh());
        arrowDown.setMaterial(Port.arrowDownMesh.getMaterial());

        this.whole.getChildren().add(platform);
        if ((type & 1) > 0)  {
            this.whole.getChildren().add(arrowDown);
        }
        if ((type & 2) > 0) {
            this.whole.getChildren().add(arrowUp);
        }
    }

    /** Loads meshes for Port, should be called during loading. */
    public static void loadMeshes() {
        MeshView[] meshes = readObj("port.obj");
        Port.platformMesh = meshes[0];
        Port.arrowDownMesh = meshes[2];
        Port.arrowUpMesh = meshes[3];
        ((PhongMaterial)platformMesh.getMaterial()).setSpecularColor(Color.grayRgb(20));
    }

    private static MeshView platformMesh;
    private static MeshView arrowUpMesh;
    private static MeshView arrowDownMesh;
}
