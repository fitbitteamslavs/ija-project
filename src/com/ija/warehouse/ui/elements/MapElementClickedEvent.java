/*
Author: Dávid Lacko (xlacko09)
Purpose: Event when some object is clicked on the map
 */
package com.ija.warehouse.ui.elements;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/** Event used internally for click events on the map objects **/
public class MapElementClickedEvent extends ActionEvent {
    public static EventType<MapElementClickedEvent> FORKLIFT_CLICKED = new EventType<>(ActionEvent.ACTION ,"ForkliftClicked");
    public static EventType<MapElementClickedEvent> RACK_CLICKED = new EventType<>(ActionEvent.ACTION, "RackClicked");
    public static EventType<MapElementClickedEvent> ROAD_CLICKED = new EventType<>(ActionEvent.ACTION, "RoadClicked");

    private final AbstractWarehouseElement sourceElement;

    /** When caller provides coordinates for the receiver */
    public MapElementClickedEvent(EventType<MapElementClickedEvent> type, AbstractWarehouseElement source, EventTarget target, int x, int y) {
        this(type, source, target);
        this.x = x;
        this.y = y;
    }

    /** When caller provides ID of the clicked item to the receiver */
    public MapElementClickedEvent(EventType<MapElementClickedEvent> type, AbstractWarehouseElement source, EventTarget target, int id) {
        this(type, source, target);
        this.id = id;
    }

    private MapElementClickedEvent(EventType<MapElementClickedEvent> type, AbstractWarehouseElement source, EventTarget target) {
        super(source, target);
        this.sourceElement = source;
        this.eventType = type;
    }

    public AbstractWarehouseElement getSourceElement() {
        return this.sourceElement;
    }

    public int x;
    public int y;
    public int id;
}
