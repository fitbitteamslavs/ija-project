/*
Author: Dávid Lacko (xlacko09)
Purpose: Rack object manager
 */
package com.ija.warehouse.ui.elements;

import com.ija.warehouse.ui.HeatmapDisplay;
import javafx.beans.property.ObjectProperty;
import javafx.scene.Group;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.MeshView;

/** Represents meshes of the rack and operations on it within the GUI */
public class Rack extends AbstractWarehouseElement implements HeatmapDisplay {
    /**
     * @param scene parent group to add the meshes to
     * @param x
     * @param y
     */
    public Rack(Group scene, int x, int y) {
        super(scene, x, y);
        floors = new ArrayList<>();
        boxes = new ArrayList<>();

        this.whole.addEventHandler(MouseEvent.MOUSE_RELEASED, mouseEvent -> {
            System.out.printf("Clicked on rack with %d floors.", floors.size());
            MapElementClickedEvent.fireEvent(this.whole,
                    new MapElementClickedEvent(MapElementClickedEvent.RACK_CLICKED, this, scene, (int) this.whole.getTranslateX(), (int) this.whole.getTranslateZ()));
        });
    }

    /** Adds another floor to the rack */
    public void addFloor() {
        Group floor = new Group();

        MeshView legs = new MeshView(Rack.legs.getMesh());
        legs.setMaterial(Rack.legs.getMaterial());

        MeshView shelf = new MeshView(Rack.shelf.getMesh());
        shelf.setMaterial(Rack.shelf.getMaterial());

        this.floors.add(shelf);

        floor.setTranslateY(this.floors.size()-1);

        MeshView bx = new MeshView(Rack.box.getMesh());
        bx.setMaterial(Rack.box.getMaterial());
        bx.setVisible(false);

        floor.getChildren().addAll(legs, shelf, bx);
        this.boxes.add(bx);

        this.whole.getChildren().add(floor);
    }

    /** Adds multple floors.
     * @param num number of floors to add
     */
    public void addFloors(int num) {
        for(int i = 0; i < num; i++){
            this.addFloor();
        }
    }

    /** Sets the box representig the goods on given floor to visible or hidden
     * @param floor floor of the shelf
     * @param show
     */
    public void setGoods(int floor, boolean show) {
        if (floor < this.boxes.size()) {
            this.boxes.get(floor).setVisible(show);
        }
    }

    /** Loads necessary meshes for Rack, should be called during loading. */
    public static void loadMeshes() {
        MeshView[] parts = readObj("shelf.obj");
        Rack.legs = parts[0];
        Rack.shelf = parts[1];
        Rack.box = parts[2];
        ((PhongMaterial)Rack.shelf.getMaterial()).setSpecularColor(Color.grayRgb(50));
        ((PhongMaterial)Rack.shelf.getMaterial()).setDiffuseColor(Color.rgb(61, 61, 61));
        ((PhongMaterial)Rack.box.getMaterial()).setDiffuseColor(Color.rgb(76,32,16));
        ((PhongMaterial)Rack.legs.getMaterial()).setSpecularColor(Color.grayRgb(200));

        shelfMaterial = (PhongMaterial) Rack.shelf.getMaterial();
    }

    @Override
    public void enableHeatmap(PhongMaterial floorMaterial) {
        for (MeshView floor : this.floors) {
            floor.setMaterial(floorMaterial);
        }
    }

    @Override
    public void disableHeatmap() {
        for(MeshView m : this.floors) {
            m.setMaterial(shelfMaterial);
        }
    }

    private final ArrayList<MeshView> floors;
    private final ArrayList<MeshView> boxes;

    private static MeshView legs;
    private static MeshView shelf;
    private static MeshView box;
    private static PhongMaterial shelfMaterial;
}
