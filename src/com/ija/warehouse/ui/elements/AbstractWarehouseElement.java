/*
Author: Dávid Lacko (xlacko09)
Purpose: Base class for map objects.
 */
package com.ija.warehouse.ui.elements;

import com.interactivemesh.jfx.importer.obj.ObjModelImporter;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.MeshView;

import java.net.URL;

/** Represents Element that can be added to the map */
public abstract class AbstractWarehouseElement {
    /**
     * @param scene parent Group to add the meshes to
     * @param x
     * @param y
     */
    protected AbstractWarehouseElement(Group scene, int x, int y) {
        this.whole = new Group();
        scene.getChildren().add(this.whole);
        this.whole.setTranslateX(x);
        this.whole.setTranslateZ(y);
        this.whole.setPickOnBounds(true);
    }

    /**
     * Loads meshes from given file
     * @param name Name of the .obj file to laod
     * @return Array of loaded meshes with materials.
     */
    protected static MeshView[] readObj(String name) {
        ObjModelImporter importer = new ObjModelImporter();

        URL fl = AbstractWarehouseElement.class.getResource("/models/" + name);
        if (fl == null) {
            System.out.println("Cannot load resource: "+name);
            System.exit(1);
        }
        importer.read(fl);
        if (importer.getImport() == null) {
            System.out.println("Wrong obj file format");
            System.exit(1);
        }
        return importer.getImport();
    }

    /** Each child class must implement this to load it own meshes */
    public static void loadMeshes() {};

    /** Group that contains all Meshes that are part of the 3D object */
    protected Group whole;
}
