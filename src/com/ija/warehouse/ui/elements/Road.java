/*
Author: Dávid Lacko (xlacko09)
Purpose: Road object manager
 */
package com.ija.warehouse.ui.elements;

import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Material;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Mesh;
import javafx.scene.shape.MeshView;

/** Represents the road Element on the map.
 * It has direction arrows and the base black road mesh. */
public class Road extends AbstractWarehouseElement {
    /** Denotes possible directions that the road allows */
    public enum Directions {
        N(1),
        E(2),
        W(4),
        S(8);

        private final int dir;

        Directions(int dir) {
            this.dir = dir;
        }

        public int get() {
            return this.dir;
        }
    }

    /**
     * @param scene Parent group where the models will be added
     * @param directions All allowed diretions of the road
     * @param x coordinate
     * @param y coordinate
     * @see Directions
     */
    public Road(Group scene, int directions, int x, int y) {
        super(scene, x, y);

        baseRoad = new MeshView(Road.road.getMesh());
        baseRoad.setMaterial(Road.road.getMaterial());

        if ( (directions & Directions.N.dir) > 0) {
            MeshView N = new MeshView(Road.arrowN);
            N.setMaterial(Road.arrowMaterial);
            this.directions.getChildren().add(N);
        }
        if ( (directions & Directions.E.dir) > 0) {
            MeshView E = new MeshView(Road.arrowE);
            E.setMaterial(Road.arrowMaterial);
            this.directions.getChildren().add(E);
        }
        if ( (directions & Directions.S.dir) > 0) {
            MeshView S = new MeshView(Road.arrowS);
            S.setMaterial(Road.arrowMaterial);
            this.directions.getChildren().add(S);
        }
        if ( (directions & Directions.W.dir) > 0) {
            MeshView W = new MeshView(Road.arrowW);
            W.setMaterial(Road.arrowMaterial);
            this.directions.getChildren().add(W);
        }
        this.whole.getChildren().add(this.directions);

        this.whole.addEventHandler(MouseEvent.MOUSE_RELEASED, mouseEvent -> {
            MapElementClickedEvent.fireEvent(whole, new MapElementClickedEvent(MapElementClickedEvent.ROAD_CLICKED, this, scene,
                    (int) whole.getTranslateX(), (int) whole.getTranslateZ()));
        });

        this.whole.getChildren().add(baseRoad);
    }

    /**
     * Block the road (make its color red)
     */
    public void toggleBlocked() {
        this.directions.setVisible(blocked);
        this.baseRoad.setMaterial(blocked ? road.getMaterial() : blockedRoadMaterial);
        blocked = !blocked;
    }

    /** This loads necessary meshes for the road.
     * Should be called at least once at the start of the program.
     */
    public static void loadMeshes() {
        MeshView[] parts = readObj("road.obj");

        road = parts[0];
        arrowN = parts[1].getMesh();
        arrowW = parts[2].getMesh();
        arrowS = parts[3].getMesh();
        arrowE = parts[4].getMesh();
        arrowMaterial = parts[1].getMaterial();
        ((PhongMaterial)road.getMaterial()).setSpecularColor(Color.grayRgb(20));
    }

    private boolean blocked = false;
    private final Group directions = new Group();
    private final MeshView baseRoad;

    private static MeshView road;
    private static Mesh arrowN;
    private static Mesh arrowE;
    private static Mesh arrowS;
    private static Mesh arrowW;
    private static Material arrowMaterial;
    private static final Material blockedRoadMaterial = new PhongMaterial(Color.rgb(158, 30, 30));
}
