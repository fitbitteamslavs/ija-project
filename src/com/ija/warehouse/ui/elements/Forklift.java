/*
Author: Dávid Lacko (xlacko09)
Purpose: Forklift object manager
 */
package com.ija.warehouse.ui.elements;

import javafx.animation.*;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.MeshView;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

/** Represents forklift meshes and operations in the GUI*/
public class Forklift extends AbstractWarehouseElement {
    /**
     * @param scene parent Group to add meshes to
     * @param x
     * @param y
     */
    public Forklift(Group scene, int x, int y, int id) {
        super(scene,x,y);

        this.id = id;

        this.cargo = new MeshView();
        cargo.setMesh(cargoMesh.getMesh());
        cargo.setMaterial(cargoMesh.getMaterial());
        cargo.setOpacity(0);

        for(MeshView m : forkliftMesh){
            MeshView v = new MeshView();
            v.setMesh(m.getMesh());
            v.setMaterial(m.getMaterial());
            this.whole.getChildren().add(v);
        }

        Group fork = new Group();
        for (MeshView m : forkMesh) {
            MeshView v = new MeshView();
            v.setMesh(m.getMesh());
            v.setMaterial(m.getMaterial());
            fork.getChildren().add(v);
        }
        fork.getChildren().add(this.cargo);
        this.whole.getChildren().add(fork);

        this.whole.addEventHandler(MouseEvent.MOUSE_RELEASED, mouseEvent ->
                MapElementClickedEvent.fireEvent(whole,
                        new MapElementClickedEvent(MapElementClickedEvent.FORKLIFT_CLICKED, this, scene, this.id)));
        this.whole.setPickOnBounds(true);

        MeshView lift = new MeshView();
        lift.setMesh(liftMesh.getMesh());
        lift.setMaterial(liftMesh.getMaterial());

        this.whole.setRotationAxis(Rotate.Y_AXIS);

        this.whole.getChildren().add(lift);

        this.moveAnimation.setNode(this.whole);
        this.moveAnimation.setCycleCount(1);

        this.liftSizeAnimation.setAutoReverse(true);
        this.liftSizeAnimation.setCycleCount(2);
        this.liftSizeAnimation.setNode(lift);

        this.liftTranslateAnimation.setAutoReverse(true);
        this.liftTranslateAnimation.setCycleCount(2);
        this.liftTranslateAnimation.setNode(lift);

        this.forkTranslateAnimation.setAutoReverse(true);
        this.forkTranslateAnimation.setCycleCount(2);
        this.forkTranslateAnimation.setNode(fork);

        this.showBoxAnimation.setDuration(Duration.seconds(1));
        showBoxAnimation.setOnFinished(actionEvent -> setLoaded(true));

        this.allReachBoxAnimations = new ParallelTransition(this.liftSizeAnimation, this.liftTranslateAnimation, this.forkTranslateAnimation, showBoxAnimation);
        moveAnimation.setInterpolator(Interpolator.LINEAR);
    }

    /** Moves forklift in delta mode, meaning adding or substracting given values from current location */
    public void deltaMode(int x, int y) {
        move(x+(int)this.whole.getTranslateX(), (int)this.whole.getTranslateZ()+y);
    }

    /** Moves forklift in absolute/world coorinates
     * @return Manhattan distance moved
     */
    public int move(int x, int y) {
        int deltaX = (int) (x - this.whole.getTranslateX());
        int deltaY = (int) (y - this.whole.getTranslateZ());

        int totalMovementLength = Math.abs(deltaX) + Math.abs(deltaY);
        if (totalMovementLength > 1 || this.moveAnimation.getStatus() == Animation.Status.RUNNING) {
            this.moveAnimation.stop();
            this.whole.setTranslateX(x);
            this.whole.setTranslateZ(y);
            return totalMovementLength;
        }

        if (deltaX < 0) {
            this.whole.setRotate(0);
        } else if (deltaX > 0) {
            this.whole.setRotate(180);
        } else if (deltaY < 0) {
            this.whole.setRotate(270);
        } else if (deltaY > 0){
            this.whole.setRotate(90);
        }

        this.moveAnimation.setToX(x);
        this.moveAnimation.setToZ(y);
        this.moveAnimation.setDuration(Duration.seconds((totalMovementLength) *timeUnit));
        this.moveAnimation.play();
        return totalMovementLength;
    }

    /** Updates speed of animation */
    public void updateSpeed(double newSpeed) {
        this.allReachBoxAnimations.setRate(newSpeed);
        this.moveAnimation.setRate(newSpeed);
    }

    /** Starts animation of lifting the fork and getting a box on a given floor. */
    public void reachBox(int level, double duration) {
        if ( level <= 0) {
            return;
        }

        if (this.allReachBoxAnimations.getStatus() == Animation.Status.RUNNING){
            return;
        }

        this.liftTranslateAnimation.setDuration(Duration.seconds(duration));
        this.liftTranslateAnimation.setByY((level-1)/2.0);

        this.liftSizeAnimation.setDuration(Duration.seconds(duration));
        this.liftSizeAnimation.setToY(1+1.647*(level-1));

        this.forkTranslateAnimation.setDuration(Duration.seconds(duration));
        this.forkTranslateAnimation.setByY(0.36+level-1);

        this.showBoxAnimation.setDuration(Duration.seconds(duration));

        this.allReachBoxAnimations.play();
    }

    /** Shows or hides box */
    public void setLoaded(boolean loaded) {
        if (loaded) {
            this.cargo.setOpacity(1);
        } else {
            this.cargo.setOpacity(0);
        }
    }

    /** Loads meshes for the forklift, should be called during loading. */
    public static void loadMeshes() {
        forkliftMesh = new MeshView[3];

        MeshView[] parts = readObj("fork.obj");
        System.out.println(parts.length);
        System.arraycopy(parts, 0, forkliftMesh, 0, 3);
        for (MeshView m : forkliftMesh) {
            System.out.println(m.getId());
        }

        cargoMesh = parts[3];
        forkMesh = new MeshView[2];
        System.arraycopy(parts, 4, forkMesh,0,2);

        liftMesh = parts[6];
    }

    private final MeshView cargo;

    private final TranslateTransition moveAnimation = new TranslateTransition();

    private final TranslateTransition forkTranslateAnimation = new TranslateTransition();
    private final ScaleTransition liftSizeAnimation = new ScaleTransition();
    private final TranslateTransition liftTranslateAnimation = new TranslateTransition();
    private final PauseTransition showBoxAnimation = new PauseTransition();
    private final ParallelTransition allReachBoxAnimations;
    private final int id;

    private static MeshView[] forkliftMesh;
    private static MeshView cargoMesh;
    private static MeshView[] forkMesh;
    private static MeshView liftMesh;

    /**
     * This is number in seconds which is considered a unit of time.
     * One block at speed modifier 1.0 is traveled in this number of seconds.
     */
    public static double timeUnit = 0.90;

}
