/*
Author: Dávid Lacko (xlacko09)
Purpose: Manages heatmap overlay
 */
package com.ija.warehouse.ui;

import javafx.css.converter.PaintConverter;
import javafx.scene.paint.*;
import javafx.util.Pair;

import java.util.*;

/**
 * Manages heatmap overlay
 */
public class HeatmapManager {
    private final List<PhongMaterial> materials = new ArrayList<>();
    private final Map<CoordinatesUI, ? extends HeatmapDisplay> displays;

    public HeatmapManager(Map<CoordinatesUI, ? extends HeatmapDisplay> displays, int resolution) {
        this.displays = displays;
        for(int i = 0; i < resolution; i++) {
            materials.add(new PhongMaterial(Color.rgb((int)(240*(i/10.)), (int)(240*(1-(i/10.))),0)));
        }
    }

    /**
     * Enables heatmap.
     * The colors are distributed between maximum of provided hitcounts and minimum.
     * @param heats map of coordinates with hitcounts
     */
    public void enableHeatmap(Map<CoordinatesUI, Integer> heats) {

        if (heats == null || heats.isEmpty()) {
            throw new RuntimeException("Empty heats list provided to heatmap");
        }

        int maxFound = 0;
        int minFound = Integer.MAX_VALUE;
        for (int val : heats.values()) {
            if (val > maxFound) {
                maxFound = val;
            }
            if (val < minFound) {
                minFound = val;
            }
        }

        List<PhongMaterial> lst = new ArrayList<>();
        for(CoordinatesUI key : displays.keySet()) {
            if (heats.containsKey(key)) {
                // Linear sampling of the hitcounts into resolution of heatmap
                PhongMaterial mat = materials.get((int)( (((double) heats.get(key) - minFound) / maxFound)  *  (materials.size()-1)));
                displays.get(key).enableHeatmap(mat);
            }
        }
    }

    public void disableHeatmap() {
        for(HeatmapDisplay d : displays.values()) {
            d.disableHeatmap();
        }
    }
}
