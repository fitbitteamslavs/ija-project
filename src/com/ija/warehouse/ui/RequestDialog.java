/*
Author: Dávid Lacko (xlacko09)
Purpose: Dialog controller for adding requests
 */
package com.ija.warehouse.ui;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableIntegerValue;
import javafx.beans.value.ObservableStringValue;
import javafx.beans.value.WritableIntegerValue;
import javafx.beans.value.WritableStringValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Dialog controller for sending new request to the backend
 */
public class RequestDialog implements Initializable {
    private static class GoodsEntry {
        private final WritableStringValue name;
        private final WritableIntegerValue amount;
        GoodsEntry(String name, int amount) {
            this.name = new SimpleStringProperty(name);
            this.amount = new SimpleIntegerProperty(amount);
        }
        public void setAmount(int amount) {
            this.amount.set(amount);
        }
        public void add(int amount) {
            this.amount.set(this.amount.get()+amount);
        }
        public ObservableStringValue getName() {
            return (ObservableStringValue) name;
        }
        public ObservableIntegerValue getAmount() {
            return (ObservableIntegerValue) amount;
        }
    }

    @FXML private ToggleButton toggleTypeButton;
    @FXML private Circle toggleColorCircle;
    @FXML private Label availableLabel;
    @FXML private Spinner<Integer> amountInput;
    @FXML private ComboBox<String> goodsList;
    @FXML private TableView<GoodsEntry> contentsTable;
    @FXML private Label newGoodsLabel;
    @FXML private TextField newGoodsInput;

    private Map<String, Integer> goods;
    HashMap<String, Integer> output = new HashMap<>();
    int type = 0;

    public RequestDialog() {
    }

    /** Set available goods */
    public void setGoods(Map<String, Integer> goods) {
        this.goods = goods;
        for (String s : goods.keySet()) {
            goodsList.getItems().add(s);
        }
        goodsList.setValue(goodsList.getItems().get(0));
        selectGoods();
    }

    /** Event handler for switching type of request */
    @FXML private void typeSwitched() {
        contentsTable.getSelectionModel().selectAll();
        removeSelectedGoods();
        if(toggleTypeButton.isSelected()) {
            toggleTypeButton.setText("Import");
            toggleTypeButton.setContentDisplay(ContentDisplay.LEFT);
            toggleColorCircle.setFill(Paint.valueOf("#2b79db"));
            type = 1;
            newGoodsInput.setVisible(true);
            newGoodsLabel.setVisible(true);
        } else {
            toggleTypeButton.setText("Export");
            toggleTypeButton.setContentDisplay(ContentDisplay.RIGHT);
            toggleColorCircle.setFill(Paint.valueOf("#54c462"));
            type = 0;
            newGoodsInput.setVisible(false);
            newGoodsLabel.setVisible(false);
        }
    }

    public Map<String, Integer> getRequestContent() {
        return output;
    }

    /**
     * Returns type of request.
     * 0 - Export
     * 1 - Import
     * @return number representing type of request
     */
    public int getRequestType() {
        return type;
    }

    /**
     * Initializer of scene
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        TableColumn<GoodsEntry, String> col1 = new TableColumn<>("Goods Name");
        col1.setCellValueFactory(goodsEntryStringCellDataFeatures -> goodsEntryStringCellDataFeatures.getValue().getName());
        col1.setPrefWidth(200);
        TableColumn<GoodsEntry, Number> col2 = new TableColumn<>("Amount");
        col2.setCellValueFactory(goodsEntryIntegerCellDataFeatures -> goodsEntryIntegerCellDataFeatures.getValue().getAmount());
        col2.setPrefWidth(100);
        contentsTable.getColumns().addAll(col1, col2);
        contentsTable.getSelectionModel().setCellSelectionEnabled(false);
        contentsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        amountInput.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1,Integer.MAX_VALUE,1));
        amountInput.getEditor().textProperty().addListener((observableValue, oldVal, newVal) -> {
            if (!newVal.equals("")) {
                try {
                    int i = Integer.parseUnsignedInt(newVal);
                    if (!toggleTypeButton.isSelected() && i > goods.get(goodsList.getValue())) {
                        amountInput.getEditor().setText(oldVal);
                    }
                } catch (NumberFormatException e) {
                    amountInput.getEditor().setText(oldVal);
                }
            }
        });
        
        newGoodsInput.textProperty().addListener((observableValue, s, t1) -> goodsList.setDisable(!t1.equals("")));
    }

    /** Handler for goods selection selection input */
    @FXML private void selectGoods() {
        availableLabel.setText(goods.get(goodsList.getValue()).toString());
    }

    @FXML private void close() {
        ((Stage) toggleTypeButton.getScene().getWindow()).close();
    }

    @FXML private void submit() {
        for(GoodsEntry p : this.contentsTable.getItems()) {
            output.put(p.getName().get(), p.getAmount().get());
        }
        this.close();
    }

    /** Adds currently selected goods to request goods in provided amount */
    @FXML private void addGoods() {
        boolean contains = false;
        String addedGoods = goodsList.getValue();
        if (toggleTypeButton.isSelected() && !newGoodsInput.getText().equals("")) {
            addedGoods = newGoodsInput.getText();
            newGoodsInput.setText("");
        }
        for(GoodsEntry p : contentsTable.getItems()) {
            if (p.getName().get().equals(addedGoods)) {
                p.add(amountInput.getValue());
                if (!toggleTypeButton.isSelected()) {
                    int i = goods.get(p.getName().get());
                    goods.replace(p.getName().get(), i - amountInput.getValue());
                }
                contains = true;
                break;
            }
        }
        if (!contains) {
            GoodsEntry entr = new GoodsEntry(addedGoods, amountInput.getValue());
            contentsTable.getItems().add(entr);
            if (!toggleTypeButton.isSelected()) {
                int i = goods.get(entr.getName().get());
                goods.replace(entr.getName().get(), i - amountInput.getValue());
            }
        }
        selectGoods();
    }

    /** Removed currently selected goods from request */
    @FXML private void removeSelectedGoods() {
        if (type == 0) {
            for (GoodsEntry p : contentsTable.getSelectionModel().getSelectedItems()) {
                goods.replace(p.getName().get(), p.getAmount().get() + goods.get(p.getName().get()));
            }
        }
        contentsTable.getItems().removeAll(contentsTable.getSelectionModel().getSelectedItems());
        selectGoods();
    }
}
