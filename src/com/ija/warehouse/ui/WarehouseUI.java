/*
Author: Dávid Lacko (xlacko09)
Purpose: Controller for the whole map display and its controls
 */
package com.ija.warehouse.ui;

import com.ija.warehouse.MainApp;
import com.ija.warehouse.ui.elements.*;
import javafx.application.ConditionalFeature;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Pair;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Function;

/** Acts as a controller for the GUI
 *  All public methods are internally wrapped in Platform.runLater to run all GUI oriented tasks in the GUI thread.
 **/
public class WarehouseUI implements Initializable {
    public WarehouseUI() {
        if ( !Platform.isSupported(ConditionalFeature.SCENE3D) ) {
            throw new RuntimeException("No support for 3D in javafx.");
        }
    }


    /* UI elements from fxml file */
    @FXML private SubScene scene;
    @FXML private Group sceneRoot;
    @FXML private Pane sceneParent;
    @FXML private Slider speedSlider;
    @FXML private Label speedLabel;
    @FXML private TreeTableView<Pair<String, Integer>> infoView;
    @FXML private Label timeLabel;
    @FXML private ToggleButton heatmapButton;

    /* Local variables */
    private final HashMap<CoordinatesUI, Rack> rackMap = new HashMap<>();
    private final HashMap<Integer, Forklift> forklifts = new HashMap<>();

    /* Variables connected with managing scene */
    private final Group staticElements = new Group();
    private final Box foundation = new Box(0,0.05,0);
    private final PointLight pointLight = new PointLight(Color.WHITE);
    private final PhongMaterial wallMaterial = new PhongMaterial(Color.gray(0.3));
    private ForkliftRouteDisplay forkliftRoute;
    private HeatmapManager heatmapManager;
    private WarehouseCamera camera;

    /* Communication with backend */
    private InfoGetterUI infoGetter = null;
    private ActionListenerUI actionListener = null;

    /* User control (keyboard, mouse) variables */
    private double prevMouseX;
    private double prevMouseY;
    private boolean playing = false;
    private boolean mouseDragging = false;

    /** Initializes all necessary GUI elements, like cameras, lights and controls, registers event handlers.
     *
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        initializeScene();

        /* ---------- Register all listeners --------------------------------------------- */
        // Simulation speed controller
        speedSlider.valueProperty().addListener((observableValue, oldValue, newValue) -> {
            speedLabel.setText(String.format("%.1fx", newValue.doubleValue()));
            this.actionListener.speedChanged(newValue.doubleValue());
            for(Forklift f : this.forklifts.values()){
                f.updateSpeed(newValue.doubleValue());
            }
        });

        // **************************  Clicks on scene objects **********************
        scene.addEventHandler(MapElementClickedEvent.FORKLIFT_CLICKED, showInfoEvent -> {
            this.setViewInfo(this.infoGetter.getForkliftContent(showInfoEvent.id), "Forklift");
            InfoGetterUI.ForkliftRoute route = this.infoGetter.getForkliftRoute(showInfoEvent.id);
            this.forkliftRoute.displayRoute(route, showInfoEvent.id);
        });

        scene.addEventHandler(MapElementClickedEvent.RACK_CLICKED, showInfoEvent ->
                this.setViewInfo(this.infoGetter.getStorageInfo(showInfoEvent.x, showInfoEvent.y), "Rack"));

        scene.addEventHandler(MapElementClickedEvent.ROAD_CLICKED, showInfoEvent -> {
            if (this.actionListener != null) {
                this.actionListener.roadBlock(showInfoEvent.x, showInfoEvent.y);
            }
            ((Road)showInfoEvent.getSourceElement()).toggleBlocked();
        });

        // ******************************* User input handlers **************************************
        scene.setOnMousePressed(mouseEvent -> {
            prevMouseX = mouseEvent.getSceneX();
            prevMouseY = mouseEvent.getSceneY();
            scene.requestFocus();
        });

        // When dragging the mouse do not activate clicks on map -> consume event
        scene.addEventFilter(MouseEvent.MOUSE_RELEASED, event -> {
            if(mouseDragging) {
                event.consume();
                mouseDragging = false;
            }
        });

        // When dragging with left mouse clicked rotate camera or move 2D view
        scene.setOnMouseDragged(mouseEvent -> {
            mouseDragging = true;
            if ( mouseEvent.isPrimaryButtonDown()) {
                double xPos = mouseEvent.getSceneX();
                double yPos = mouseEvent.getSceneY();

                camera.rotateCamera((prevMouseX-xPos), (yPos-prevMouseY));

                prevMouseX = xPos;
                prevMouseY = yPos;
            }
        });

        // Change zoom on scrolling
        scene.setOnScroll(scrollEvent -> {
            double delta = scrollEvent.getDeltaY();
            if (delta == 0) {
                return;
            }
            camera.zoomCamera(delta);
        });

        scene.setOnKeyPressed(keyEvent -> {
            double moveAngle = 0;
            boolean move = false;
            int code = keyEvent.getCode().getCode();
            // WASD controls camera movement
            if (code == KeyCode.W.getCode()) { //W
                move = true;
            } else if (code == KeyCode.A.getCode()) { //A
                moveAngle -= 90;
                move = true;
            } else if (code == KeyCode.D.getCode()) { //D
                moveAngle += 90;
                move = true;
            } else if (code == KeyCode.S.getCode()) { //S
                moveAngle += 180;
                move = true;
            // Escape clears info viewer and forklift route if displayed
            } else if (code == KeyCode.ESCAPE.getCode()) {
                this.forkliftRoute.clear();
                this.infoView.setRoot(null);
            }
            if (move) {
                moveAngle = moveAngle % 360;
                camera.moveCamera(moveAngle);
            }
        });


        // ********************************* Setup info view **************************************
        TreeTableColumn<Pair<String, Integer>, String> col1 = new TreeTableColumn<>("Goods");
        col1.setCellValueFactory(stringCellDataFeatures ->
                new ReadOnlyStringWrapper(stringCellDataFeatures.getValue().getValue().getKey()));
        col1.setMinWidth(150);

        TreeTableColumn<Pair<String, Integer>, Integer> col2 = new TreeTableColumn<>("Count");
        col2.setCellValueFactory(integerCellDataFeatures ->
                new ReadOnlyObjectWrapper<>(integerCellDataFeatures.getValue().getValue().getValue()));
        infoView.getColumns().add(col1);
        infoView.getColumns().add(col2);
        infoView.setShowRoot(true);

    }

    /** Fill scene with basic 3D objects */
    private void initializeScene() {
        scene.requestFocus();
        scene.heightProperty().bind(sceneParent.heightProperty());
        scene.widthProperty().bind(sceneParent.widthProperty());

        camera = new WarehouseCamera(scene.widthProperty(), scene.heightProperty(), foundation.getWidth(), foundation.getDepth());
        scene.setCamera(camera.getCamera());

        AmbientLight light = new AmbientLight(Color.WHITE);
        pointLight.setTranslateY(30);
        pointLight.setTranslateZ(0);
        pointLight.setTranslateX(0);
        PointLight pointLight2 = new PointLight(Color.WHITE);
        pointLight2.setTranslateY(30);
        pointLight2.setTranslateZ(0);
        pointLight2.setTranslateY(0);

        PhongMaterial foundationMat = new PhongMaterial();
        foundationMat.setDiffuseColor(Color.grayRgb(20));
        foundationMat.setSpecularColor(Color.BLACK);
        foundationMat.setSpecularPower(32);
        foundation.setTranslateY(-0.05);
        foundation.setMaterial(foundationMat);

        Line bx = new Line(50 ,50,200,200);
        sceneRoot.getChildren().add(bx);

        this.staticElements.getChildren().addAll(foundation, light, pointLight);

        this.sceneRoot.getChildren().add(this.staticElements);

        this.forkliftRoute = new ForkliftRouteDisplay();
        sceneRoot.getChildren().add(forkliftRoute.getGroup());

        Road.loadMeshes();
        Rack.loadMeshes();
        Forklift.loadMeshes();
        Port.loadMeshes();

        heatmapManager = new HeatmapManager(this.rackMap, 10);
    }

    /** Shows provided entries in the info view which has two columns of types (String, Integer) */
    private void setViewInfo(Map<String, Integer> map, String rootName) {
        if (map == null) return;

        TreeItem<Pair<String, Integer>> viewRoot = new TreeItem<>(new Pair<>(rootName, null));
        viewRoot.setExpanded(true);

        map.entrySet().stream()
                .map(goodsQuantityEntry -> new Pair<>(goodsQuantityEntry.getKey(), goodsQuantityEntry.getValue()))
                .map((Function<Pair<String, Integer>, TreeItem<Pair<String, Integer>>>) TreeItem::new)
                .forEach(treeItem -> viewRoot.getChildren().add(treeItem));
        this.infoView.setRoot(viewRoot);
    }



    /* ------------------------------ FXML Methods ------------------------------------- */
    /** Switches view to perspective camera (better suited for 3D view) **/
    @FXML
    private void setPerspectiveCamera() {
        camera.setPerspectiveCamera();
        scene.setCamera(camera.getCamera());
    }

    /** Switches view to orthogonal or parallel camera (better suited for 2D map view) **/
    @FXML
    private void setParallelCamera() {
        camera.setParallelCamera();
        scene.setCamera(camera.getCamera());
    }

    /** Enables heatmap on racks */
    @FXML private void heatmapToggle() {
        if (heatmapButton.isSelected()) {
            heatmapManager.enableHeatmap(this.infoGetter.getHitRate());
        } else {
            heatmapManager.disableHeatmap();
        }
    }

    /** Disables heatmap on racks */
    @FXML private void onPlayPause() {
        if(playing) {
            this.actionListener.pause();
        } else {
            this.actionListener.play();
        }
        this.playing = !playing;
    }

    /** Handler of reset simultion button */
    @FXML private void onSimulationReset() {
        this.actionListener.reset();
        this.forkliftRoute.clear();
        this.infoView.setRoot(null);
    }

    /** Handler of Requests button that shows all requests */
    @FXML private void showRequests() {
        this.setViewInfo(this.infoGetter.getRequestsInfo(), "Requests");
    }



    /* --------------------------------- public interface --------------------------- */
    /**
     * Returns menu for menu bar with specific actions for Warehouse
     * @return Menu for menubar
     */
    public Menu getMenu() {
        Menu menu = new Menu("Warehouse");

        MenuItem request = new MenuItem("Request");
        request.setOnAction(actionEvent -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/ija/warehouse/ui/requestDialog.fxml").toURI().toURL());
                Parent parent;
                parent = fxmlLoader.load();

                RequestDialog dialogController = fxmlLoader.getController();
                dialogController.setGoods(this.infoGetter.getGoodsInfo());

                Scene scene = new Scene(parent);
                scene.getStylesheets().add(getClass().getResource("/styles/style.css").toExternalForm());
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initStyle(StageStyle.UNDECORATED);
                stage.setScene(scene);
                stage.showAndWait();
                System.out.println(dialogController);

                if (this.actionListener != null) {
                    this.actionListener.newRequest(dialogController.getRequestType() == 0 ? ActionListenerUI.RequestType.EXPORT : ActionListenerUI.RequestType.IMPORT,
                            dialogController.getRequestContent());
                }
            } catch (IOException e) {
                System.out.println("Caught exception");
                System.out.println(e.toString());
            } catch (URISyntaxException e) {
                System.err.println("Cannot find layout for request dialog.");
            }
        });

        MenuItem loadRequests = new MenuItem("Load requests...");
        loadRequests.setOnAction(actionEvent -> {
            FileChooser fc = new FileChooser();
            fc.setTitle("Open XML file with requests");
            fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Requests file", "*.xml"));
            fc.setInitialDirectory(new File(System.getProperty("user.dir")));

            File file = fc.showOpenDialog(this.scene.getScene().getWindow());
            if (file != null) {
                System.out.println(file);
            } else {
                return;
            }

            if (!this.actionListener.loadRequestsFromFile(file)) {
                MainApp.showAlert(Alert.AlertType.ERROR, "Problem loading requests", "Cannot load requests from given file.");
            }
        });

        MenuItem setTime = new MenuItem("Set time");
        setTime.setOnAction(event -> {
            TextInputDialog input = new TextInputDialog("Enter time.");
            var result = input.showAndWait();
            if (result.isPresent()) {
                String timeS = input.getEditor().getText();
                if (timeS.matches("\\d\\d?:\\d\\d")) {
                    String[] split = timeS.split(":");
                    double time = Integer.parseInt(split[0]) * 60 + Integer.parseInt(split[1]);
                    this.actionListener.timeChanged(time);
                    this.forkliftRoute.clear();
                    this.infoView.setRoot(null);
                } else {
                    MainApp.showAlert(Alert.AlertType.ERROR, "Wrong time format", "Valid time format is: \\d[\\d]:\\d\\d");
                }
            }
        });

        menu.getItems().addAll(request, loadRequests, setTime);

        return menu;
    }

    /** Resizes base platform object for the warehouse **/
    private void resize(int sizeX, int sizeY) {
        this.foundation.setWidth(sizeX);
        this.foundation.setDepth(sizeY);
        this.foundation.setTranslateX(sizeX / 2. - .5);
        this.foundation.setTranslateZ(sizeY / 2. - .5);

        this.pointLight.setTranslateX(sizeX);
        this.pointLight.setTranslateZ(sizeY);
        System.out.printf("Dimensions: %d %d\n", sizeX, sizeY);

        camera.setMapSize(sizeX, sizeY);
    }

    /** Resizes base platform object for the warehouse **/
    public void setSize(int sizeX, int sizeY) {
        Platform.runLater(() -> {
            this.resize(sizeX, sizeY);
        });
    }

    /** Adds and empty rack object on the given coordinates and height.
     * @see com.ija.warehouse.ui.elements.Rack
     * **/
    public void addRack(int x, int y, int height) {
        Platform.runLater(() -> {
            CoordinatesUI coords = new CoordinatesUI(x, y);
            Rack rack = new Rack(sceneRoot, x, y);
            this.rackMap.put(coords, rack);
            rack.addFloors(height);
        });
    }

    /** Shows or hides box that signals that on given shelf there are some Goods
     * @param x position x
     * @param y position y
     * @param height floor of the shelf
     * @param state show/true or hide/false the box
     */
    public void setRackGoods(int x, int y, int height, boolean state) {
        Platform.runLater(() -> {
            CoordinatesUI coords = new CoordinatesUI(x, y);
            if (this.rackMap.containsKey(coords)) {
                this.rackMap.get(coords).setGoods(height, state);
            }
        });
    }

    /** Adds road with given directions to the map
     * @see com.ija.warehouse.ui.elements.Road
     */
    public void addRoad(int x, int y, int directions) {
        Platform.runLater(() ->
                new Road(this.sceneRoot, directions, x, y));
    }

    /** Adds wall to the map **/
    public void addWall(int x, int y) {
        Platform.runLater(() -> {
            Box bx = new Box(1, 1, 1);
            bx.setMaterial(wallMaterial);
            bx.setTranslateX(x);
            bx.setTranslateZ(y);
            sceneRoot.getChildren().add(bx);
        });
    }

    /** Adds port to the map with given type - Input and/or Output
     * @param x coordinate
     * @param y coordinate
     * @param type type of the port as bitmap 1 is Input and 2 is Output
     * @see com.ija.warehouse.ui.elements.Port
     */
    public void addPort(int x, int y, int type) {
        Platform.runLater(() ->
            new Port(this.sceneRoot, x, y, type));
    }

    /** Adds forklift with given id.
     * Mind that on given coordinates also road/parking spot will be added to the map.
     * If ID that already exists is provided, old reference is rewritten and lost (the model will stay in the map)
     * @param x coordinate
     * @param y coordinate
     * @param id unique ID
     */
    public void addForklift(int x, int y, int id) {
        Platform.runLater(() -> {
            Forklift fl = new Forklift(this.sceneRoot, x, y, id);
            this.forklifts.put(id, fl);
        });
    }

    /** Gives command to the forklift to move to given location.
     * This is done by setting up and playing animation.
     * @param x absolute coordinate to move to
     * @param y absolute coordinate to move to
     * @param id ID of the forklift
     */
    public void moveForklift(int x, int y, int id) {
        Platform.runLater(() -> {
            if (this.forklifts.containsKey(id)) {
                int length = this.forklifts.get(id).move(x, y);
                if (this.forkliftRoute.getForklift() == id) {
                    this.forkliftRoute.moved(length);
                }
            }
        });
    }

    /**
     * Plays animation of forklift reaching for the box.
     * The animation length is linearly dependent on height.
     * @param height height to reach to
     * @param id Identifier of the forklift
     */
    public void reachBoxWithForklift(int height, int id) {
        Platform.runLater(() -> {
            if (this.forklifts.containsKey(id)) {
                this.forklifts.get(id).reachBox(height, height/2.);
                if (this.forkliftRoute.getForklift() == id) {
                    this.forkliftRoute.stopped();
                }
            }
        });
    }

    /**
     * Removes the box from forklift fork
     * @param id Identifier of forklift
     */
    public void unloadForklift(int id) {
        Platform.runLater(() -> {
            if (this.forklifts.containsKey(id)) {
                this.forklifts.get(id).setLoaded(false);
                if (this.forkliftRoute.getForklift() == id) {
                    this.forkliftRoute.stopped();
                }
            }
        });
    }

    /**
     * Updates the displayed route of the forklift with provided route.
     * If the id does not correspond with forklift which route is currently displayed it does nothing.
     * @param id Identifier of forklift which route is currently displayed
     * @param route New route for forklift
     */
    public void updateForkliftRoute(int id, InfoGetterUI.ForkliftRoute route) {
        Platform.runLater(() -> {
            if (this.forkliftRoute.getForklift() == id) {
                this.forkliftRoute.displayRoute(route, id);
            }
        });
    }

    /**
     * Updates Info view with new information.
     * If the id does not correspond with currently displayed ID, it does nothing.
     * @param id
     * @param map
     */
    public void updateForkliftContent(int id, Map<String, Integer> map) {
        Platform.runLater(() -> {
            if (this.forkliftRoute.getForklift() == id) {
                this.setViewInfo(map, "Forklift");
            }
        });
    }

    /**
     * Clears the whole scene of all previously added elements
     */
    public void clean() {
        Platform.runLater(() -> {
            this.rackMap.clear();
            this.forklifts.clear();
            this.forkliftRoute.clear();

            timeLabel.setText("00:00");

            this.resize(0, 0);

            this.sceneRoot.getChildren().clear();
            this.sceneRoot.getChildren().add(this.staticElements);
            this.sceneRoot.getChildren().add(this.forkliftRoute.getGroup());
        });
    }

    /** Registers InfoGetter, which will be called to gain information about the warehouse **/
    public void registerInfoGetter(InfoGetterUI infoGetter) {
        Platform.runLater(() ->
                this.infoGetter = infoGetter);
    }

    /**
     * Registers objects that listens to actions from GUI.
     * Needs to be assigned to work properly.
     * @param listener Listener object
     */
    public void registerActionListener(ActionListenerUI listener) {
        Platform.runLater(() ->
                this.actionListener = listener);
    }

    /**
     * This function sets the time label with provided time.
     * @param t time in seconds
     */
    public void setTime(double t) {
        Platform.runLater(() ->
                timeLabel.setText(String.format("%02d:%02d",(int)t/60, (int)t%60)));
    }

}
