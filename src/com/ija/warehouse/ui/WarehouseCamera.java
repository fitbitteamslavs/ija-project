/*
Author: Dávid Lacko (xlacko09)
Purpose: Manages cameras for scene
 */
package com.ija.warehouse.ui;

import javafx.beans.value.ObservableDoubleValue;
import javafx.scene.Camera;
import javafx.scene.ParallelCamera;
import javafx.scene.PerspectiveCamera;
import javafx.scene.transform.Rotate;

/**
 * Encapsulates operations on cameras for scene and their switching
 */
public final class WarehouseCamera {

    private final Rotate camRotX = new Rotate(0, Rotate.X_AXIS);
    private final Rotate camRotY = new Rotate(0, Rotate.Y_AXIS);

    private final PerspectiveCamera perspectiveCamera = new PerspectiveCamera(true);
    private final ParallelCamera parallelCamera = new ParallelCamera();

    private Camera currentCamera = parallelCamera;

    private final ObservableDoubleValue sceneX;
    private final ObservableDoubleValue sceneY;

    private double mapX;
    private double mapY;

    /**
     *
     * @param viewSizeX Size of the view (displayed part of the scene) in pixels
     * @param viewSizeY Size of the view (displayed part of the scene) in pixels
     * @param mapSizeX Size of the map
     * @param mapSizeY Size of the map
     */
    public WarehouseCamera(ObservableDoubleValue viewSizeX, ObservableDoubleValue viewSizeY, double mapSizeX, double mapSizeY) {
        sceneX = viewSizeX;
        sceneY = viewSizeY;

        setMapSize(mapSizeX, mapSizeY);

        perspectiveCamera.setTranslateZ(4);
        perspectiveCamera.setTranslateY(15);
        perspectiveCamera.setTranslateX(10);
        perspectiveCamera.setRotationAxis(Rotate.Z_AXIS);
        perspectiveCamera.setRotate(180);
        camRotY.setAngle(180);
        camRotX.setAngle(-90);
        Rotate camRotZ = new Rotate(0, Rotate.Z_AXIS);
        perspectiveCamera.getTransforms().addAll(camRotZ,camRotY,camRotX);
        perspectiveCamera.setFieldOfView(60);

        parallelCamera.setRotationAxis(Rotate.X_AXIS);
        parallelCamera.setRotate(90);
        parallelCamera.setTranslateX(1);
        parallelCamera.setTranslateZ(0);
        parallelCamera.setScaleY(0.05);
        parallelCamera.setScaleX(0.05);
    }

    /**
     * Sets map size
     */
    public void setMapSize(double mapSizeX, double mapSizeY) {
        mapX = mapSizeX;
        mapY = mapSizeY;
        center();
    }

    /**
     * Rotates camera from mouse movement
     * @param x mouse x movement
     * @param y mouse y movement
     */
    public void rotateCamera(double x, double y) {
        if (currentCamera == perspectiveCamera) {
            double camX = camRotX.getAngle();
            double camY = camRotY.getAngle();

            camRotY.setAngle(camY + x*0.1);
            camRotX.setAngle(camX + y*0.1);
        } else {
            parallelCamera.setTranslateZ(parallelCamera.getTranslateZ() - y*parallelCamera.getScaleX());
            parallelCamera.setTranslateX(parallelCamera.getTranslateX() + x*parallelCamera.getScaleX());
        }
    }

    /**
     * Zooms camera.
     * @param z amount of zoom
     */
    public void zoomCamera(double z) {

        if (currentCamera == perspectiveCamera) {
            double yPos = perspectiveCamera.getTranslateY();
            if (yPos > 40 && z < 0) {
                perspectiveCamera.setTranslateY(perspectiveCamera.getTranslateY() + z * 0.02);
            }
            if (yPos < .1 && z > 0) {
                perspectiveCamera.setTranslateY(perspectiveCamera.getTranslateY() + z * 0.02);
            }
            if (yPos > .1 && perspectiveCamera.getTranslateY() < 40) {
                perspectiveCamera.setTranslateY(perspectiveCamera.getTranslateY() + z * 0.02);
            }
            if ((yPos < 10 && z < 0)) {
                double angle = camRotX.getAngle();
                camRotX.setAngle(angle + Math.abs(angle) * Math.sin(-angle / 180 * Math.PI * 0.2));
            }
        }
        else if (currentCamera == parallelCamera) {
            double scale = parallelCamera.getScaleX();
            double change = 0.0;
            if (z < 0) {
                change = -0.001;
            } else if (z > 0) {
                change = 0.001;
            }
            if (scale + change > 0) {
                double deltaX = sceneX.doubleValue()*change;
                double deltaY = sceneY.doubleValue()*change;

                parallelCamera.setScaleX(scale + change);
                parallelCamera.setScaleY(scale + change);

                parallelCamera.setTranslateZ(parallelCamera.getTranslateZ() - deltaY/2.);
                parallelCamera.setTranslateX(parallelCamera.getTranslateX() - deltaX/2.);
            }
        }
    }

    /**
     * Moves camera by one scene unit in direction.
     * @param direction direction of movement in degrees
     */
    public void moveCamera(double direction) {
        double modifZ, modifX;
        if (currentCamera == perspectiveCamera) {
            modifZ = Math.cos(Math.toRadians(direction+camRotY.getAngle()));
            modifX = Math.sin(Math.toRadians(direction+camRotY.getAngle()));
            perspectiveCamera.setTranslateZ(perspectiveCamera.getTranslateZ() + modifZ * 0.5);
            perspectiveCamera.setTranslateX(perspectiveCamera.getTranslateX() - modifX * 0.5);
        } else {
            modifZ = Math.cos(Math.toRadians(direction));
            modifX = Math.sin(Math.toRadians(direction));

            parallelCamera.setTranslateZ(parallelCamera.getTranslateZ() - modifZ);
            parallelCamera.setTranslateX(parallelCamera.getTranslateX() + modifX);
        }
    }

    /** Centers 2D camera to fit the whole map */
    public void center() {
        parallelCamera.setTranslateZ(-sceneY.doubleValue() * parallelCamera.getScaleX() / 2 + mapY / 2);
        parallelCamera.setTranslateX(-sceneX.doubleValue() * parallelCamera.getScaleX() / 2 + mapX / 2);
    }

    /** Set perspective camera as active */
    public void setPerspectiveCamera() {
        this.currentCamera = perspectiveCamera;
    }

    /** Set parallel camera as active */
    public void setParallelCamera() {
        this.currentCamera = parallelCamera;
    }

    /** Returns active camera */
    public Camera getCamera() {
        return this.currentCamera;
    }
}
