/*
Author: Dávid Lacko (xlacko09)
Purpose: Manages forklift route display
 */
package com.ija.warehouse.ui;

import com.ija.warehouse.ui.elements.AbstractWarehouseElement;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Sphere;

/**
 * Manages display of forklift route and its stops
 */
class ForkliftRouteDisplay {

    private final Group route = new Group();
    private final Group routePath = new Group();
    private final Group routeStops = new Group();
    private final PhongMaterial routeMaterial = new PhongMaterial(Color.rgb(19, 107, 194));
    private final PhongMaterial routeStopMaterial = new PhongMaterial(Color.rgb(224, 49, 114));

    private int forkliftID = -1;

    protected ForkliftRouteDisplay() {
        route.getChildren().addAll(routePath, routeStops);
    }

    /**
     * Displays provided route
     * @param route Route of forklift
     * @param id Id of the forklift
     */
    protected void displayRoute(InfoGetterUI.ForkliftRoute route, int id) {
        this.clear();

        System.out.println(route.route.size());

        this.forkliftID = id;
        for(CoordinatesUI position : route.route) {
            Sphere sp = new Sphere(0.15);
            sp.setMaterial(this.routeMaterial);
            sp.setTranslateY(0.5);
            sp.setTranslateX(position.getX());
            sp.setTranslateZ(position.getY());
            this.routePath.getChildren().add(sp);
        }
        System.out.println(this.routePath.getChildren().size());
        for (CoordinatesUI position : route.stops) {
            Box sp = new Box(0.4,0.4,0.4);
            sp.setMaterial(this.routeStopMaterial);
            sp.setTranslateY(0.5);
            sp.setTranslateX(position.getX());
            sp.setTranslateZ(position.getY());
            this.routeStops.getChildren().add(sp);
        }
        System.out.println(this.route.getChildren().size());
    }

    /** If forklift moves remove the path display where it moves */
    public void moved(int length) {
        if (this.routePath.getChildren().size() >= length && length > 0) {
            this.routePath.getChildren().remove(0, length);
        }
    }

    /** Returns a group where the route will be placed */
    public Group getGroup() {
        return this.route;
    }

    /**
     * @return Id of the forklift for which the path is displayed
     */
    public int getForklift() {
        return this.forkliftID;
    }

    /**
     * Clears the display
     */
    protected void clear() {
        this.routePath.getChildren().clear();
        this.routeStops.getChildren().clear();
        this.forkliftID = -1;
    }

    /**
     * Removes the stop on the path
     */
    public void stopped() {
        if (!this.routeStops.getChildren().isEmpty()) {
            this.routeStops.getChildren().remove(0);
        }
    }
}
