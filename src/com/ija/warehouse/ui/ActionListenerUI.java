/*
Author: Dávid Lacko (xlacko09)
Purpose: Interface for object which will be registered as ActionListener in WarehouseUI
 */
package com.ija.warehouse.ui;

import java.io.File;
import java.util.Map;

/** Interface for receiving event from GUI */
public interface ActionListenerUI {

    /** When user changes time this function is called */
    boolean timeChanged(double time);

    void speedChanged(double speed);

    /** When road block is placed or removed this function is called */
    boolean roadBlock(int x, int y);

    /**
     * Enum of possible request types
     */
    enum RequestType {
        IMPORT,
        EXPORT
    }

    /**
     * When user added new request using the dialog.
     */
    void newRequest(RequestType type, Map<String, Integer> goods);

    /**
     * Load requests from file
     */
    boolean loadRequestsFromFile(File requestsFile);

    /** Plays/resumes the simulation */
    void play();
    /** Pauses the simulation */
    void pause();

    void reset();


}
