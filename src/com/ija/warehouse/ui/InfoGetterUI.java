/*
Author: Dávid Lacko (xlacko09)
Purpose: Interface for object which will be registered as infoGetter in WarehouseUI
 */
package com.ija.warehouse.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface InfoGetterUI {
    class ForkliftRoute {
        public List<CoordinatesUI> route = new ArrayList<>();
        public List<CoordinatesUI> stops = new ArrayList<>();
    }

    /** Returns info about the contents of the rack or other storage at given coordinates */
    Map<String,Integer> getStorageInfo(int x, int y);

    /** @return info about cargo of the forklift with given ID */
    Map<String, Integer> getForkliftContent(int id);
    /** @return  list of coordinates how the forklift's route is planned */
    ForkliftRoute getForkliftRoute(int id);

    Map<String, Integer> getGoodsInfo();

    Map<CoordinatesUI, Integer> getHitRate();

    Map<String, Integer> getRequestsInfo();

}
