/*
Author: Dávid Lacko (xlacko09)
Purpose: Interface for objects which allow heatmap overlay
 */
package com.ija.warehouse.ui;

import javafx.scene.paint.PhongMaterial;

import java.util.List;

public interface HeatmapDisplay {
    /** Enables textures for heatmap mode */
    void enableHeatmap(PhongMaterial floorMaterial);
    /** Disables textures for heatmap mode */
    void disableHeatmap();
}
