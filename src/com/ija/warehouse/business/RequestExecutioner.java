/*
Authors: Dávid Lacko (xlacko02), Peter Močáry (xmcoar00)
Purpose: Execution of requests submitted by the user from file or user interface.
 */
package com.ija.warehouse.business;

import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.map.*;
import com.ija.warehouse.business.planning.*;
import com.ija.warehouse.ui.ActionListenerUI;
import com.ija.warehouse.ui.CoordinatesUI;
import com.ija.warehouse.ui.InfoGetterUI;
import com.ija.warehouse.ui.WarehouseUI;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;

public class RequestExecutioner implements ActionListenerUI {

    private WarehouseMap map;
    private List<Forklift> forklifts;
    private List<Forklift> availableForklifts;
    private ForkliftRoadPlanner planner;
    private LinkedList<AbstractForkliftPlan> plans;
    private LinkedList<Request> requestsBackup;
    private LinkedList<Request> requests;
    private double time;
    private volatile double speed = 1.;
    private WarehouseUI ui;
    private volatile Boolean paused = true;
    private volatile Boolean reset = false;
    private volatile Boolean timeChange = false;
    private double newTime;
    private volatile Coordinates blockedRoad = null;
    HashMap<Coordinates, AbstractForkliftPlan> movements = new HashMap<>();
    Set<Coordinates> forkliftPositions = new HashSet<>();
    Map<Forklift, Integer> stopDelays = new HashMap<>();

    public RequestExecutioner(WarehouseMap map, LinkedList<Forklift> forklifts, LinkedList<Request> requests, WarehouseUI ui) {

        time = 0;


        this.map = map;
        this.ui = ui;
        this.forklifts = forklifts;
        this.requests = requests;
        requestsBackup = new LinkedList<>();
        this.requestsBackup.addAll(requests);

        // Create a list of forklifts to pass to road planner constructor
        availableForklifts = new ArrayList<>(forklifts.size());
        availableForklifts.addAll(forklifts);

        planner = new ForkliftRoadPlanner(this.map, availableForklifts);
        plans = new LinkedList<>();

        saveState();
        tryAssigningRequest();
    }

    /**
     * Saves the state of each storage block on the map. This is used for rewinding the time.
     * Each shelf saves current goods types and their quantities. The saving it self doesn't change
     * anything so the simulation can proceed safely until the rewinding of the time.
     */
    private void saveState() {
        map.getItemStorageBlocks().values().forEach(block -> {
            if (block.getMapBlock().getBlockType().getTypeName().equals("Rack")) {
                Rack rack = (Rack) block;
                rack.saveState();
            } // Other storage block don't exist for now
        });
    }

    private void resetState() {
        // Reset the time
        time = 0;

        // Port the forklifts home and empty them
        availableForklifts.clear();
        for (Forklift forklift : forklifts) {
            forklift.moveAllItemsOut(); // the items are regenerated by the reset function of the rack
            forklift.setPosition(forklift.getHomePosition());
            ui.moveForklift(forklift.getHomePosition().getX(), forklift.getHomePosition().getY(), forklift.getID());
            ui.unloadForklift(forklift.getID());
            availableForklifts.add(forklift);
        }

        //unlock the ports
        map.getItemExportBlocks().values().forEach(block -> {
            block.getMapBlock().free();
        });

        map.getItemImportBlocks().values().forEach(block -> {
            block.getMapBlock().free();
        });

        // Reset the requests and plans
        requests.clear();
        requests.addAll(requestsBackup);
        plans.clear();
        planner = new ForkliftRoadPlanner(map, availableForklifts);

        // Reset storage blocks
        map.getItemStorageBlocks().values().forEach(block -> {
            if (block.getMapBlock().getBlockType().getTypeName().equals("Rack")) {
                Rack rack = (Rack) block;
                rack.resetState();
            } // Other storage block don't exist for now
        });

    }

    private void tick() {
        var it = plans.descendingIterator();
        while (it.hasNext()) {
            AbstractForkliftPlan plan = it.next();
            Forklift forklift = plan.getAssociatedForklift();

            if (this.stopDelays.containsKey(forklift)) {
                int delay = this.stopDelays.get(forklift);
                if (delay > 0) {
                    this.stopDelays.put(forklift, delay-1);
                    continue;
                } else {
                    this.stopDelays.remove(forklift);
                }
            }

            ForkliftPlanStop currentStop = plan.getCurrentStop();
            Coordinates coords = currentStop.getNextPosition();
            // If there is no more distance to go to the Block at which we want to stop
            if (coords == null) {
                ForkliftAction action = currentStop.getAction();
                int forkliftID = plan.getAssociatedForklift().getID();

                // Perform action and send appropriate actions to GUI
                int status = action.perform(plan.getAssociatedForklift(), currentStop.getTargetBlock());
                if (status == -1) {
                    throw new RuntimeException("Cannot perform action at given location.");
                }
                if (status > 0) {
                    this.stopDelays.put(forklift, status);
                }

                if (!timeChange) {
                    // Update GUI
                    Map<String, Integer> outputMap = new HashMap<>();
                    Map<Goods, Integer> forkliftInventoryDescription = forklift.getInventoryDescription();
                    outputMap.put("<<ID>>", forklift.getID());
                    forkliftInventoryDescription.forEach((key, value) -> outputMap.put(key.getName(), value));
                    ui.updateForkliftContent(forkliftID, outputMap);
                    if (plan.getAssociatedForklift().getCurrentItemCount() == 0) {
                        ui.unloadForklift(forkliftID);
                    } else {
                        ui.reachBoxWithForklift(status+1, forkliftID);
                    }
                }

                if (plan.getNextStop() == null) {
                    planner.addAvailableForklift(plan.getAssociatedForklift());
                    plan.getPort().free();
                    it.remove();
                    tryAssigningRequest();
                }

                // If we are loading or unloading we are standing still
                this.forkliftPositions.add(forklift.getActualPosition());
            } else {
                // Check if we can move to next position
                if (!map.getBlockAtPosition(coords).isReserved()) {
                    if (!this.movements.containsKey(coords) || coords.equals(forklift.getActualPosition())) {
                        this.movements.put(coords, plan);
                    }
                    // If not we are standing still
                }
                this.forkliftPositions.add(forklift.getActualPosition());
            }
        }

        // For each movement check if it does not collide with some still standing forklift
        movements.forEach((coords, plan) -> {
            if (!this.forkliftPositions.contains(coords) || coords.equals(plan.getAssociatedForklift().getActualPosition())) {
                if (!timeChange) {
                    ui.moveForklift(coords.getX(), coords.getY(), plan.getAssociatedForklift().getID());
                }
                plan.getAssociatedForklift().setPosition(coords);
                plan.getCurrentStop().advance();
            }
        });
        movements.clear();
        this.forkliftPositions.clear();
    }

    private void checkNewRoadBlockages() {
        if (this.blockedRoad != null) {
            MapBlock b = this.map.getBlockAtPosition(blockedRoad);
            if (b.getBlockType() == Road.blockType) {
                Road r = (Road) b;
                if (r.doesContainObstacle()) {
                    r.removeObstacle();
                } else {
                    r.placeObstacle();
                    for (int i = 0; i < this.plans.size(); i++) {
                        try {
                            var plan = this.plans.get(i);
                            var newPlan = planner.adaptPlan(plan, blockedRoad);
                            this.plans.set(i, planner.adaptPlan(plan, blockedRoad));
                            if (plan != newPlan) {
                                int forkliftId = plan.getAssociatedForklift().getID();
                                ui.updateForkliftRoute(forkliftId, this.getForkliftRoute(forkliftId));
                            }
                        } catch (PathPlanningException e) {
                            System.err.print("Cannot adapt plan: ");
                            System.err.println(e.getMessage());
                        } catch (RuntimeException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            this.blockedRoad = null;
        }
    }

    public synchronized void run() {
        try {
            wait();
            while (true) {
                ui.setTime(time);
                if (paused) {
                    wait();
                }
                if (reset) {
                    resetState();
                    reset = false;
                    tryAssigningRequest();
                }
                checkNewRoadBlockages();
                tick();
                if (timeChange) {
                    if (newTime == time) {
                        timeChange = false;
                        for(Forklift fl : this.forklifts) {
                            ui.moveForklift(fl.getActualPosition().getX(), fl.getActualPosition().getY(), fl.getID());
                            System.out.println("Finished simulation setting forklifts: " + fl.getID() + " " + fl.getActualPosition());
                        }
                        notify();
                    }
                } else {
                    wait((long) (1000 / speed));
                }
                time++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void tryAssigningRequest() {
        System.out.println("\nAvailable forklifts:");
        availableForklifts.forEach(fl -> {
            System.out.printf("\t%d ", fl.getID());
        });
        System.out.println();

        Request currentRequest;

        // Creates as many plans as possible before starting clock. These plans are assigned to concrete forklift by the
        // createGoodsRetrievalPLan function.
        int requestsSize = requests.size(); // needs to be stored to avoid infinite loop, since this loop is moving unhandled requests to its end.
        for (int i = 0; i < requestsSize; i++) {
            currentRequest = requests.pop();

            // Create plan from request and store it
            MapBlock port;
            Predicate<MapBlock> predicate;
            if (currentRequest.getActionType() == Request.RequestType.EXPORT) {
                predicate = b -> b instanceof ItemExportBlock && !b.isReserved();
            } else {
                predicate = b -> b instanceof ItemImportBlock && !b.isReserved();
            }

            var opt = map.getClosestReachableBlockSatisfyingPredicate(planner.getNextAvailableForklift().getActualPosition(),
                    predicate);
            if (opt.isPresent()) {
                port = opt.get().getValue();
            } else {
                return;
            }

            try {

                // Create a plan for the current request
                AbstractForkliftPlan plan;
                if (currentRequest.getActionType() == Request.RequestType.EXPORT) {
                    plan = planner.createGoodsRetrievalPlan(currentRequest.getRequest(), (ItemExportBlock) port);
                } else {
                    plan = planner.createGoodsResupplyPlan(currentRequest.getRequest(), (ItemImportBlock) port);
                }

                // If the plan can't be created store it at the end of the list. The function tries to create new
                // plan (for this request) at its next call.
                if (plan == null) {
                    System.out.printf("Creation of the plan failed! Will try later. The request: %s\n", currentRequest);
                    requests.addLast(currentRequest);
                } else {
                    plans.add(plan);
                }
            } catch (PathPlanningException e) {
                System.err.println("Cannot create path to satisfy the request. Will try later.");
                requests.addLast(currentRequest);
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
    }

    public InfoGetterUI.ForkliftRoute getForkliftRoute(int id) {
        InfoGetterUI.ForkliftRoute route = new InfoGetterUI.ForkliftRoute();

        for (var plan : plans) {
            if (plan.getAssociatedForklift().getID() == id) {
                // First route is from current position
                Arrays.stream(plan.getCurrentStop().getPathFromLastStop()).skip(plan.getCurrentStop().getPathPosition()).map(c -> new CoordinatesUI(c.getX(), c.getY()))
                        .forEach(c -> route.route.add(c));
                // Add full route of next stops
                plan.getRemainingStopsTillCompletion().stream().skip(1).limit(2).map(ForkliftPlanStop::getPathFromLastStop)
                        .forEach(pth -> {
                            Arrays.stream(pth).map(c -> new CoordinatesUI(c.getX(), c.getY()))
                                    .forEach(c -> route.route.add(c));
                        });
                // Add stops
                plan.getRemainingStopsTillCompletion().stream().limit(3).forEach(
                        stop -> route.stops.add(new CoordinatesUI(stop.getStopCoordinates().getX(), stop.getStopCoordinates().getY()))
                );
                break;
            }
        }

        return route;
    }


    @Override
    public synchronized boolean timeChanged(double time) {

        timeChange = true;
        newTime = time;
        if (newTime < this.time) {
            reset = true;
        }
        notify();
//        try {
//            wait();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        return true;
    }

    @Override
    public void speedChanged(double speed) {
        this.speed = speed;
    }

    @Override
    public synchronized boolean roadBlock(int x, int y) {
        blockedRoad = new Coordinates(x, y);
        return true;
    }

    @Override
    public void newRequest(RequestType type, Map<String, Integer> goods) {
        Request request = new Request(Request.RequestType.convert(type));
        for (String goodsName : goods.keySet()) {
            Goods newGoods = new Goods(goodsName);
            RequestPart requestPart = new RequestPart(newGoods, goods.get(goodsName));
            request.addRequestPart(requestPart);
        }
        synchronized (this) {
            requests.add(request);
            tryAssigningRequest();
        }
        System.out.println(request);
    }

    @Override
    public synchronized boolean loadRequestsFromFile(File requestsFile) {
        XMLRequestLoader loader = new XMLRequestLoader(this.requests);
        try {
            loader.loadFromFile(requestsFile);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            System.err.println("Cannot load from file");
            return false;
        }
        tryAssigningRequest();
        return true;
    }

    @Override
    public synchronized void play() {
        this.paused = false;
        notify();
    }

    @Override
    public synchronized void pause() {
        this.paused = true;
    }

    @Override
    public void reset() {
        this.reset = true;
    }
}
