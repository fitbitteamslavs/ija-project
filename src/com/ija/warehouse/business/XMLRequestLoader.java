/*
Authors: Peter Močáry (xmocar00)
Purpose: A loader of requests from XML representaion.
 */
package com.ija.warehouse.business;

import com.ija.warehouse.business.goods.Goods;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XMLRequestLoader {

    List<Request> requestQueue;

    public XMLRequestLoader(List<Request> requestQueue) {
       this.requestQueue = requestQueue;
    }

    /**
     * Loads the requests from specified XML stores them in a queue.
     * @param file XML representation of the request queue
     */
    public void loadFromFile(File file) throws ParserConfigurationException, IOException, SAXException {
        Document doc = getXmlDocumentFromFile(file);
        Element root = getRootElementFromXmlDoc(doc, "request_queue");
        fillRequestQueue(root);
    }

     /**
     * Initial parsing of the file by DOM xml parser, which creates Document from where one can easily request nodes.
     * @param file xml file containing request queue representation
     * @return Dom xml parser Document
     */
    private static Document getXmlDocumentFromFile(File file) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        return docBuilder.parse(file);
    }

    /**
     * Retrieves root element and checks its name with the specified one.
     * @param doc DOM xml parser Document representing the request queue
     * @param expectedName expected root Element name
     * @return the rootElement
     * @throws RuntimeException If the expected name doesn't match the actual root Element name
     */
    private Element getRootElementFromXmlDoc(Document doc, String expectedName) throws RuntimeException {
        Element rootElement = doc.getDocumentElement();
        if (!rootElement.getNodeName().equals(expectedName)) {
            throw new RuntimeException("Expected "+expectedName+" as a root element of XML file!");
        }
        return rootElement;
    }


    /**
     * Fills the request queue with request parts form XML file. A Request is a group of
     * request parts containing goods and quantity. We fill the queue with the parts as requests.
     * @param root the root Element of XML file which contains the request queue representation
     */
    private void fillRequestQueue(Element root) {
        NodeList requestNodes = root.getElementsByTagName("request");

        for (int n = 0; n < requestNodes.getLength(); n++) {
            Node node = requestNodes.item(n);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element requestElement = (Element) node;

                String action = requestElement.getAttribute("action");

                Request request = createRequestFromParts(requestElement, action);
                requestQueue.add(request);
            }
        }
    }

    /**
     * Creates new request from the XML file and returns it.
     * @param requestElem the DOM Element representing a request
     * @param action a string containing "in" or "out" (anything other then "in" is considered as "out")
     */
    private Request createRequestFromParts(Element requestElem, String action) {

        // Convert the action to integer
        Request.RequestType actionType;
        if (action.equals("in")) {
            actionType = Request.RequestType.IMPORT;
        } else {
            actionType = Request.RequestType.EXPORT;
        }

        // Process the XML and load the
        NodeList requestPartNodes = requestElem.getElementsByTagName("request_part");
        Request request = new Request(actionType);

        for (int n = 0; n < requestPartNodes.getLength(); n++) {
            Node node = requestPartNodes.item(n);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element requestPartElement = (Element) node;

                String name = requestPartElement.getAttribute("name");
                int quantity = Integer.parseInt(requestPartElement.getAttribute("quantity"));

                Goods requesetdGoods = new Goods(name);
                RequestPart req = new RequestPart(requesetdGoods, quantity);
                request.addRequestPart(req);
            }
        }

        return request;
    }
}
