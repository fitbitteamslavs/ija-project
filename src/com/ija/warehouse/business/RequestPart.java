/*
Authors: Peter Močáry (xmocar00)
Purpose: A part of a request containing single goods type.
 */
package com.ija.warehouse.business;

import com.ija.warehouse.business.goods.Goods;

public class RequestPart {
    private Goods goods;
    private int quantity;

    public RequestPart(Goods goods, int quantity){
        this.goods = goods;
        this.quantity = quantity;
    }

    public Goods getGoods() {
        return goods;
    }

    public int getQuantity() {
        return quantity;
    }

}
