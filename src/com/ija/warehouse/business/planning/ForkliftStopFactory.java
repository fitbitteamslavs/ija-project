/*
Author: Michal Hečko (xhecko02)
Purpose: Factory for creation of stops for execution of plans for requests.
 */
package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.map.Coordinates;
import com.ija.warehouse.business.map.MapBlock;

import java.util.Map;

/**
 * Factory for forklift stops.
 * Decouples the forklift stop creation (and specific class selection) from the planning utilities.
 */
public class ForkliftStopFactory {
    public static ForkliftPlanStop createLoadItemsStop(
            Coordinates[] pathFromLastStop,
            MapBlock block,
            Map<Goods, Integer> loadedItems) {

        ForkliftAction action  = new ForkliftLoadFromStorageBlockAction(loadedItems);
        return new ForkliftPlanStop(pathFromLastStop, block, action);
    }

    public static ForkliftPlanStop createUnloadAllStop(
            Coordinates[] pathFromLastStop,
            MapBlock block) {

        ForkliftAction action = new ForkliftUnloadAllAction();
        return new ForkliftPlanStop(pathFromLastStop, block, action);
    }

    public static ForkliftPlanStop createStoreSuppliedItemsStop(
            Coordinates[] pathFromLastStop,
            MapBlock block,
            Map<Goods, Integer> suppliedItems) {

        ForkliftAction action = new ForkliftInsertItemsIntoStorageAction(suppliedItems);
        return new ForkliftPlanStop(pathFromLastStop, block, action);
    }


}
