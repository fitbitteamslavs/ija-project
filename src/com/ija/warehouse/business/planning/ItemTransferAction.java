/*
Authors: Michal Hečko (xhecko02)
Purpose: A forklift action for transefering items.
 */
package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.map.MapBlock;

import java.util.Map;

/**
 * Abstraction over forklift action that manipulates a particular subset of its inventory.
 */
public abstract class ItemTransferAction implements ForkliftAction {
    Map<Goods, Integer> actionItems;

    public ItemTransferAction(Map<Goods, Integer> itemsToTransfer) {
        this.actionItems = itemsToTransfer;
    }

    public Map<Goods, Integer> getTransferedItems() {
        return actionItems;
    }

    @Override
    public abstract int perform(Forklift forklift, MapBlock block);
}
