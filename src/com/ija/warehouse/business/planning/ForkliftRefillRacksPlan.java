package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.Request;
import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.map.ItemStorageBlock;
import com.ija.warehouse.business.map.WarehouseMap;
import com.ija.warehouse.business.utils.PlanningUtils;

import java.util.HashMap;
import java.util.Map;

public class ForkliftRefillRacksPlan extends AbstractForkliftPlan {

    public ForkliftRefillRacksPlan(Forklift associatedForklift) {
        super(associatedForklift, Request.RequestType.IMPORT);
    }

    @Override
    public Map<Goods, Integer> getRemainingItemsTillCompletion() {
        return this.getRemainingStopsTillCompletion().stream()
                .filter(stop -> stop.getAction() instanceof ForkliftInsertItemsIntoStorageAction)
                .map(stop -> (ForkliftInsertItemsIntoStorageAction) stop.getAction())
                .map(ForkliftInsertItemsIntoStorageAction::getTransferedItems)
                .reduce(PlanningUtils::sumRequests)
                .orElse(new HashMap<>());
    }

    @Override
    public Map<Goods, Integer> getOverallProcessedItems() {
        return this.stops.stream()
                .filter(stop -> stop.getAction() instanceof ForkliftInsertItemsIntoStorageAction)
                .map(stop -> (ForkliftInsertItemsIntoStorageAction) stop.getAction())
                .map(ForkliftInsertItemsIntoStorageAction::getTransferedItems)
                .reduce(PlanningUtils::sumRequests)
                .orElse(new HashMap<>());
    }

    @Override
    public void unwindPlannedWarehouseState(Map<ItemStorageBlock, Map<Goods, Integer>> reservedStates, WarehouseMap map) {
        this.getRemainingStopsTillCompletion().stream()
                .filter(stop -> stop.getAction() instanceof ForkliftInsertItemsIntoStorageAction)
                .forEach(stop -> {
                    var action = (ForkliftInsertItemsIntoStorageAction) stop.getAction();
                    var loadedItems = action.getTransferedItems();
                    var loadedItemsCount = loadedItems.values().stream().mapToInt(a -> a).sum();

                    var storageBlock = (ItemStorageBlock) stop.getTargetBlock();
                    storageBlock.freeReservedCapacity(loadedItemsCount);
                });
    }
}
