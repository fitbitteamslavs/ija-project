/*
Author: Peter Močáry (xmocar00)
Purpose: A forklift action for parking, which is NOP equivalent.
 */

package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.map.MapBlock;

/**
 * Forklift action:
 * Does not perform any block interaction.
 */
public class ForkliftParkAction implements ForkliftAction{

    public ForkliftParkAction() {
    }

    @Override
    public int perform(Forklift forklift, MapBlock block) {
        return 0;
    }
}
