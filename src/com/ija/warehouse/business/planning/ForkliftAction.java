/*
Authors: Michal Hečko (xhecko02)
Purpose: Interface for a action of a forklift.
 */
package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.map.MapBlock;

/**
 * Interface providing abstraction over an action that forklift is able to perform.
 * Used in forklift stops.
 */
public interface ForkliftAction {
    int perform(Forklift forklift, MapBlock block);
}
