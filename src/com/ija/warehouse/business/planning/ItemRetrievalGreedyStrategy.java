/*
Author: Michal Hečko (xhecko02)
Purpose: A greedy stragefy for item retrival.
 */
package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.goods.GoodsItem;
import com.ija.warehouse.business.map.Coordinates;
import com.ija.warehouse.business.map.ItemStorageBlock;
import com.ija.warehouse.business.map.MapBlock;
import javafx.util.Pair;

import java.util.*;

/**
 * Item retrieval strategy used in the planner. It works by choosing a "best" available block
 * that is reachable from the last planned position, extracting items, and repeating the process
 * until the request is satisfied, or no more satisfying blocks are reachable.
 */
public class ItemRetrievalGreedyStrategy implements StorageBlockVisitStrategy {
    private PlanningContext actualPlanningContext;
    private Set<ItemStorageBlock> alreadyVisitedBlocks;

    @Override
    public LinkedList<VisitedBlockDescription> createVisitOrder(PlanningContext planningContext) {
        actualPlanningContext = planningContext;
        alreadyVisitedBlocks = new HashSet<>();

        LinkedList<VisitedBlockDescription> visitedBlocks = new LinkedList<>();
        Coordinates lastCartStop = planningContext.getAllocatedForklift().getActualPosition();
        Map<Goods, Integer> requestSummary = new HashMap<>(planningContext.getRequestSummary());

        // Greedily select blocks starting from the last selected block until the we have no more items to fetch.
        while (!requestSummary.isEmpty()) {
            var bestBlockPathPair = getBestStorageBlock(requestSummary, lastCartStop);
            var storageBlock = bestBlockPathPair.getKey();
            var retrievedItems = this.getExtractedItemsFromStorageBlock(storageBlock, requestSummary);

            VisitedBlockDescription visitedBlockDescription = new VisitedBlockDescription(storageBlock, retrievedItems);
            visitedBlocks.add(visitedBlockDescription);

            // Update remaining goods in the request.
            ForkliftRoadPlanner.subtractItemsFromRequestSummary(requestSummary, retrievedItems);

            var stopPath = bestBlockPathPair.getValue();
            lastCartStop = stopPath[stopPath.length - 1];
        }

        return visitedBlocks;
    }

    private Map<Goods, Integer> getExtractedItemsFromStorageBlock(ItemStorageBlock storageBlock, Map<Goods, Integer> requestSummary) {
        Map<Goods, Integer> extractedItems = new HashMap<>();
        Map<Goods, List<GoodsItem>> availableInventory = storageBlock.getAvailableItems();

        for (var requestedGoodsQuantityEntry: requestSummary.entrySet()) {
            Goods requestedGoods = requestedGoodsQuantityEntry.getKey();
            Integer requestedQuantity = requestedGoodsQuantityEntry.getValue();

            List<GoodsItem> availableItems = availableInventory.get(requestedGoods);
            if (availableItems != null) {
                extractedItems.put(requestedGoods, Math.min(availableItems.size(), requestedQuantity));
            }
        }
        return extractedItems;
    }

    private double blockRankingFunction(double distance, double requestSatisfactionRatio) {
        return 1/distance * requestSatisfactionRatio;
    }

    private Pair<ItemStorageBlock, Coordinates[]> chooseBestStorageBlockFromSearchResults(
            Map<MapBlock, Coordinates[]> searchResults,
            Map<Goods, Integer> remainingRequestItems) {

        Coordinates[] pathToBestStorageBlock = null;
        ItemStorageBlock bestStorageBlock = null;
        double bestBlockHeuristicsValue = 0.0;
        for (var discoveryEntry : searchResults.entrySet()) {
            Coordinates[] currentPathToStorageBlock = discoveryEntry.getValue();
            ItemStorageBlock currentStorageBlock = (ItemStorageBlock)  discoveryEntry.getKey();

            int distance = currentPathToStorageBlock.length;

            double requestSatisfactionRatio = this.calculateRequestsSatisfiabilityRatio(remainingRequestItems, currentStorageBlock );
            double blockHeuristicsValue = this.blockRankingFunction(distance, requestSatisfactionRatio);

            if (bestStorageBlock == null) {
                bestStorageBlock = currentStorageBlock;
                pathToBestStorageBlock = currentPathToStorageBlock;
                bestBlockHeuristicsValue = blockHeuristicsValue;
            } else {
                if (blockHeuristicsValue > bestBlockHeuristicsValue) {
                    // Found a better block.
                    bestStorageBlock = currentStorageBlock;
                    pathToBestStorageBlock = currentPathToStorageBlock;
                    bestBlockHeuristicsValue = blockHeuristicsValue;
                }
            }
        }
        return new Pair<>(bestStorageBlock, pathToBestStorageBlock);
    }

    public Pair<ItemStorageBlock, Coordinates[]> getBestStorageBlock(
            Map<Goods, Integer> remainingRequestItems,
            Coordinates startAt) {

        var discoveredStorageBlocks = actualPlanningContext
                .getMap()
                .computeShortestPathsToReachableSatisfyingBlocks(
                        startAt,
                        (warehouseMap, block) -> (block instanceof ItemStorageBlock && !this.alreadyVisitedBlocks.contains(block)));

        // Select warehouse that is closest to us and has the most items available
        var block = this.chooseBestStorageBlockFromSearchResults(discoveredStorageBlocks, remainingRequestItems);
        this.alreadyVisitedBlocks.add(block.getKey());
        return block;
    }

    private double calculateRequestsSatisfiabilityRatio(Map<Goods, Integer> allRequestedGoods, ItemStorageBlock storageBlock) {
        Map<Goods, Integer> availableInventory = this.actualPlanningContext.getActualWarehouseStorageState().get(storageBlock);
        int satItemsCount = 0;
        int overallCount = 0;
        for (var requestedGoodsQuantityEntry: allRequestedGoods.entrySet()) {
            Goods requestedGoods = requestedGoodsQuantityEntry.getKey();
            Integer requestedQuantity = requestedGoodsQuantityEntry.getValue();

            int satisfiableQuantity = 0;
            if (availableInventory.containsKey(requestedGoods)) {
                Integer availableItems = availableInventory.get(requestedGoods);
                satisfiableQuantity = Math.min(availableItems, requestedQuantity);
            }

            satItemsCount += satisfiableQuantity;
            overallCount += requestedQuantity;
        }
        return satItemsCount / (double) overallCount;
    }
}
