/*
Authors: Michal Hečko (xhecko02), Dávid Lacko (xlacko09), Peter Močáry (xmocar02)
Purpose: A class that implements algorithms performing planning for a forklift (or other map entity) to fulfill the required collection of requirements.
 */
package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.Request;
import com.ija.warehouse.business.RequestPart;
import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.map.*;
import com.ija.warehouse.business.utils.PlanningUtils;
import javafx.util.Pair;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class responsible for planning action plans for individual forklifts. Can create plans for goods retrieval
 * and goods import along with the ability to adapt the plans when an obstacle is placed in the map.
 *
 * As a planner it is responsible for keeping tracks of reserved goods (planning context) so that the forklifts
 * will not compete over goods.
 */
public class ForkliftRoadPlanner {
    private Map<ItemStorageBlock, Map<Goods, Integer>> actualWarehouseStorageState;
    private final List<Forklift> availableForklifts;
    private final WarehouseMap warehouse;

    /**
     * Creates the planner for given warehouse that has available the given forklifts.
     *
     * @param warehouse Warehouse map over in which are the created plans executed.
     * @param forklifts The list of available forklifts for usage.
     */
    public ForkliftRoadPlanner(WarehouseMap warehouse, List<Forklift> forklifts) {
        this.warehouse = warehouse;
        this.availableForklifts = forklifts;
        this.createPlanningContextFromWarehouseState(warehouse);
    }

    /**
     * Add a forklift to the (internal) pool of available forklifts to use when
     * creating a plan.
     * @param forklift
     */
    public void addAvailableForklift(Forklift forklift) {
        this.availableForklifts.add(forklift);
    }

    /**
     * Retrieves the next available forklift that will be used when requesting a new plan creation.
     * The forklift is not removed from the (internal) collection of available forklifts.
     * @return The next used forklift.
     */
    public Forklift getNextAvailableForklift() {
        return this.availableForklifts.get(availableForklifts.size() - 1);
    }

    /**
     * Possibly perform a plan adaptation when an obstacle has been placed somewhere in the map.
     * @param plan The plan which should be checked for satisfiability.
     * @param obstacleCoordinates Coordinates at which the obstacle was placed.
     * @return  Adapted plan (might be the same as passed via argument if not adaptation is needed) or null if the plan
     *          cannot be adapted. This might happen if some of the remaining goods to be fetches/imported cannot be transfered
     *          - all paths to block that have them available were blocked.
     */
    public AbstractForkliftPlan adaptPlan(AbstractForkliftPlan plan, Coordinates obstacleCoordinates) {
        var brokenStops = plan.atWhichStopsRoadPassesThroughPoint(obstacleCoordinates);
        if (brokenStops.isEmpty()) return plan;  // Plan was not affected by the map change.

        // Insert back the reserved items
        plan.unwindPlannedWarehouseState(actualWarehouseStorageState, warehouse);

        var remainingItems = plan.getRemainingItemsTillCompletion();

        // Reuse the same Export block
        var exportBlock = plan.getPort();
        exportBlock.free();
        if (plan.getType() == Request.RequestType.EXPORT) {
            // Recreate the plan with given export block.
            return this.createGoodsRetrievalPlan(remainingItems, (ItemExportBlock) exportBlock, plan.getAssociatedForklift());
        } else {
            ForkliftRefillRacksPlan newPlan = new ForkliftRefillRacksPlan(plan.getAssociatedForklift());
            boolean didCurrentInventoryRedistribution = false;
            // Check that the forklift is currently not caring any items -- otherwise the create plan
            // will actually fetch them twice from the import block.
            if (plan.getCurrentStop().getAction() instanceof ForkliftInsertItemsIntoStorageAction) {
                // Choose the closest available storage, that is capable of storing enough items.
                var planToDistributeRest = ItemRefillStrategy.createPlanToDistributeCurrentForkliftContents(plan.getAssociatedForklift(), this.warehouse);
                if (planToDistributeRest == null) return null; // This deserves a better handling, however no time
                remainingItems = PlanningUtils.retrievalItemListsDifference(remainingItems, plan.getAssociatedForklift().getInventoryDescription()).get();

                if (remainingItems.isEmpty()) {
                    createHomeStopFromCurrentLocation(planToDistributeRest, plan.getAssociatedForklift());
                    return planToDistributeRest;
                } else {
                    newPlan.chainWith(planToDistributeRest);
                    didCurrentInventoryRedistribution = true;
                }
            }

            Coordinates terminalPoint = didCurrentInventoryRedistribution ? newPlan.getPlanTerminalPoint() : null;
            return this.createGoodsResupplyPlan(remainingItems, (ItemImportBlock) exportBlock, plan.getAssociatedForklift(), terminalPoint);
        }
    }

    /**
     * Create a plan to insert items specified in the request into suitable storage blocks.
     * @param request The goods and the corresponding quantity to be imported from an import block and distributed into the warehouse.
     * @param importBlock The block from which the items will be imported.
     * @return The complete created plan for the distribution of requested items. Might be null, if the storage does not
     *         have suitable capacity or no forklifts are currently available.
     */
    public ForkliftRefillRacksPlan createGoodsResupplyPlan(Collection<RequestPart> request, ItemImportBlock importBlock) {
        if (this.availableForklifts.isEmpty()) return null;

        Forklift forklift = this.availableForklifts.remove(this.availableForklifts.size() - 1);
        return createGoodsResupplyPlan(convertRequestsToSummaryMap(request), importBlock, forklift, null);
    }

    /**
     * Create a plan to insert items specified in the request into suitable storage blocks.
     * @param requestSummary The requested goods with their corresponding quantity.
     * @param importBlock The block from which the items will be imported.
     * @param forklift Force the plan to be associated to this particular forklift. If the argument is null a forklift will be selected
     *                 from the collection of available forklifts (that have no plans attached to them)
     * @return The complete created plan for the distribution of requested items. Might be null, if the storage does not
     *         have suitable capacity or no forklifts are currently available.
     */
    public ForkliftRefillRacksPlan createGoodsResupplyPlan(Map<Goods, Integer> requestSummary, ItemImportBlock importBlock, Forklift forklift, Coordinates startAt) {

        ItemRefillStrategy refillStrategy = new ItemRefillStrategy();
        StorageBlockVisitStrategy.PlanningContext ctx = new StorageBlockVisitStrategy.PlanningContext(
                this.warehouse,
                this.actualWarehouseStorageState,
                requestSummary,
                forklift
        );

        if (startAt == null) {
            startAt = forklift.getActualPosition();
        }

        var blocksToVisit = refillStrategy.createVisitOrder(ctx);
        if (blocksToVisit == null) return null;

        var usedStorageBlocks = blocksToVisit.stream()
                .map(visitedBlockDescription -> new Pair<>(visitedBlockDescription.getRetrievedItems(), visitedBlockDescription.getStorageBlock()))
                .collect(Collectors.toList());

        var visitBatches = this.createStorageBlocksGroupsToMatchCapacity(
                usedStorageBlocks, forklift);

        ForkliftRefillRacksPlan plan = new ForkliftRefillRacksPlan(forklift);
        Coordinates currentPosition = startAt;
        plan.setPort(importBlock.getMapBlock());
        for (var visitBatch : visitBatches) {
            ForkliftRefillRacksPlan batchPlan = new ForkliftRefillRacksPlan(forklift);

            // Sum up the items inside this batch, so we can load them at once from the ImportBlock
            Map<Goods, Integer> batchSummary = visitBatch.stream().map(Pair::getKey).reduce(PlanningUtils::sumRequests).get();

            // The forklift might be located somewhere in the map, bring it to the item import block
            var maybePathToImportBlock = this.warehouse.findShortestPathBetweenBlocks(currentPosition, importBlock.getMapBlock().getCoordinates());
            if (maybePathToImportBlock.isEmpty())
                throw new RuntimeException("Could not find a path to given import block.");

            var travelToItemImportBlock = ForkliftStopFactory.createLoadItemsStop(maybePathToImportBlock.get(), importBlock.getMapBlock(), batchSummary);
            batchPlan.addStop(travelToItemImportBlock);

            currentPosition = travelToItemImportBlock.getStopCoordinates();

            for (var suppliedGoodsListBlockPair : visitBatch) {
                var suppliedGoodsList = suppliedGoodsListBlockPair.getKey();
                var targetBlock = suppliedGoodsListBlockPair.getValue();

                var targetBlockCoordinates = targetBlock.getMapBlock().getCoordinates();
                var maybeRoadFromLastStop = this.warehouse.findShortestPathBetweenBlocks(currentPosition, targetBlockCoordinates);
                if (maybeRoadFromLastStop.isEmpty())
                    throw new RuntimeException("Unable to find a path between two storage blocks while planning.");

                var roadFromLastStop = maybeRoadFromLastStop.get();

                // We need to reserve the capacity in this block, so that when planning multiple requests,
                // they cannot compete for capacity.
                targetBlock.reserveCapacity(suppliedGoodsList.values().stream().mapToInt(i -> i).sum());

                var unloadStop = ForkliftStopFactory.createStoreSuppliedItemsStop(roadFromLastStop, targetBlock.getMapBlock(), suppliedGoodsList);
                batchPlan.addStop(unloadStop);

                currentPosition = unloadStop.getStopCoordinates();
            }

            plan.chainWith(batchPlan);
        }
        importBlock.getMapBlock().reserve();

        createHomeStopFromCurrentLocation(plan, forklift);
        return plan;
    }

    /**
     * Creates a plan for a forklift to retrieve the required items from the warehouse and transport them to an item export block.
     *
     * @param request Collection of requested goods along with the corresponding quantity to retrieve from the warehouse.
     * @return A complete goods retrieval plan. Can be null if the requests cannot be satisfied
     *         (no forklifts available or not enough items in the warehouse).
     */
    public ForkliftGoodsRetrievalPlan createGoodsRetrievalPlan(Collection<RequestPart> request) {
        return this.createGoodsRetrievalPlan(request, null);
    }

    /**
     * Creates a plan for a forklift to retrieve the required items from the warehouse and transport them to an item export block.
     *
     * @param request Collection of requested goods along with the corresponding quantity to retrieve from the warehouse.
     * @param exportBlock Force a particular block to be used as a destination for the retrieved items.
     * @return A complete goods retrieval plan. Can be null if the requests cannot be satisfied
     *         (no forklifts available or not enough items in the warehouse).
     */
    public ForkliftGoodsRetrievalPlan createGoodsRetrievalPlan(Collection<RequestPart> request, ItemExportBlock exportBlock) {
        var requestSummary = convertRequestsToSummaryMap(request);
        if (this.availableForklifts.isEmpty()) {
            return null;
        }
        return this.createGoodsRetrievalPlan(requestSummary, exportBlock, this.availableForklifts.remove(availableForklifts.size() - 1));
    }

    /**
     * Creates a plan for a forklift to retrieve the required items from the warehouse and transport them to an item export block.
     *
     * @param requestSummary The requested goods along with the corresponding quantity to retrieve from the warehouse.
     * @param exportBlock   Force a particular block to be used as a destination for the retrieved items.
     * @param forklift      Force a forklift to be associated with the created plan. If null, the planner will use a forklift from the
     *                      internal collection of available forklifts.
     * @return A complete goods retrieval plan. Can be null if the requests cannot be satisfied
     *         (no forklifts available or not enough items in the warehouse).
     */
    public ForkliftGoodsRetrievalPlan createGoodsRetrievalPlan(Map<Goods, Integer> requestSummary, ItemExportBlock exportBlock, Forklift forklift) {
        boolean isForkliftAvailable = this.availableForklifts.size() > 0;
        if (!(areRequestedItemsAvailable(requestSummary) && (isForkliftAvailable || forklift != null))) {
            return null;
        }

        Coordinates startAt = forklift.getActualPosition();

        var strategy = new ItemRetrievalGreedyStrategy();
        var ctx = new StorageBlockVisitStrategy.PlanningContext(
                this.warehouse,
                this.actualWarehouseStorageState,
                requestSummary,
                forklift
        );

        var blocksFromStrategy = strategy.createVisitOrder(ctx);

        // Get a list of blocks that will have to be visited in order for the plan to be satisfied.
        var blocksToVisit = blocksFromStrategy.stream()
                .map(vbd -> new Pair<>(vbd.getRetrievedItems(), vbd.getStorageBlock()))
                .collect(Collectors.toList());

        blocksFromStrategy.forEach(vbd -> updatePlanningContextAfterStop(vbd.getStorageBlock(), vbd.getRetrievedItems()));

        // Group visited blocks so that the forklift will not exceed capacity in one turn.
        var retrievalBatches = createStorageBlocksGroupsToMatchCapacity(blocksToVisit, forklift);

        // Localize Export port to unload the items into -- choose arbitrary
        if (exportBlock == null) {
            for (var eb : this.warehouse.getItemExportBlocks().values()) {
                exportBlock = eb;
                break; // Unfortunately there is no better way to do this afaik.
            }
        }

        if (exportBlock == null) throw new PathPlanningException("No item export blocks found!");

        // For each of these groups create one atomic retrieval plan.
        Coordinates origin = startAt;
        ForkliftGoodsRetrievalPlan retrievalPlan = new ForkliftGoodsRetrievalPlan(forklift);
        retrievalPlan.setPort(exportBlock.getMapBlock());
        for (var retrievalBatch : retrievalBatches) {
            var plan = createRetrievalPlanFromBlocksToVisit(forklift, origin, retrievalBatch);
            var currentPlanTerminalPoint = plan.getPlanTerminalPoint();

            // The forklift loaded all items and waits by the last rack, we need to add one more stop - to the export block
            var maybePathToExportBlock = this.warehouse.findShortestPathBetweenBlocks(currentPlanTerminalPoint, exportBlock.getMapBlock().getCoordinates());
            if (maybePathToExportBlock.isEmpty())
                throw new PathPlanningException("Could not find a path between last block and export block.");

            var unloadAllStop = ForkliftStopFactory.createUnloadAllStop(maybePathToExportBlock.get(), exportBlock.getMapBlock());
            plan.addStop(unloadAllStop);

            // This partial plan is complete -- add it as a sub-plan into the overall plan
            retrievalPlan.chainWith(plan);
            origin = plan.getPlanTerminalPoint();  // Next time we will start at this point.
        }
        exportBlock.getMapBlock().reserve();

        createHomeStopFromCurrentLocation(retrievalPlan, forklift);
        return retrievalPlan;
    }

    /**
     * Creates a goods retrieval plan from the previously identified storage block to be used for the plan.
     * @param forklift      Forklift associated with the plan.
     * @param origin        Coordinates, from which the starting process will start.
     * @param blocksToVisit Collection of storage along with the goods and the corresponding quantity to fetch from the block.
     * @return              The created forklift plan.
     */
    private ForkliftGoodsRetrievalPlan createRetrievalPlanFromBlocksToVisit(Forklift forklift, Coordinates origin, List<Pair<Map<Goods, Integer>, ItemStorageBlock>> blocksToVisit) {
        Coordinates stopCoors = origin;
        ForkliftGoodsRetrievalPlan plan = new ForkliftGoodsRetrievalPlan(forklift);
        for (var blockGoodsPair : blocksToVisit) {
            Map<Goods, Integer> goodsToRetrieve = blockGoodsPair.getKey();
            ItemStorageBlock storageBlock = blockGoodsPair.getValue();

            var maybePath = this.warehouse.findShortestPathBetweenBlocks(stopCoors, storageBlock.getMapBlock().getCoordinates());
            if (maybePath.isEmpty()) {
                throw new RuntimeException("This should not happen - failed to find path between two storage blocks (which was previously found)");
            }

            var path = maybePath.get();
            var stop = ForkliftStopFactory.createLoadItemsStop(path, storageBlock.getMapBlock(), goodsToRetrieve);
            plan.addStop(stop);

            stopCoors = path[path.length - 1];
        }

        return plan;
    }


    /**
     * Breaks down the storage block visits into multiple if the visit would load more items than the capacity of the
     * forklift is.
     * @param forklift      Forklift responsible for visiting the blocks.
     * @param blocksToVisit Collection of storage blocks to visit along with the goods and its quantity to fetch from the storage blocks.
     * @return The visit list containing no visits that load more items than the capacity of the forklift..
     */
    private List<Pair<Map<Goods, Integer>, ItemStorageBlock>> breakBlockVisitsToMatchForkliftCapacity(
            Forklift forklift,
            List<Pair<Map<Goods, Integer>, ItemStorageBlock>> blocksToVisit) {

        final int capacity = forklift.getMaxItemCapacity();

        List<Pair<Map<Goods, Integer>, ItemStorageBlock>> normalizedVisits = new ArrayList<>(blocksToVisit.size());
        for (var blockGoodsPair : blocksToVisit) {
            var retrievedItems = blockGoodsPair.getKey();
            int retrievedItemsCount = retrievedItems.values().stream().mapToInt(i -> i).sum();

            // Will we be able to carry the items it one take?
            if (retrievedItemsCount > capacity) {
                // If not, we will have to split it into multiple visits
                Map<Goods, Integer> currentVisitRetrievedItems = new HashMap<>();

                int availableForkliftCapacityForThisBatch = capacity;

                for (var retrievedGoodsEntry : retrievedItems.entrySet()) {
                    int currentGoodsCount = retrievedGoodsEntry.getValue();

                    // While we were not able to transport all the items, transport them away.
                    while (currentGoodsCount > 0) {
                        int itemsInThisVisit = Math.min(availableForkliftCapacityForThisBatch, currentGoodsCount);
                        currentVisitRetrievedItems.put(retrievedGoodsEntry.getKey(), itemsInThisVisit);

                        if (itemsInThisVisit == capacity) {
                            normalizedVisits.add(new Pair<Map<Goods, Integer>, ItemStorageBlock>(currentVisitRetrievedItems, blockGoodsPair.getValue()));
                            currentVisitRetrievedItems = new HashMap<>();
                            availableForkliftCapacityForThisBatch = capacity;
                        } else if (availableForkliftCapacityForThisBatch == 0) {
                            // We filled the forklift for some reason earlier than expected
                            // TODO(Codeboy) Check this
                            normalizedVisits.add(new Pair<Map<Goods, Integer>, ItemStorageBlock>(currentVisitRetrievedItems, blockGoodsPair.getValue()));
                            currentVisitRetrievedItems = new HashMap<>();
                            availableForkliftCapacityForThisBatch = capacity;
                        } else {
                            // We loaded all the items of current goods, however there might be other goods items, keep information on
                            // how much space we have left in the forklift.
                            availableForkliftCapacityForThisBatch -= itemsInThisVisit;
                        }

                        currentGoodsCount -= itemsInThisVisit;
                    }
                }

                if (!currentVisitRetrievedItems.isEmpty()) {
                    normalizedVisits.add(new Pair<Map<Goods, Integer>, ItemStorageBlock>(currentVisitRetrievedItems, blockGoodsPair.getValue()));
                }
            } else {
                normalizedVisits.add(blockGoodsPair);
            }
        }

        return normalizedVisits;
    }

    /**
     * Given a list of storage blocks and the items to load from them, creates groups which can be visited at once before
     * needing to return the items to the export block due to the capacity reasons.
     * @param blocksToVisit A list of blocks to visit along with the retrieved items.
     * @param forklift The forklift responsible for retrieving the requested items.
     * @return List containing batches of storage block visits.
     */
    private List<List<Pair<Map<Goods, Integer>, ItemStorageBlock>>> createStorageBlocksGroupsToMatchCapacity(
            List<Pair<Map<Goods, Integer>, ItemStorageBlock>> blocksToVisit,
            Forklift forklift) {

        blocksToVisit = breakBlockVisitsToMatchForkliftCapacity(forklift, blocksToVisit);

        // The forklift might not be empty when we are trying to create groups (e.g. we are performing a re-plan
        // after a obstacle have been placed)
        int currentBatchItemCount = forklift.getCurrentItemCount();
        final int limit = forklift.getMaxItemCapacity();

        List<Pair<Map<Goods, Integer>, ItemStorageBlock>> currentBatch = new ArrayList<>();
        List<List<Pair<Map<Goods, Integer>, ItemStorageBlock>>> batches = new ArrayList<>();

        for (var blockToVisit : blocksToVisit) {
            var loadedGoods = blockToVisit.getKey();
            var block = blockToVisit.getValue();
            int itemCount = loadedGoods.values().stream().mapToInt(i -> i).sum();

            if (currentBatchItemCount + itemCount > limit) {
                batches.add(currentBatch);

                // Create new batch.
                currentBatch = new ArrayList<>();
                currentBatch.add(blockToVisit);
                currentBatchItemCount = itemCount;
            } else {

                // The forklift can carry more items.
                currentBatch.add(blockToVisit);
                currentBatchItemCount += itemCount;
            }
        }

        if (!currentBatch.isEmpty()) {
            batches.add(currentBatch);
        }

        return batches;
    }

    /**
     * Create the internal planning context that is used for retrieved items retrieved.
     * @param warehouseMap The warehouse map in which the planning will be performed.
     */
    private void createPlanningContextFromWarehouseState(WarehouseMap warehouseMap) {
        this.actualWarehouseStorageState = new HashMap<>();
        warehouseMap.getItemStorageBlocks().forEach((key, block) -> {
            Map<Goods, Integer> inventory = new HashMap<>();
            block.getAvailableItems().forEach((key1, value) -> inventory.put(key1, value.size()));
            this.actualWarehouseStorageState.put(block, inventory);
        });
    }

    /**
     * Subtracts the retrieved items from the overall requested items (in situ).
     * @param requestSummary The overall requested items.
     * @param retrievedItems The items that are retrieved in this stop/turn.
     */
    public static void subtractItemsFromRequestSummary(Map<Goods, Integer> requestSummary, Map<Goods, Integer> retrievedItems) {
        for (Map.Entry<Goods, Integer> goodsIntegerEntry : retrievedItems.entrySet()) {
            Goods goods = goodsIntegerEntry.getKey();
            Integer retrievedAmount = goodsIntegerEntry.getValue();
            int actualAmount = requestSummary.get(goods);
            int remainingAmount = actualAmount - retrievedAmount;
            if (remainingAmount == 0) {
                requestSummary.remove(goods);
            } else {
                requestSummary.put(goods, actualAmount - retrievedAmount);  // Update the value
            }
        }
    }

    /**
     * Given a collection of requested goods along with their quantity, create a summary representation of the requested goods.
     * @param requests The collection of requested items.
     * @return The map of requested goods along with the requested quantity.
     */
    private Map<Goods, Integer> convertRequestsToSummaryMap(Collection<RequestPart> requests) {
        Map<Goods, Integer> remainingGoodsQuantity = new HashMap<>();
        requests.forEach((request) -> {
            Goods curRequestGoods = request.getGoods();
            if (remainingGoodsQuantity.containsKey(curRequestGoods)) {
                Integer newQuantity = remainingGoodsQuantity.get(curRequestGoods) + request.getQuantity();
                remainingGoodsQuantity.put(curRequestGoods, newQuantity);
            } else {
                remainingGoodsQuantity.put(curRequestGoods, request.getQuantity());
            }
        });
        return remainingGoodsQuantity;
    }

    /**
     * Checks, whether the request collection can be satisfied using the current warehouse state (map).
     *
     * @param requestsSummary A collection of requested items from the warehouse.
     * @return True, if the request collection can be fully satisfied, false otherwise.
     */
    public boolean areRequestedItemsAvailable(Map<Goods, Integer> requestsSummary) {
        var maybeOverallAvailableQuantity = this.actualWarehouseStorageState.values().stream()
                .reduce(PlanningUtils::sumRequests);
        if (maybeOverallAvailableQuantity.isEmpty()) {
            return false;
        }

        var overallQuantity = maybeOverallAvailableQuantity.get();
        var difference = PlanningUtils.retrievalItemListsDifference(overallQuantity, requestsSummary);
        // The difference is non present if the quantity is insufficient or if the  requested goods are not even present.

        return difference.isPresent();
    }

    /**
     * After the plan stop was created (and the creation process should not crash) marks the removed goods as no longer
     * available in the planning context.
     */
    private void updatePlanningContextAfterStop(ItemStorageBlock block, Map<Goods, Integer> removedItems) {
        var actualBlockState = this.actualWarehouseStorageState.get(block);
        removedItems.forEach((goods, quantity) -> {
            var actualAmount = actualBlockState.get(goods);
            var newAmount = actualAmount - quantity;
            if (newAmount == 0) {
                actualBlockState.remove(goods);
            } else {
                actualBlockState.put(goods, newAmount);
            }
        });
    }

    /**
     * Create a new forklift stop in the plan that contains the route home.
     * @implNote Checks whether the last coordinate of the plan so far is not already home coordinate and will not create
     *           a new stop in that case.
     * @param plan      The plan in which the stop should be created.
     * @param forklift  The forklift for which the go-home stop should be created.
     */
    private void  createHomeStopFromCurrentLocation(ForkliftPlan plan, Forklift forklift) {
        Coordinates startPoint;
        if (plan.getStops().isEmpty()) {
            startPoint = forklift.getActualPosition();
        } else {
            startPoint = plan.getPlanTerminalPoint();
            if (plan.getPlanTerminalPoint().equals(forklift.getHomePosition())) {
                return;
            }
        }

        createHomeStopFromLocation(plan, startPoint, forklift.getHomePosition());
    }

    private void createHomeStopFromLocation(ForkliftPlan plan, Coordinates startPoint, Coordinates homePosition) {
        // Find the shortest route to the forklift home
        var maybePath = warehouse.getPathReachingBlockSatisfyingPredicate(
                startPoint,
                homePosition);
        if (maybePath.isEmpty()) {
            throw new RuntimeException("Couldn't locate the home for the forklift!");
        }

        var pathToHome = maybePath.get();

        ForkliftParkAction park = new ForkliftParkAction();
        plan.addStop(new ForkliftPlanStop(pathToHome, null, park));
    }
}