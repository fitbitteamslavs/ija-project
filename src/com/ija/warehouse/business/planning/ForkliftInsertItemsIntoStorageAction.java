/*
Authors: Michal Hečko (xhecko02), Dávid Lacko (xlacko02)
Purpose: A forklift action for inserting items into storage.
 */
package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.goods.GoodsItem;
import com.ija.warehouse.business.map.ItemStorageBlock;
import com.ija.warehouse.business.map.MapBlock;

import java.util.Collection;
import java.util.Map;

/**
 * Forklift action:
 * Insert some of the items contained in the forklift inventory into the provided item storage block.
 */
public class ForkliftInsertItemsIntoStorageAction extends ItemTransferAction{
    public ForkliftInsertItemsIntoStorageAction(Map<Goods, Integer> itemsToTransfer) {
        super(itemsToTransfer);
    }

    @Override
    public int perform(Forklift forklift, MapBlock block) {
        if (block instanceof ItemStorageBlock) {
            var storageBlock = (ItemStorageBlock) block;
            var availableCapacity = storageBlock.getAvailableCapacityByHeight().stream().mapToInt(a -> a).sum();
            var requiredCapacity = this.actionItems.values().stream().mapToInt(a -> a).sum();

            // We cannot place the items there... Some kind of planning error?
            if (requiredCapacity > availableCapacity) {
                return -1;
            }

            var maxFoundObj = new Object() {
                int maxFound = 0;
            };

            this.actionItems.forEach((goods, transferQuantity) -> {
                Collection<GoodsItem> items = forklift.getItemsOfGoods(goods, transferQuantity);
                for (var item : items) {
                    int val = storageBlock.storeItem(item);
                    if (val > maxFoundObj.maxFound) {
                        maxFoundObj.maxFound = val;
                    }
                }
            });
            storageBlock.hit();
            return maxFoundObj.maxFound;
        }
        return -1;
    }
}
