/*
Author: Dávid Lacko (xlacko09)
Purpose: Exception for failed planning of a request execution.
 */
package com.ija.warehouse.business.planning;

public class PathPlanningException extends RuntimeException{
    public PathPlanningException(String s) {
        super(s);
    }
}
