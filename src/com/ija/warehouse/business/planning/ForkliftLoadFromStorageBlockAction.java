/*
Authors: Michal Hečko (xhecko02), Dávid Lacko (xlacko02)
Purpose: A forklift action for loading items from storage.
 */
package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.goods.GoodsItem;
import com.ija.warehouse.business.map.ItemImportBlock;
import com.ija.warehouse.business.map.ItemStorageBlock;
import com.ija.warehouse.business.map.MapBlock;

import java.util.Map;


/**
 * Forklift action:
 * Retrieve specified items from the given item storage block and insert them into our internal inventory.
 */
public class ForkliftLoadFromStorageBlockAction extends ItemTransferAction {
    public ForkliftLoadFromStorageBlockAction(Map<Goods, Integer> itemsToTransfer) {
        super(itemsToTransfer);
    }

    @Override
    public int perform(Forklift forklift, MapBlock block) {
        if (block instanceof ItemStorageBlock) {
            var storageBlock = (ItemStorageBlock) block;

            // TODO: Check beforehand whether we can retrieve the items
            int maxFloor = 0;
            for (Map.Entry<Goods, Integer> entry : this.actionItems.entrySet()) {
                var goods = entry.getKey();
                var amount = entry.getValue();
                for (int i = 0; i < amount; i++) {
                    var storedItem = storageBlock.retrieveSomeItemOfGoods(goods);
                    if (storedItem.getKey() > maxFloor) {
                        maxFloor = storedItem.getKey();
                    }
                    forklift.storeItem(storedItem.getValue());
                }
            }
            storageBlock.hit();
            return maxFloor;
        } else if (block instanceof ItemImportBlock) {
            actionItems.forEach((key,value) -> {
                for(int i = 0; i < value; i++) {
                    forklift.storeItem(new GoodsItem(key));
                }
            });
            return 1;
        }
        return -1;
    }

    public Map<Goods, Integer> getItemsToLoad() {
        return actionItems;
    }
}
