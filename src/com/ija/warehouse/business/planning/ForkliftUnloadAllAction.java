/*
Authors: Michal Hečko (xhecko02)
Purpose: A forklift action for unloading items.
 */
package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.map.ItemExportBlock;
import com.ija.warehouse.business.map.MapBlock;

/**
 * Forklift action:
 * Forklift should unload all of its inventory inside the target item export block.
 */
public class ForkliftUnloadAllAction implements ForkliftAction{
    public ForkliftUnloadAllAction() {
    }

    @Override
    public int perform(Forklift forklift, MapBlock block) {
        if (block instanceof ItemExportBlock) {
            var exportBlock = (ItemExportBlock) block;
            var forkliftContents = forklift.moveAllItemsOut();
            forkliftContents.forEach(exportBlock::exportItem);
            return 0;
        }

        return -1;
    }
}
