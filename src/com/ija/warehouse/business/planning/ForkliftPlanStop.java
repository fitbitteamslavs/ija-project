/*
Authors: Michal Hečko (xhecko02), Dávid Lacko (xlacko09), Peter Močáry (xmocar00)
Purpose: A stop in plan of request execution by forklift.
 */

package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.map.Coordinates;
import com.ija.warehouse.business.map.MapBlock;

/**
 * One stop in the forklift movement plan.
 * Contains the path from the previous stop and the action that should a forklift perform at this stop, along with the action target block.
 */
public class ForkliftPlanStop {
    private Coordinates[] pathFromLastStop;
    private int pathPosition = 0;
    private final MapBlock targetBlock;
    private final ForkliftAction action;

    public ForkliftPlanStop(Coordinates[] pathFromLastStop, MapBlock stopTargetBlock, ForkliftAction action) {
        this.pathFromLastStop = pathFromLastStop;
        this.targetBlock = stopTargetBlock;
        this.action = action;
    }

    public Coordinates[] getPathFromLastStop() {
        return pathFromLastStop;
    }

    public void setPathFromLastStop(Coordinates[] path) {
        this.pathFromLastStop = path;
    }

    public Coordinates getNextPosition() {
        if (pathPosition < pathFromLastStop.length) {
            return pathFromLastStop[pathPosition];
        }
        return null;
    }

    public void advance() {
        if (pathPosition < pathFromLastStop.length) {
            pathPosition++;
        }
    }

    public int getPathPosition() {
        return pathPosition;
    }

    public MapBlock getTargetBlock() {
        return targetBlock;
    }

    public ForkliftAction getAction() {
        return action;
    }

    public Coordinates getStopCoordinates() {
        return this.pathFromLastStop[this.pathFromLastStop.length - 1];
    }

}
