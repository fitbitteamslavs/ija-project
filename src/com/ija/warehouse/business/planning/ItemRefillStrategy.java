/*
Authors: Michal Hečko (xhecko02)
Purpose: A strategy for refilling items.
 */
package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.map.Coordinates;
import com.ija.warehouse.business.map.ItemStorageBlock;
import com.ija.warehouse.business.map.WarehouseMap;
import com.ija.warehouse.business.utils.PlanningUtils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Encapsulates the process of the selection of the item storage blocks into which the provided items
 * should be distributed.
 */
public class ItemRefillStrategy implements StorageBlockVisitStrategy{
    private PlanningContext actualPlanningContext;

    @Override
    public LinkedList<VisitedBlockDescription> createVisitOrder(PlanningContext context) {
        actualPlanningContext = context;
        var availableStorageBlocks = context.getMap().computeShortestPathsToReachableStorageBlocks(
                context.getAllocatedForklift().getActualPosition());

        // Rank the blocks so we can choose better
        Map<ItemStorageBlock, Double> blocksRankings = new HashMap<>();
        availableStorageBlocks.forEach((block, c) -> {
            var currentInventory = ((ItemStorageBlock) block).describeInventory();
            var availableCapacity = ((ItemStorageBlock) block).getAvailableCapacity();
            var goodsCommonWithSuppliedGoods = currentInventory.keySet().stream().filter(context.getRequestSummary()::containsKey).count();
            var distance = c.length;

            // Rank the current storage block:
            // - The higher the distance, the smaller is its ranking (1/distance)
            // - The more goods common with the request the better
            // - The more available capacity the better
            double currentBlockRanking = 1 / (double) distance  * goodsCommonWithSuppliedGoods * availableCapacity;
            blocksRankings.put((ItemStorageBlock) block, currentBlockRanking);
        });

        // From the ranked blocks choose the best ones
        LinkedList<ItemStorageBlock> blocksSortedByRank = new LinkedList<>(blocksRankings.keySet());
        blocksSortedByRank.sort((left, right) -> {
            return (int) (blocksRankings.get(right) - blocksRankings.get(left));
        });

        // Traverse through these blocks one by another and identify the blocks we extract
        var startAt = actualPlanningContext.getAllocatedForklift().getActualPosition();

        Map<Goods, Integer> remainingGoods = new HashMap<>(actualPlanningContext.getRequestSummary());
        LinkedList<VisitedBlockDescription> chosenBlocks = new LinkedList<>();
        while (!remainingGoods.isEmpty()) {
            if (blocksSortedByRank.isEmpty()) {
                // We did not satisfy the requests, and no more reachable blocks are available
                return null;
            }

            ItemStorageBlock block = blocksSortedByRank.removeFirst();
            Map<Goods, Integer> itemsStoredInThisBlock = getItemsStoredInBlock(remainingGoods, block);
            if (itemsStoredInThisBlock == null) continue;

            VisitedBlockDescription vbd = new VisitedBlockDescription(block, itemsStoredInThisBlock);
            chosenBlocks.add(vbd);

            remainingGoods = PlanningUtils.retrievalItemListsDifference(remainingGoods, itemsStoredInThisBlock).get();
        }

        return chosenBlocks;
    }

    private static Map<Goods, Integer> getItemsStoredInBlock(Map<Goods, Integer> remainingGoods, ItemStorageBlock block) {
        Map<Goods, Integer> itemsStoredInThisBlock = new HashMap<>();
        int availableCapacityForStorageBlock = block.getAvailableCapacity();

        if (availableCapacityForStorageBlock == 0) {
            return null;
        }

        // TODO(maybe) Priority on placement of goods items that are already stored in this storage block
        for (var goods : remainingGoods.keySet()) {
            int suppliedAmount = remainingGoods.get(goods);
            int storedAmount = Math.min(suppliedAmount, availableCapacityForStorageBlock);
            itemsStoredInThisBlock.put(goods, storedAmount);

            availableCapacityForStorageBlock -= storedAmount;
            if (availableCapacityForStorageBlock == 0) break; // We have exhausted the whole block capacity
        }
        return itemsStoredInThisBlock;
    }

    public static ForkliftRefillRacksPlan createPlanToDistributeCurrentForkliftContents(Forklift forklift, WarehouseMap map) {
        var remainingGoods = forklift.getInventoryDescription();

        ForkliftRefillRacksPlan createdPlan = new ForkliftRefillRacksPlan(forklift);
        Coordinates pos = forklift.getActualPosition();
        while (!remainingGoods.isEmpty()) {
            var maybeBlockPath = map.getClosestReachableBlockSatisfyingPredicate(pos, block -> {
                if (block instanceof ItemStorageBlock) {
                    var storage = (ItemStorageBlock) block;
                    return (storage.getAvailableCapacity() > 0);
                } else return false;
            });

            if (maybeBlockPath.isEmpty()) return null; // Cannot create such plan.

            var block = (ItemStorageBlock) maybeBlockPath.get().getValue();
            var path = maybeBlockPath.get().getKey();

            var itemsStoredInThisBlock = getItemsStoredInBlock(remainingGoods, block);

            block.reserveCapacity(itemsStoredInThisBlock.values().stream().mapToInt(a -> a).sum());
            createdPlan.addStop(ForkliftStopFactory.createStoreSuppliedItemsStop(path, block.getMapBlock(), itemsStoredInThisBlock));

            remainingGoods = PlanningUtils.retrievalItemListsDifference(remainingGoods, itemsStoredInThisBlock).get();
        }

        return createdPlan;
    }
}
