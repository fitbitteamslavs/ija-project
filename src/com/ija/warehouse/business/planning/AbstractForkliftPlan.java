package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.Request;
import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.map.Coordinates;
import com.ija.warehouse.business.map.ItemStorageBlock;
import com.ija.warehouse.business.map.MapBlock;
import com.ija.warehouse.business.map.WarehouseMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The base class for both types of supported forklift plans - goods retrieval and goods distribution
 * into the warehouse.
 *
 * The plan naturally consists of several stops (at which forklift is supposed to perform some action) and the
 * paths between them.
 */
public abstract class AbstractForkliftPlan implements ForkliftPlan {
    protected final List<ForkliftPlanStop> stops;
    protected final Forklift associatedForklift;
    protected MapBlock port;
    protected final Request.RequestType planType;

    private int currentStop = 0;

    public AbstractForkliftPlan(Forklift associatedForklift, Request.RequestType type) {
        this.associatedForklift = associatedForklift;
        stops = new ArrayList<>();
        this.planType = type;
    }

    public Request.RequestType getType() {
        return this.planType;
    }

    public void setPort(MapBlock port) {
        this.port = port;
    }

    public MapBlock getPort() {
        return this.port;
    }

    public Forklift getAssociatedForklift() {
        return associatedForklift;
    }

    public void addStop(ForkliftPlanStop stop) {
        stops.add(stop);
    }

    public ForkliftPlanStop getNextStop() {
        this.currentStop++;
        if (this.currentStop < this.stops.size()) {
            return this.stops.get(this.currentStop);
        }
        return null;
    }

    public int getStopCount() {
        return this.stops.size();
    }

    public ForkliftPlanStop getCurrentStop() {
        if (currentStop < stops.size()) return this.stops.get(this.currentStop);
        else return null;
    }

    public List<ForkliftPlanStop> getStops() {
        return this.stops;
    }

    /**
     * Checks whether we will cross the given map point in the future (the current state is taken into account).
     *
     * @param point Point which might be crossed.
     */
    public List<ForkliftPlanStop> atWhichStopsRoadPassesThroughPoint(Coordinates point) {
        List<ForkliftPlanStop> stopsPassed = new ArrayList<>();
        // Current stop might be -1, when the plan is not active yet
        for (int i = this.currentStop; i < stops.size(); i++) {
            var stop = this.stops.get(i);
            var path = stop.getPathFromLastStop();
            for (Coordinates visitedPoint : path) {
                if (visitedPoint.equals(point)) {
                    stopsPassed.add(stop);
                }
            }
        }
        return stopsPassed;
    }

    public Coordinates getPlanTerminalPoint() {
        if (this.stops.isEmpty()) throw new PathPlanningException("There are no stops in the plan.");
        return this.stops.get(this.stops.size() - 1).getStopCoordinates();
    }

    public void chainWith(ForkliftPlan plan) {
        var otherStops = plan.getStops();
        this.stops.addAll(otherStops);
    }

    public List<ForkliftPlanStop> getRemainingStopsTillCompletion() {
        return stops.subList(this.currentStop, this.stops.size());
    }

    /**
     * Describe the goods and their corresponding quantity which need to be retrieved in order
     * for the plan to be considered executed.
     * @return The description of remaining goods and their quantity.
     */
    public abstract Map<Goods, Integer> getRemainingItemsTillCompletion();

    /**
     * Describe the overall goods and their corresponding quantity retrieved in this plan.
     * @return The description of retrieved goods and their quantity.
     */
    public abstract Map<Goods, Integer> getOverallProcessedItems();

    /**
     * Perform plan specific context unwinding - cancel reserved items.
     * @param warehouseState The state of reserved items within this warehosue.
     * @param map            The map in which the plan should be executed.
     */
    public abstract void unwindPlannedWarehouseState(Map<ItemStorageBlock, Map<Goods, Integer>> warehouseState, WarehouseMap map);
}
