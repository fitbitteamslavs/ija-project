/*
Authors: Michal Hečko (xhecko02)
Purpose: Interface for a strategy of storage block visiting.
 */
package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.map.ItemStorageBlock;
import com.ija.warehouse.business.map.WarehouseMap;

import java.util.LinkedList;
import java.util.Map;

/**
 * Interface providing an abstraction over the process of choosing particular blocks
 * which should be visited in order to satisfy some planner specific goal.
 */
public interface StorageBlockVisitStrategy {
    public class PlanningContext {
        private WarehouseMap map;
        private Map<ItemStorageBlock, Map<Goods, Integer>> actualWarehouseStorageState;
        private Map<Goods, Integer> requestSummary;
        private Forklift allocatedForklift;

        public PlanningContext(
                WarehouseMap map,
                Map<ItemStorageBlock, Map<Goods, Integer>> actualWarehouseStorageState,
                Map<Goods, Integer> requestSummary,
                Forklift allocatedForklift) {

            this.map = map;
            this.actualWarehouseStorageState = actualWarehouseStorageState;
            this.requestSummary = requestSummary;
            this.allocatedForklift = allocatedForklift;
        }

        public WarehouseMap getMap() {
            return map;
        }

        public Map<ItemStorageBlock, Map<Goods, Integer>> getActualWarehouseStorageState() {
            return actualWarehouseStorageState;
        }

        public Map<Goods, Integer> getRequestSummary() {
            return requestSummary;
        }

        public Forklift getAllocatedForklift() {
            return allocatedForklift;
        }
    }

    public class VisitedBlockDescription {
        private ItemStorageBlock storageBlock;
        private Map<Goods, Integer> retrievedItems;

        public VisitedBlockDescription(ItemStorageBlock storageBlock, Map<Goods, Integer> retrievedItems) {
            this.storageBlock = storageBlock;
            this.retrievedItems = retrievedItems;
        }

        public ItemStorageBlock getStorageBlock() {
            return storageBlock;
        }

        public Map<Goods, Integer> getRetrievedItems() {
            return retrievedItems;
        }
    }

    LinkedList<VisitedBlockDescription> createVisitOrder(PlanningContext ctx);
}
