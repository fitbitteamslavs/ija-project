/*
Authors: Michal Hečko (xhecko02)
Purpose: Interface for a plan of forklifts path when executing a request.
 */
package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.map.Coordinates;
import com.ija.warehouse.business.map.MapBlock;

import java.util.List;

/**
 * Interface providing abstraction over forklift action plan.
 */
public interface ForkliftPlan {
    ForkliftPlanStop getCurrentStop();
    Coordinates getPlanTerminalPoint();
    ForkliftPlanStop getNextStop();
    List<ForkliftPlanStop> getStops();
    int getStopCount();
    Forklift getAssociatedForklift();
    void addStop(ForkliftPlanStop stop);

    MapBlock getPort();
}
