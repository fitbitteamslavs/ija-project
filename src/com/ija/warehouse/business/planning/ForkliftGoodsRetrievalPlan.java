/*
Authors: Michal Hečko (xhecko02)
Purpose: A plan for a request execution.
 */
package com.ija.warehouse.business.planning;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.business.Request;
import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.map.ItemStorageBlock;
import com.ija.warehouse.business.map.WarehouseMap;
import com.ija.warehouse.business.utils.PlanningUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents created plan for a forklift to retrieve requested items from the warehouse.
 */
public class ForkliftGoodsRetrievalPlan extends AbstractForkliftPlan {
    public ForkliftGoodsRetrievalPlan(Forklift forForklift) {
        super(forForklift, Request.RequestType.EXPORT);
    }

    @Override
    public Map<Goods, Integer> getRemainingItemsTillCompletion() {
        return this.getRemainingStopsTillCompletion().stream()
                .filter(stop -> stop.getAction() instanceof ForkliftLoadFromStorageBlockAction)
                .map(stop -> (ForkliftLoadFromStorageBlockAction) stop.getAction())
                .map(ForkliftLoadFromStorageBlockAction::getItemsToLoad)
                .reduce(PlanningUtils::sumRequests)
                .orElse(new HashMap<>());
    }

    @Override
    public Map<Goods, Integer> getOverallProcessedItems() {
        return this.stops.stream()
                .filter(stop -> stop.getAction() instanceof ForkliftLoadFromStorageBlockAction)
                .map(stop -> (ForkliftLoadFromStorageBlockAction) stop.getAction())
                .map(ForkliftLoadFromStorageBlockAction::getItemsToLoad)
                .reduce(PlanningUtils::sumRequests)
                .orElse(new HashMap<>());
    }

    @Override
    public void unwindPlannedWarehouseState(Map<ItemStorageBlock, Map<Goods, Integer>> warehouseState, WarehouseMap map) {
        this.getRemainingStopsTillCompletion().stream()
                .filter(stop -> stop.getAction() instanceof ForkliftLoadFromStorageBlockAction)
                .forEach(stop -> {
                    var action = (ForkliftLoadFromStorageBlockAction) stop.getAction();
                    var loadedItems = action.getItemsToLoad();
                    var targetBlockState = warehouseState.get(stop.getTargetBlock());
                    PlanningUtils.sumRequestedItemsInPlace(targetBlockState, loadedItems);
                });
    }
}
