/*
Author: Michal Hečko (xhecko02)
Purpose: Implementation of a central authority that is able to provide information about used types.
 */
package com.ija.warehouse.business.map;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class BlockTypeManager {
    private static BlockTypeManager instance = null;
    private Map<String, MapBlockType> knownTypes;
    private long idCounter;

    private BlockTypeManager() {
        this.knownTypes = new HashMap<>();
        this.idCounter = 0l;
    }

    public static BlockTypeManager getInstance() {
        if (instance == null) {
            instance = new BlockTypeManager();
        }
        return instance;
    }

    public MapBlockType getType(String typeName) {
        if (knownTypes.containsKey(typeName)) {
            return knownTypes.get(typeName);
        } else {
            MapBlockType type = new MapBlockType(typeName, this.idCounter);
            this.idCounter++;
            knownTypes.put(typeName, type);
            return type;
        }
    }

    public long getHighestTypeId() {
        return this.idCounter - 1;
    }

    public void forEachTypeWithId(BiConsumer<String, Long> computeFunction) {
        this.knownTypes.forEach((typeName, type) -> computeFunction.accept(typeName, type.getId()));
    }

}
