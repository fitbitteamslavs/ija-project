/*
Author: Michal Hečko (xhecko02)
Purpose: Interface to abstract the away the capability of a block to import items into the warehouse.
*/
package com.ija.warehouse.business.map;

import com.ija.warehouse.business.goods.GoodsItem;

public interface ItemExportBlock {
    void exportItem(GoodsItem item);
    MapBlock getMapBlock();
}
