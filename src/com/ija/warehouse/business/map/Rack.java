/*
Authors: Michal Hečko (xhecko02), Peter Močáry (xmocar00), Dávid Lacko (xlacko09)
Purpose: An implementation of a ItemStorageBlock that stores item in a height specific fashion - in a rack shelves.
 */
package com.ija.warehouse.business.map;

import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.goods.GoodsItem;
import javafx.util.Pair;

import java.util.*;

public class Rack extends MapBlock implements ItemStorageBlock {
    private final int shelfCount;
    private static MapBlockType blockType = BlockTypeManager.getInstance().getType("Rack");

    private final List<ItemShelf> shelves;
    private int hitCount = 0;
    private int reservedCapacity = 0;

    /**
     * Constructs a Rack block type capable of storing items in height specific location (shelves).
     * @param coords       The coordinates of this block in the map.
     * @param shelfCount   The number of shelves that this Rack has. Must be >= 5, otherwise Runtime exception is raised.
     */
    public Rack(Coordinates coords, int shelfCount) {
        super(coords);
        this.shelves = new ArrayList<>();
        this.shelfCount = shelfCount;

        if (shelfCount < 5) {
            // This is illegal - assigment specifies the number of shelves to be at least 5.
            throw new RuntimeException("Cannot initialize a Rack with shelf count < 5");
        }

        // Create shelves
        for (int i = 0; i < shelfCount; i++) {
            shelves.add(new ItemShelf());
        }

    }

    @Override
    public void hit() {
        hitCount++;
    }

    /**
     * Returns the block type of this block family (Racks).
     */
    @Override
    public MapBlockType getBlockType() {
        return blockType;
    }

    public static MapBlockType getBlockTypeS() {
        return blockType;
    }

    /**
     * Store an item within this Rack. As the Rack storage block stores items in a height dependent location,
     * the Rack itself will determine the location for the item. To get a hint where an item will be stored, call
     * Rack::getHeightIfStored.
     * @param item Item to be stored.
     * @return True, if the operation was performed successfully. False otherwise (insufficient capacity).
     */
    @Override
    public int storeItem(GoodsItem item) {
        for (ItemShelf shelf: shelves) {
            // Try to insert the item in the shelf.
            if (shelf.addItem(item)) {
                return this.shelves.indexOf(shelf);
            }
        }
        return -1;
    }

    /**
     * Store item in a specific shelf.
     * @param item Item to be stored.
     * @param shelfLevel The shelf that should receive the item.
     * @return True, if the item could be stored in the shelf, False otherwise (insufficient shelf capacity or no such shelf).
     */
    @Override
    public int storeItem(GoodsItem item, int shelfLevel) {
        final int maxShelfLevel = shelves.size();
        final int shelfIndex = shelfLevel - 1; // Shelf levels starts at 1, convert to index starting at 0
        if (shelfLevel <= maxShelfLevel) {
            ItemShelf shelf = shelves.get(shelfIndex);
            return shelf.addItem(item) ? shelves.indexOf(shelf) : -1; // Maybe item storing failed (not enough capacity)
        } else {
            return -1;
        }
    }

    /**
     * Retrieves the information about available capacities for individual shelves.
     * @return A list, containing the available capacities for individual shelves in the same order as they are located in
     *         this rack (in the bottom-to-top order).
     */
    @Override
    public List<Integer> getAvailableCapacityByHeight() {
        List<Integer> availableCapacity = new ArrayList<>();
        for (ItemShelf s: shelves) {
            availableCapacity.add(s.getAvailableCapacity());
        }
        return availableCapacity;
    }

    /**
     * Get an item of the given goods. The choice of the item is arbitrary.
     * @param goods Goods of the item that will be retrieved (removed) from this shelf.
     * @return The GoodsItem of given goods or null if no such item is present within the rack.
     */
    @Override
    public Pair<Integer, GoodsItem> retrieveSomeItemOfGoods(Goods goods) {
        ItemShelf shelf;
        for (int i = 0; i < shelves.size();i++) {
            shelf = shelves.get(i);
            // Try to retrieve item, check whether it could retrieve any
            Optional<GoodsItem> retrievedItem = shelf.getGoodsItem(goods);
            if (retrievedItem.isPresent()) {
                return new Pair<>(i, retrievedItem.get());  // We know, that the Optional will not be empty.
            }
        }
        return null;
    }

    /**
     * Tries to remove a specific item from this rack.
     * @param item Item to be removed.
     * @return True, if the item could be removed, False otherwise.
     */
    @Override
    public boolean retrieveSpecificGoodsItem(GoodsItem item) {
        for (ItemShelf shelf: shelves) {
            if (shelf.removeSpecificItem(item)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks for a presence of some item of given goods in this rack.
     * @param goods Goods for which presence we are checking.
     * @return True, if some item of given goods is stored within this Rack, false otherwise.
     */
    @Override
    public boolean hasItemOfGoods(Goods goods) {
        for (ItemShelf shelf: shelves) {
            if (shelf.hasGoods(goods)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retrieves the given quantity of requested Goods from this Rack and returns them in a list.
     * After the operation items are not present anymore in this rack.
     *
     * Example on how to interpret the return value:
     * Returned: [[ListItem1, ListItem2], null, [ListItem3]]
     *   -> From level 0 we retrieved two items (item1 and item2)
     *   -> From level 1 we retrieved nothing
     *   -> From level 2 we retrieved one item (item3)
     *
     * @implNote Do not use this method, author himself has lost the idea about is intentions.
     * @param goods Goods of which items we request.
     * @param quantity The amount of items we request.
     * @return Returns the list of retrieved items, which are **moved out** of the Rack inventory. The returned list is
     *         a list of lists, one for each level/height from which the items were retrieved.
     *         This is best effort retrieval - if the quantity could not be satisfied, the results might be shorter.
     */
    public List<List<GoodsItem>> retrieveGoodsItems(Goods goods, int quantity) {
        // TODO: What really is the semantic of this?
        int remindingItemsToGet = quantity;

        // TODO: Maybe devise some smarter strategy how to retrieve items
        List<List<GoodsItem>> items = new ArrayList<>();
        for (ItemShelf shelf : shelves) {
            if (remindingItemsToGet == 0) break;
            int quantityInSelf = shelf.getGoodsQuantity(goods);

            // We don't wanna get more than the shelf offers or then we want to retrieve
            int retrievedQuantity = Math.min(quantityInSelf, remindingItemsToGet);

            // This returns null, when retrievedQuantity is 0
            List<GoodsItem> shelfItems = shelf.getMultipleGoodsItems(goods, retrievedQuantity);
            remindingItemsToGet -= retrievedQuantity;

            items.add(shelfItems);
        }

        return items; // Note that this request might not be fully satisfied.
    }

    /**
     * Gets the (symbolic) height of the shelf on which the item is stored.
     * This method is supposed to help with animation - so that the forklift knows how height should
     * it move its fork.
     * @implNote Note that this operation is computationally expansive. Better approach is to use retrieveGoodsItem
     *           which returns also the height of the item.
     * @param item The item which height will be returned.
     * @return The height of the item (integer >= 0), or -1, when the item is not present in this rack.
     */
    public int getItemHeightPlacement(GoodsItem item) {
        // The position in the shelves array is the shelf height placement.
        ItemShelf shelf;
        for (int level = 0; level < shelves.size(); level++) {
            shelf = shelves.get(level);
            if (shelf.containsItem(item)) return level; // Item has been found at the current level
        }
        return -1;
    }

    /**
     * Returns the information about the contents of this rack.
     * @return Contents of this rack in form of mapping of Goods, to the list of Items that belong to that Goods.
     */
    @Override
    public Map<Goods, List<GoodsItem>> getAvailableItems() {
        Map<Goods, List<GoodsItem>> rackContents = new HashMap<>();
        for (ItemShelf shelf: shelves) {
            Map<Goods, List<GoodsItem>> shelfContents = shelf.getContents();
            for (Goods goods: shelfContents.keySet()) {
                // The first occurrence of this Goods - we need to create an entry in the rackContents map
                if (!rackContents.containsKey(goods)) {
                    rackContents.put(goods, new ArrayList<>());
                }
                rackContents.get(goods).addAll(shelfContents.get(goods));
            }
        }
        return rackContents;
    }

    /**
     * Retrieves the information about the number of items of given Goods that are stored in this rack.
     * @param goods Goods which quantity shall be calculated
     * @return The number of items of given goods present in this rack.
     */
    public int getQuantityOfGoods(Goods goods) {
        // Ask every shelf whether it contains (and how much) of given goods.
        return shelves.stream()
                .mapToInt((shelf) -> shelf.getGoodsQuantity(goods))
                .sum();
    }

    /**
     * Get the number of shelves that constitute this rack.
     * @return The number of shelves.
     */
    public int getShelfCount() {
        return this.shelfCount;
    }

    /**
     * Sets the capacity of each shelf.
     * @implNote Do not use this, as it causes inconsistencies between information stored in the shelves and the rack itself.
     * @param maxCapacity New capacity for the shelves.
     * @throws RuntimeException If any of the shelves is occupied by a larger number of items than the new max capacity specified.
     */
    public void setMaxCapacityOfEachShelf(int maxCapacity) {
        for (ItemShelf shelf: shelves) {
            shelf.setMaximalCapacity(maxCapacity);
        }
    }

    /**
     * Checks whether this Rack contains items of given goods in the requested quantity.
     * @param goods The goods of which we want to check the quantity.
     * @param requiredQuantity The quantity of goods that is required.
     * @return True when the rack contains items of given goods in sufficient quantity, False otherwise.
     */
    public boolean hasGoodsWithQuantity(Goods goods, int requiredQuantity) {
        return getQuantityOfGoods(goods) >= requiredQuantity;
    }

    /**
     * Determine the storage height, should the item be stored in this rack. This information is temporary and might
     * change between after every item insertion.
     * @param item Item that would be stored.
     * @return The shelf height in which the item would be stored. If there is insufficient capacity returns -1.
     */
    public int getShelfLocationIfStored(GoodsItem item) {
        int shelfLocation = 1;
        for (ItemShelf shelf: shelves) {
            if (shelf.getAvailableCapacity() > 0) {
                return shelfLocation;
            }
        }
        return -1;
    }

    /**
     * Computes hit rates for all shelves.
     * @return List of hit percentages for all shelves
     */
    public int getHitcount() {
        return hitCount;
    }

    @Override
    public Map<Goods, Integer> describeInventory() {
        Map<Goods, Integer> inventoryDescription = new HashMap<>();
        this.getAvailableItems().forEach((goods, goodsItems) -> inventoryDescription.put(goods, goodsItems.size()));
        return inventoryDescription;
    }

    /**
     * Returns the overall available capacity in items (units).
     * @return The number of items that could be stored in this rack.
     */
    public int getAvailableCapacity() {
        var availableCapacityInShelves = this.shelves.stream().mapToInt(ItemShelf::getAvailableCapacity).sum();
        return availableCapacityInShelves - reservedCapacity;
    }

    /**
     * Get the underlying map block that should act as an ItemStorageBlock.
     * @return The underlying map block, that is capable of storing items.
     */
    @Override
    public MapBlock getMapBlock() {
        return this;
    }


    public void saveState() {
        shelves.forEach(ItemShelf::saveContentsQuantities);
    }

    public void resetState() {
        shelves.forEach(ItemShelf::restoreContentsIfSaved);
        this.hitCount = 0;
    }

    @Override
    public boolean reserveCapacity(int capacity) {
        if (this.getAvailableCapacity() >= capacity) {
            this.reservedCapacity += capacity;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean freeReservedCapacity(int capacity) {
        if (reservedCapacity >= capacity) {
            this.reservedCapacity -= capacity;
            return true;
        } else {
            return false;
        }
    }
}
