/*
Author: Michal Hečko (xhecko02)
Purpose: A class that represents a block that acts as a Road for map entities (e.g. Forklift).
 */
package com.ija.warehouse.business.map;

import java.util.HashSet;
import java.util.Set;

public class Road extends MapBlock {
    private Set<RoadDirection> directions;
    public final static MapBlockType blockType = BlockTypeManager.getInstance().getType("Road");
    private boolean containsObstacle = false;

    public Road(Coordinates coords, RoadDirection direction) {
        super(coords);

        this.directions = new HashSet<>();
        this.directions.add(direction);

    }

    public MapBlockType getBlockType() {
        return blockType;
    }

    public Road addDirection(RoadDirection d) {
        this.directions.add(d);
        return this;
    }

    public boolean canMoveToNeighbourBlockAt(Coordinates c) {
        int deltaX = c.getX() - getCoordinates().getX();
        int deltaY = c.getY() - getCoordinates().getY();
        //    N      ^Y--
        //  W C E    >X++
        //    S
        if (deltaX == 0 && deltaY == -1) return this.directions.contains(RoadDirection.N);
        else if (deltaX == 1 && deltaY == 0) return this.directions.contains(RoadDirection.E);
        else if (deltaX == 0 && deltaY == 1) return this.directions.contains(RoadDirection.S);
        else if (deltaX == -1 && deltaY == 0) return this.directions.contains(RoadDirection.W);
        return false;
    }

    public static Road withMultipleDirections(Coordinates position, RoadDirection primaryDirection, RoadDirection ...directions) {
        Road road = new Road(position, primaryDirection);
        for (RoadDirection direction : directions) {
            road.addDirection(direction);
        }

        return road;
    }

    public static Road newOmnidirectional(Coordinates at) {
        return Road.withMultipleDirections(
                at,
                RoadDirection.N,
                RoadDirection.W,
                RoadDirection.S,
                RoadDirection.E);
    }


    public void placeObstacle() {
        this.containsObstacle = true;
        this.reserve();
    }

    public void removeObstacle() {
        this.containsObstacle = false;
        this.free();
    }

    public boolean doesContainObstacle() {
        return this.containsObstacle;
    }


}
