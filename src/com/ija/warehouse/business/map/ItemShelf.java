/*
Authors: Michal Hečko (xhecko02), Dávid Lacko (xlacko09), Peter Močáry (xmocar00)
Purpose: Implementation of a single Rack shelf that holds goods items up to some max capacity. Provides most of the functionality
        exposed via the Rack block.
 */
package com.ija.warehouse.business.map;

import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.goods.GoodsItem;

import java.util.*;

public class ItemShelf {
    private HashMap<Goods, List<GoodsItem>> contents;
    private HashMap<Goods, Integer> savedContentsQuantities = null;
    private int totalCapacity;
    private int availableCapacity;
    private boolean maxCapacitySet;

    private static long totalHitCount = 0;
    private long hitCount = 0;

    private void hit() {
        totalHitCount++;
        hitCount++;
    }

    public double getHitCount() {
        if (totalHitCount != 0)
            return (double)hitCount;
        else
            return 0.;
    }

    /**
     * Create a rack shelf that can hold up to `capacity` items.
     * @param capacity The maximum number of items that this shelf can store.
     */
    public ItemShelf(int capacity) {
        totalCapacity = capacity;
        availableCapacity = capacity;
        maxCapacitySet = true;
        contents = new HashMap<>();
    }

    /**
     * Create a new rack shelf ready to be filled out, without specifying the limits on the capacity.
     * This is needed, since the shelf must be created when the map is parsed, but the max capacity limits
     * are retrieved in the Inventory parsing stage.
     */
    public ItemShelf() {
        totalCapacity = Integer.MAX_VALUE;
        availableCapacity = Integer.MAX_VALUE;
        maxCapacitySet = false;
        contents = new HashMap<>();
    }

    /**
     * Adds an item to this shelf.
     * @param item Item to be added.
     * @return True, when the item could be added to this shelf, False otherwise.
     */
    public boolean addItem(GoodsItem item) {
        // TODO: @mark(CapacityChecks)
        Goods goods = item.getGoods();

        if (getAvailableCapacity() == 0) { // If the capacity was not set, returns MAX_INT
            return false; // Item cannot be stored.
        }

        if (contents.containsKey(goods)) {
            this.contents.get(goods).add(item);
        } else {
            // This shelf does not contain any items of this type, we therefore must create the list for it.
            List<GoodsItem> itemsList = new ArrayList<>();
            itemsList.add(item);
            this.contents.put(goods, itemsList);
        }

        if (maxCapacitySet) { // Update only when we know the bounds we work in
            this.availableCapacity -= 1;
        }
        hit();
        return true;
    }

    /**
     * Retrieve the number of items of given goods stored in this shelf.
     * @param goods Goods of which items we want to be counted.
     * @return The number of items of given goods stored in this shelf.
     */
    public int getGoodsQuantity(Goods goods) {
        if (contents.containsKey(goods)) {
            return contents.get(goods).size();
        } else {
            return 0;
        }
    }

    /**
     * Checks whether this shelf contains any items of given goods.
     * @param goods Goods of which items are checked to be present in this shelf.
     * @return True if any item is stored in this self, False otherwise.
     */
    public boolean hasGoods(Goods goods) {
        return contents.containsKey(goods);
    }

    /**
     * Get the list off all the items of given goods stored in this shelf.
     * @param goods Goods of which items we want to retrieve.
     * @return List of goods items stored in this shelf that might be empty, if no items of given goods stored in this
     *         shelf.
     */
    public List<GoodsItem> getGoodsItems(Goods goods) {
        return contents.getOrDefault(goods, new ArrayList<>());
    }

    /**
     * Tries (best effort) to retrieve given quantity of goods from this shelf.
     * If the number of items within this rack is insufficient (cannot meet requested quantity),
     * less than requested count will be returned and removed from this shelf.
     * @param goods Goods of which items will be retrieved, and removed from this shelf.
     * @param quantity Quantity of goods that will be attempted to be retrieved.
     * @return List containing removed items of size as described above.
     */
    public List<GoodsItem> getMultipleGoodsItems(Goods goods, int quantity) {
        if (!contents.containsKey(goods) || quantity == 0) {
            return null;
        }

        List<GoodsItem> items = contents.get(goods);
        List<GoodsItem> retrievedItems = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            retrievedItems.add(items.remove(0));
        }

        if (items.isEmpty()) {
            contents.remove(goods);
        }

        if (maxCapacitySet) {
            availableCapacity += retrievedItems.size();
        }
        hit();
        return retrievedItems;
    }

    /**
     * Retrieve and remove an item of given goods from this shelf.
     * @param goods Goods of which item will be removed.
     * @return Optional containing the removed item or empty optional if no such item could be removed.
     */
    public Optional<GoodsItem> getGoodsItem(Goods goods) {
        if (contents.containsKey(goods)) {
            List<GoodsItem> items = contents.get(goods);
            GoodsItem someItem = items.remove(0);
            if (items.isEmpty()) {
                // We have no more items of this type, remove it from the table
                contents.remove(goods);
            }
            return Optional.of(someItem);
        } else {
            return Optional.empty();
        }
    }

    /**
     * Checks for items presence in the inventory of this shelf.
     * @param item Item in question.
     * @return True, if the item is present, false otherwise.
     */
    public boolean containsItem(GoodsItem item) {
        if (!contents.containsKey(item.getGoods())) return false;
        List<GoodsItem> itemList = contents.get(item);
        for (GoodsItem i : itemList) {
            if (item == i) {
                return true;
            }
        }
        return false;
    }

    /**
     * Remove specific item from this shelf.
     * @param item Item that will be attempted to be removed from this shelf.
     * @return True if the item was successfully removed, False otherwise.
     */
    public boolean removeSpecificItem(GoodsItem item) {
        if (!this.containsItem(item)) {
            return false;
        }
        Goods itemGoods = item.getGoods();
        List<GoodsItem> items = contents.get(itemGoods);
        items.remove(item);
        hit();
        return true;
    }

    /**
     * Retrieve the contents of this self.
     * @return Contents of this shelf.
     */
    public Map<Goods, List<GoodsItem>> getContents() {
        return contents;
    }

    /**
     * Gets the number of items that can be stored in this self without breaking the capacity limits.
     * @return The number of items that can be stored.
     */
    public int getAvailableCapacity() {
        if (this.maxCapacitySet) {
            return this.availableCapacity;
        } else {
            return Integer.MAX_VALUE;
        }
    }

    /**
     * Sets the maximal capacity for this shelf.
     * @param  newMaxCapacity The new maximal capacity for this shelf.
     * @throws RuntimeException When the new capacity would be smaller then the capacity currently occupied by the items.
     */
    public void setMaximalCapacity(int newMaxCapacity) {
        int overallUsedCapacity = this.contents.values().stream().mapToInt(List::size).sum();
        if (newMaxCapacity < overallUsedCapacity) {
            throw new RuntimeException("Cannot set the shelf capacity to be lower than the capacity currently taken.");
        }
        this.totalCapacity = newMaxCapacity;
        this.availableCapacity = newMaxCapacity - overallUsedCapacity;
        this.maxCapacitySet = true;
    }


    /**
     * Saves the current state of the shelf, by noting goods and their quantities.
     */
    public void saveContentsQuantities() {

        savedContentsQuantities = new HashMap<>();
        for (Goods goods : contents.keySet()) {
            savedContentsQuantities.put(goods, contents.get(goods).size());
        }

    }

    /**
     * Restoration of contents to the point saved. If the contents weren't saved at any point the function does nothing.
     */
    public void restoreContentsIfSaved() {

        if (savedContentsQuantities != null) {
            contents = new HashMap<>();
            for (Goods goods : savedContentsQuantities.keySet()) {
                List<GoodsItem> items = new ArrayList<>();
                for (int i = 0; i < savedContentsQuantities.get(goods); i++) {
                    items.add(goods.createNewItem());
                }
                contents.put(goods, items);
            }
        }

    }
}
