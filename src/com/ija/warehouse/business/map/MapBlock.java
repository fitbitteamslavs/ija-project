/*
Authors: Michal Hečko (xhecko02), Dávid Lacko (xlacko09)
Purpose: Abstract class that represents a single map block (e.g. Wall, Rack, etc.)
 */
package com.ija.warehouse.business.map;

public abstract class MapBlock {
    private Coordinates coordinates;

    private boolean reserved;

    public MapBlock(Coordinates coords) {
        this.coordinates = coords;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }
    public abstract MapBlockType getBlockType() ;

    public boolean isReserved() {
        return reserved;
    }

    public void reserve() {
        if (!reserved) {
            reserved = true;
        } else {
            throw new RuntimeException("This port is already reserved.");
        }
    }

    public void free() {
        if (reserved) {
            reserved = false;
        }
    }
}
