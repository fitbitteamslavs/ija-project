/*
Author: Michal Hečko (xhecko02)
Purpose: Implementation of a map block that is able to import and export items into/from the warehouse system.
 */

package com.ija.warehouse.business.map;

import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.goods.GoodsItem;

import java.util.List;
import java.util.Map;

public class InputOutputPort extends MapBlock implements ItemImportBlock, ItemExportBlock{
    private static MapBlockType blockType = BlockTypeManager.getInstance().getType("InputOutputPort");

    public InputOutputPort(Coordinates coords) {
        super(coords);
    }

    @Override
    public MapBlockType getBlockType() {
        return blockType;
    }

    @Override
    public void exportItem(GoodsItem item) {

    }

    @Override
    public Map<Goods, List<GoodsItem>> getAvailableItems() {
        return null;
    }

    @Override
    public GoodsItem retrieveItem() {
        return null;
    }

    @Override
    public MapBlock getMapBlock() {
        return this;
    }
}
