/*
Author: Michal Hečko (xhecko02)
Purpose: An implementation of a block that is capable of exporting items out of the system.
 */
package com.ija.warehouse.business.map;

import com.ija.warehouse.business.goods.GoodsItem;

public class OutputPort extends MapBlock implements ItemExportBlock {
    private static MapBlockType blockType = BlockTypeManager.getInstance().getType("OutputPort");

    public OutputPort(Coordinates coords) {
        super(coords);
    }

    @Override
    public MapBlockType getBlockType() {
        return blockType;
    }

    @Override
    public void exportItem(GoodsItem item) {

    }

    @Override
    public MapBlock getMapBlock() {
        return this;
    }
}
