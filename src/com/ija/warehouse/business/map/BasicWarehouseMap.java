/*
Authors: Michal Hečko (xhecko02), Dávid Lacko (xlacko09)
Purpose: Basic/standard implementation of the WarehouseMap interface that uses 2D array to store map blocks.
 */
package com.ija.warehouse.business.map;

import javafx.util.Pair;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BasicWarehouseMap implements WarehouseMap {
    private final Map<Coordinates, ItemStorageBlock> storageBlocks;
    private final Map<Coordinates, ItemImportBlock> itemImportBlocks;
    private final Map<Coordinates, ItemExportBlock> itemExportBlocks;
    private final int mapHeight;
    private final int mapWidth;

    private final int[][] surroundingsDiscoveryPattern;

    private final MapBlock[][] map;

    public BasicWarehouseMap(int width, int height) {
        this.mapWidth = width;
        this.mapHeight = height;
        this.map = new MapBlock[height][width];

        this.storageBlocks = new HashMap<>();
        this.itemExportBlocks = new HashMap<>();
        this.itemImportBlocks = new HashMap<>();

        surroundingsDiscoveryPattern = new int[4][2];
        // North
        surroundingsDiscoveryPattern[0][0] = 0;
        surroundingsDiscoveryPattern[0][1] = -1;

        // East
        surroundingsDiscoveryPattern[1][0] = 1;
        surroundingsDiscoveryPattern[1][1] = 0;

        // South
        surroundingsDiscoveryPattern[2][0] = 0;
        surroundingsDiscoveryPattern[2][1] = 1;

        // West
        surroundingsDiscoveryPattern[3][0] = -1;
        surroundingsDiscoveryPattern[3][1] = 0;
    }

    @Override
    public boolean placeBlock(MapBlock block) {
        Coordinates blockCoordinates = block.getCoordinates();
        if (!this.areCoordinatesInsideMap(blockCoordinates)) return false;  // Block cannot be placed.
        this.setBlockAtPosition(blockCoordinates, block);
        return true;
    }

    @Override
    public boolean placeStorageBlock(ItemStorageBlock itemStorage) {
        return this.doIfBlockCanBePlaced(
                itemStorage.getMapBlock(),
                () -> this.storageBlocks.put(itemStorage.getMapBlock().getCoordinates(), itemStorage)
        );
    }

    @Override
    public boolean placeItemImportBlock(ItemImportBlock itemImportBlock) {
        return this.doIfBlockCanBePlaced(
                itemImportBlock.getMapBlock(),
                () -> this.itemImportBlocks.put(itemImportBlock.getMapBlock().getCoordinates(), itemImportBlock)
        );
    }

    @Override
    public boolean placeItemExportBlock(ItemExportBlock block) {
        return this.doIfBlockCanBePlaced(
                block.getMapBlock(),
                () -> this.itemExportBlocks.put(block.getMapBlock().getCoordinates(), block)
        );
    }

    @Override
    public Collection<MapBlock> getSurroundingBlocks(Coordinates position) {
        if (!this.areCoordinatesInsideMap(position)) return null;

        int x = position.getX(), y = position.getY();
        int deltaX, deltaY;

        Collection<MapBlock> surroundingBlocks = new ArrayList<>();
        for (int[] relativePositions : this.surroundingsDiscoveryPattern) {
            deltaX = relativePositions[0];
            deltaY = relativePositions[1];

            Coordinates neighbourBlockCoordinates = new Coordinates(x + deltaX, y + deltaY);
            MapBlock block = this.getBlockAtPosition(neighbourBlockCoordinates);
            if (block != null) {
                surroundingBlocks.add(block);
            }
        }

        return surroundingBlocks;
    }

    @Override
    public MapBlock getBlockAtPosition(Coordinates position) {
        if (areCoordinatesInsideMap(position)) {
            return this.map[position.getY()][position.getX()];
        } else {
            return null;
        }
    }

    @Override
    public Map<Coordinates, ItemStorageBlock> getItemStorageBlocks() {
        return this.storageBlocks;
    }

    @Override
    public Map<Coordinates, ItemImportBlock> getItemImportBlocks() {
        return this.itemImportBlocks;
    }

    @Override
    public Map<Coordinates, ItemExportBlock> getItemExportBlocks() {
        return this.itemExportBlocks;
    }

    private boolean areCoordinatesInsideMap(Coordinates c) {
        int x = c.getX(), y = c.getY();
        return (x >= 0 && x < mapWidth) && (y >= 0 && y < mapHeight);
    }

    private void setBlockAtPosition(Coordinates c, MapBlock block) {
        int x = c.getX(), y = c.getY();
        this.map[y][x] = block;
    }

    private boolean doIfBlockCanBePlaced(MapBlock block, Runnable action) {
        boolean wasPlacedSuccessfully = this.placeBlock(block);
        if (wasPlacedSuccessfully) {
            action.run();
            return true;
        } else return false;
    }

    @Override
    public int getMapWidth() {
        return this.mapWidth;
    }

    @Override
    public int getMapHeight() {
        return this.mapHeight;
    }

    @Override
    public Optional<Pair<Coordinates[], MapBlock>> getClosestReachableBlockSatisfyingPredicate(Coordinates searchOrigin, Predicate<MapBlock> predicate) {
        if (!areCoordinatesInsideMap(searchOrigin)) return Optional.empty();

        Queue<Coordinates[]> workQueue = new LinkedList<>();

        MapBlock initialBlock = this.getBlockAtPosition(searchOrigin);
        if (initialBlock instanceof Road) {
            Coordinates[] initialPath = {searchOrigin};
            workQueue.add(initialPath);
        } else {
            // We were given something else - probably some kind of storage
            // In this case we need to start at surrounding paths
            var adjacentRoads = this.getSurroundingBlocks(searchOrigin).stream()
                    .filter(block -> block instanceof Road)
                    .map(mapBlock -> new Coordinates[]{mapBlock.getCoordinates()})
                    .collect(Collectors.toList());
            workQueue.addAll(adjacentRoads);
        }

        Set<Coordinates> closed = new HashSet<>();

        Coordinates[] currentPath;
        Coordinates currentPathEnd;
        Road currentBlock;
        while (!workQueue.isEmpty()) {
            currentPath = workQueue.remove();
            currentPathEnd = currentPath[currentPath.length - 1];
            currentBlock = (Road) this.getBlockAtPosition(currentPathEnd); // We append only paths composed of Road blocks

            closed.add(currentPathEnd);

            // Expand the current path end.
            for (MapBlock surroundingBlock : this.getSurroundingBlocks(currentPathEnd)) {
                if (predicate.test(surroundingBlock)) {
                    return Optional.of(new Pair<Coordinates[], MapBlock>(currentPath, surroundingBlock)); // Found the block we were looking for.
                } else {
                    boolean wasExplored = closed.contains(surroundingBlock.getCoordinates());
                    boolean isRoad = surroundingBlock instanceof Road;
                    if (isRoad){
                        if(((Road)surroundingBlock).doesContainObstacle()){
                            continue;
                        }
                    }
                    boolean canBeTravelledTo = currentBlock.canMoveToNeighbourBlockAt(surroundingBlock.getCoordinates());
                    if ((!wasExplored) && isRoad && canBeTravelledTo) {
                        Coordinates[] newPath = Arrays.copyOf(currentPath, currentPath.length + 1);
                        newPath[newPath.length - 1] = surroundingBlock.getCoordinates();

                        workQueue.add(newPath);
                    }
                }
            }
        }

        // All paths leading from start at were already explored and nothing matching predicate was located.
        return Optional.empty();
    }

    @Override
    public Optional<Coordinates[]> getPathReachingBlockSatisfyingPredicate(Coordinates searchOrigin, Coordinates targetCoords) {
        if (!areCoordinatesInsideMap(searchOrigin)) return Optional.empty();

        Queue<Coordinates[]> workQueue = new LinkedList<>();

        MapBlock initialBlock = this.getBlockAtPosition(searchOrigin);
        Coordinates[] initialPath = {searchOrigin};
        workQueue.add(initialPath);

        Set<Coordinates> closed = new HashSet<>();

        Coordinates[] currentPath;
        Coordinates currentPathEnd;
        Road currentBlock;
        while (!workQueue.isEmpty()) {
            currentPath = workQueue.remove();
            currentPathEnd = currentPath[currentPath.length - 1];
            currentBlock = (Road) this.getBlockAtPosition(currentPathEnd); // We append only paths composed of Road blocks

            if (currentPathEnd.equals(targetCoords)) {
                return Optional.of(currentPath);
            }

            closed.add(currentPathEnd);

            // Expand the current path end.
            for (MapBlock surroundingBlock : this.getSurroundingBlocks(currentPathEnd)) {
                boolean wasExplored = closed.contains(surroundingBlock.getCoordinates());
                boolean isRoad = surroundingBlock instanceof Road;
                if (isRoad) {
                    if (((Road) surroundingBlock).doesContainObstacle()) {
                        continue;
                    }
                }
                boolean canBeTravelledTo = currentBlock.canMoveToNeighbourBlockAt(surroundingBlock.getCoordinates());
                if ((!wasExplored) && isRoad && canBeTravelledTo) {
                    Coordinates[] newPath = Arrays.copyOf(currentPath, currentPath.length + 1);
                    newPath[newPath.length - 1] = surroundingBlock.getCoordinates();

                    workQueue.add(newPath);
                }
            }
        }

        // All paths leading from start at were already explored and nothing matching predicate was located.
        return Optional.empty();
    }
    /**
     * Identifies all blocks inside the warehouse satisfying the predicate that are reachable from the given position.
     * The search respects paths and so the starting position should be a path.
     *
     * @param startingPosition  The position at which the search should start.
     * @param blockPredicate    Predicate that identifies the blocks we are searching for.
     * @return A map of satisfying blocks along with the paths to them.
     */
    public Map<MapBlock, Coordinates[]> computeShortestPathsToReachableSatisfyingBlocks(
            Coordinates startingPosition,
            BiFunction<WarehouseMap, MapBlock, Boolean> blockPredicate) {

        Queue<Coordinates[]> workQueue = new LinkedList<>();
        Coordinates[] initialPath = {startingPosition};
        workQueue.add(initialPath);

        Coordinates[] currentPath;
        Coordinates currentCoors;
        Collection<MapBlock> surroundingBlocks;

        Set<Coordinates> closed = new HashSet<>();
        Map<MapBlock, Coordinates[]> discoveredStorageBlocks = new HashMap<>();
        while (!workQueue.isEmpty()) {
            currentPath = workQueue.remove();
            currentCoors = currentPath[currentPath.length - 1];
            surroundingBlocks = this.getSurroundingBlocks(currentCoors);

            closed.add(currentCoors);

            // Filter out the block to which we cannot go.
            for (MapBlock surroundingBlock : surroundingBlocks) {

                // If current block is storage.
                if (blockPredicate.apply(this, surroundingBlock)) {
                    if (!discoveredStorageBlocks.containsKey(surroundingBlock)) {
                        discoveredStorageBlocks.put(surroundingBlock, currentPath);
                    }
                }

                if (surroundingBlock instanceof Road) {
                    Road currentRoad = (Road) this.getBlockAtPosition(currentCoors);
                    boolean canMoveToNeighbourBlock = currentRoad.canMoveToNeighbourBlockAt(surroundingBlock.getCoordinates());
                    boolean blockWasVisited = closed.contains(surroundingBlock.getCoordinates());
                    boolean roadHasObstacle = ((Road) surroundingBlock).doesContainObstacle();
                    if (canMoveToNeighbourBlock && !blockWasVisited && !roadHasObstacle) {
                        // Add the path to the working queue
                        Coordinates[] pathToSurroundingBlock = new Coordinates[currentPath.length + 1];

                        // Copy shared path segment.
                        System.arraycopy(currentPath, 0, pathToSurroundingBlock, 0, currentPath.length);
                        pathToSurroundingBlock[pathToSurroundingBlock.length - 1] = surroundingBlock.getCoordinates();
                        workQueue.add(pathToSurroundingBlock);
                    }
                }
            }
        }

        return discoveredStorageBlocks;
    }


    public Map<ItemStorageBlock, Coordinates[]> computeShortestPathsToReachableStorageBlocks(Coordinates startingPosition) {
        var blocks= computeShortestPathsToReachableSatisfyingBlocks(
                startingPosition,
                (warehouseMap, mapBlock) -> this.getItemStorageBlocks().containsKey(mapBlock.getCoordinates()));

        Map<ItemStorageBlock, Coordinates[]> storageBlocks = new HashMap<>();
        blocks.forEach((mapBlock, coordinates) -> storageBlocks.put((ItemStorageBlock) mapBlock, coordinates));

        return storageBlocks;
    }



    @Override
    public Optional<Coordinates[]> findShortestPathBetweenBlocks(Coordinates origin, Coordinates destinationBlock) {
        var searchResult = this.getClosestReachableBlockSatisfyingPredicate(origin,
                block -> block.getCoordinates().equals(destinationBlock));
        return searchResult.map(Pair::getKey);
    }
}