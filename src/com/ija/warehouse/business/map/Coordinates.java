/*
Authors: Michal Hečko (xhecko02), Dávid Lacko (xlacko09), Peter Močáry (xmocar00)
Purpose: Helper class for keeping the structured information about a block position.
 */
package com.ija.warehouse.business.map;

import com.ija.warehouse.ui.CoordinatesUI;

import java.util.Objects;

public class Coordinates {
    private int x;
    private int y;

    public Coordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Coordinates copy() {
        return new Coordinates(this.x, this.y);
    }

    public CoordinatesUI toCoordinatesUI() {
        return new CoordinatesUI(this.x, this.y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinates that = (Coordinates) o;
        return x == that.x && y == that.y;
    }

    @Override
    public String toString() {
        return "(" + "x=" + x + ", y=" + y + ')';
    }

    @Override
    public int hashCode() {

        return Objects.hash(x, y);
    }
}
