/*
Authors: Peter Močáry (xmocar00), Michal Hečko (xhecko02)
Purpose: Loading a map of a warehouse represented by an XML.
 */
package com.ija.warehouse.business.map;

import com.ija.warehouse.business.Forklift;
import com.ija.warehouse.ui.elements.Road;
import com.ija.warehouse.ui.WarehouseUI;
import javafx.util.Pair;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.function.BiConsumer;

public class XMLMapLoader implements MapLoader {
    private final WarehouseUI ui;
    private WarehouseMap map;
    private LinkedList<Forklift> forklifts;

    public XMLMapLoader(WarehouseUI ui) {
        this.ui = ui;
    }

    /**
     * Parses the XML representation of the file into internal map representation and adds
     * the objects to user interface.
     * @param file The file containing XML representation of the map.
     * @return The result of the parsing process.
     */
    @Override
    public MapParsingResult loadFromFile(File file) {
        Document xmlDocument;
        try {
            xmlDocument = XMLMapLoader.getXmlDocumentFromFile(file);
        } catch (Exception e) {
            return MapParsingResult.failedWithError(e.toString());
        }
        Pair<Integer, Integer> mapDimensions = getMapSizeFromXmlDoc(xmlDocument);
        int width = mapDimensions.getKey(), height = mapDimensions.getValue();

        map = new BasicWarehouseMap(width, height);
        forklifts = new LinkedList<>();

        this.ui.setSize(width, height);

        createMapBlocksOfTypeFromXML(xmlDocument, "wall", this::createWall);
        createMapBlocksOfTypeFromXML(xmlDocument, "rack", this::createRack);
        createMapBlocksOfTypeFromXML(xmlDocument, "pathway", this::createRoad);
        createMapBlocksOfTypeFromXML(xmlDocument, "port", this::createPort);
        createMapBlocksOfTypeFromXML(xmlDocument, "cart_home", this::createForklift);

        return MapParsingResult.success(map, forklifts);
    }

    /**
     * Adds a wall to the map and user interface.
     * @param position Coordinates of the wall.
     * @param elem The wall Element from the XML map representation.
     */
    private void createWall(Coordinates position, Element elem) {
        MapBlock block = new Wall(position);
        this.map.placeBlock(block);
        ui.addWall(position.getX(), position.getY());
    }

    /**
     * Creates a Rack object instance and adds it to user interface and map.
     * @param position Coordinates of the rack.
     * @param elem The rack Element from the XML map representation.
     */
    private void createRack(Coordinates position, Element elem) {
        int height = Integer.parseInt(elem.getAttribute("height"));

        Rack rack = new Rack(position, height);
        map.placeStorageBlock(rack);

        ui.addRack(position.getX(), position.getY(), height);
    }

    /**
     * Creates a Road object instance and adds it to user interface and map.
     * @param position Coordinates of the road.
     * @param elem The road Element from the XML map representation.
     */
    private void createRoad(Coordinates position, Element elem) {
        String directionsStr = elem.getAttribute("direction");
        int directionsInt = XMLMapLoader.getDirectionsIntegerFromString(directionsStr);

        com.ija.warehouse.business.map.Road road = new com.ija.warehouse.business.map.Road(position, RoadDirection.valueOf(String.valueOf(directionsStr.charAt(0))));

        // Add all directions to the road
        for (int idx = 1; idx < directionsStr.length(); idx++) {
            String direction = String.valueOf(directionsStr.charAt(idx));
            road.addDirection(RoadDirection.valueOf(direction));
        }

        this.map.placeBlock(road);

        ui.addRoad(position.getX(), position.getY(), directionsInt);
    }

    /**
     * Creates a Port object instance and adds it to user interface and map.
     * @param position Coordinates of the port.
     * @param elem The port Element from the XML map representation.
     */
    private void createPort(Coordinates position, Element elem) {
        String typeStr = elem.getAttribute("type");
        int typeInt; // 3 types of ports as separate classes?
        if (typeStr.equals("in")) {
            InputPort port = new InputPort(position);
            this.map.placeItemImportBlock(port);
            typeInt = 1;
        }
        else if (typeStr.equals("out")) {
            OutputPort port = new OutputPort(position);
            this.map.placeItemExportBlock(port);
            typeInt = 2;
        }
        else {
            InputOutputPort port = new InputOutputPort(position);
            this.map.placeItemExportBlock(port);
            this.map.placeItemImportBlock(port);
            typeInt = 3;
        }

        ui.addPort(position.getX(), position.getY(), typeInt);
    }

    /**
     * Creates a Forklift object instance and adds it to user interface and map. Along with this adds a road to the map
     * where cart is.
     * @param position Coordinates of the forklift.
     * @param elem The forklift Element from the XML map representation.
     */
    private void createForklift(Coordinates position, Element elem) {
        int id = Integer.parseInt(elem.getAttribute("id"));

        // Create new forklift and add it to the list of warehouse forklifts.
        Forklift forklift = new Forklift(30, position, id);
        this.forklifts.add(forklift);
        ui.addForklift(position.getX(), position.getY(), id);

        // Creates a road at the position of forklifts home point.
        createRoad(position, elem);
    }

    /**
     * Creates given block type from XML representation of map. The creation is handled by a function passed as
     * a parameter.
     * @param doc DOM Document representation of the XML map.
     * @param blockTypeNameInXML The type of the blocks on wants to create.
     * @param blockCreationHandler The function specifying how the block type should be created.
     */
    private void createMapBlocksOfTypeFromXML(Document doc, String blockTypeNameInXML, BiConsumer<Coordinates, Element> blockCreationHandler) {
        NodeList list = doc.getElementsByTagName(blockTypeNameInXML);
        for (int n = 0; n < list.getLength(); n++) {
            Node node = list.item(n);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                int x = Integer.parseInt(element.getAttribute("x"));
                int y = Integer.parseInt(element.getAttribute("y"));
                Coordinates coords = new Coordinates(x, y);
                blockCreationHandler.accept(coords, element);
            }
        }
    }

    /**
     * Parses the specified file into DOM Document.
     * @param file A file one wants to parse.
     * @return DOM Document representation of the specified file.
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    private static Document getXmlDocumentFromFile(File file) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        return docBuilder.parse(file);
    }

    /**
     * Retrieves the size of a map represented in the specified Document. The dimensions of the map
     * should be stored as attributes in the "map" root Element.z
     * @param doc Document representation of warehouse map.
     * @return Width and height of the map as Pair of integers
     * @throws RuntimeException If the root Element name isn't "map".
     */
    private Pair<Integer, Integer> getMapSizeFromXmlDoc(Document doc) throws RuntimeException {
        Element mapElement = getRootElementFromXmlDoc(doc,"map");
        int width = Integer.parseInt(mapElement.getAttribute("width"));
        int height = Integer.parseInt(mapElement.getAttribute("height"));
        return new Pair<>(width, height);
    }

    /**
     * Retrieves root Element from specified Document and validates its name.
     * @param doc The Document from which one wants the root Element.
     * @param expectedName The expected name of the root Element.
     * @return The root Element of specified Document
     * @throws RuntimeException If the specified expected name doesn't match the root Element name.
     */
    private Element getRootElementFromXmlDoc(Document doc, String expectedName) throws RuntimeException {
        Element rootElement = doc.getDocumentElement();
        if (!rootElement.getNodeName().equals(expectedName)) {
            throw new RuntimeException("Expected "+ expectedName + " as a root element of XML file!");
        }
        return rootElement;
    }

    /**
     * Creates integer representation of road directions from its string representation.
     * Example: "NW" - north west --> 0101 = 5
     * @param directions Directions of a road - string containing combination of NWSE letters.
     * @return Integer representation of specified directions.
     */
    private static int getDirectionsIntegerFromString(String directions) {
        int directionsInteger = 0;
        for ( int idx = 0; idx < directions.length(); idx++) {
            String direction = String.valueOf(directions.charAt(idx));
            directionsInteger |= Road.Directions.valueOf(direction).get();
        }
        return directionsInteger;
    }
}
