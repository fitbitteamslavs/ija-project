/*
Author: Michal Hečko (xhecko02)
Purpose: A interface abstracting away implementation of the warehouse map.
 */
package com.ija.warehouse.business.map;

import javafx.util.Pair;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Predicate;

public interface WarehouseMap {
    Collection<MapBlock> getSurroundingBlocks(Coordinates position);
    MapBlock getBlockAtPosition(Coordinates position);
    Map<Coordinates, ItemStorageBlock> getItemStorageBlocks();
    Map<Coordinates, ItemImportBlock> getItemImportBlocks();
    Map<Coordinates, ItemExportBlock> getItemExportBlocks();
    boolean placeBlock(MapBlock block);
    boolean placeStorageBlock(ItemStorageBlock block);
    boolean placeItemImportBlock(ItemImportBlock block);
    boolean placeItemExportBlock(ItemExportBlock block);

    Optional<Pair<Coordinates[], MapBlock>> getClosestReachableBlockSatisfyingPredicate(Coordinates searchOrigin, Predicate<MapBlock> predicate);
    Optional<Coordinates[]> findShortestPathBetweenBlocks(Coordinates origin, Coordinates destinationBlock);
    public Map<MapBlock, Coordinates[]> computeShortestPathsToReachableSatisfyingBlocks(
            Coordinates startingPosition,
            BiFunction<WarehouseMap, MapBlock, Boolean> blockPredicate);

    Map<ItemStorageBlock, Coordinates[]> computeShortestPathsToReachableStorageBlocks(Coordinates startingPosition);
    Optional<Coordinates[]> getPathReachingBlockSatisfyingPredicate(Coordinates searchOrigin, Coordinates targetCoords);

    int getMapWidth();
    int getMapHeight();
}