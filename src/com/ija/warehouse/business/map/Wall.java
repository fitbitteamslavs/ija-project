/*
Author: Michal Hečko (xhecko02)
Purpose: A map block that represents a Wall. (what more to add :))
 */
package com.ija.warehouse.business.map;

public class Wall extends MapBlock{
    private final MapBlockType blockType = BlockTypeManager.getInstance().getType("Wall");

    public Wall(Coordinates coords) {
        super(coords);
    }

    @Override
    public MapBlockType getBlockType() {
        return this.blockType;
    }
}
