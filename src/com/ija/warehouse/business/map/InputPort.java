/*
Author: Michal Hečko (xhecko02)
Purpose: Implementation of a map block that is able to import items into the warehouse system.
*/
package com.ija.warehouse.business.map;

import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.goods.GoodsItem;

import java.util.List;
import java.util.Map;

public class InputPort extends MapBlock implements ItemImportBlock {
    private static MapBlockType blockType = BlockTypeManager.getInstance().getType("InputPort");

    public InputPort(Coordinates coords) {
        super(coords);
    }

    @Override
    public GoodsItem retrieveItem() {
        return null;
    }

    @Override
    public MapBlockType getBlockType() {
        return blockType;
    }

    @Override
    public Map<Goods, List<GoodsItem>> getAvailableItems() {
        return null;
    }

    @Override
    public MapBlock getMapBlock() {
        return this;
    }
}