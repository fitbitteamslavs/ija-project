/*
Authors: Peter Močáry (xmocar00), Michal Hečko (xhecko02)
Purpose: Loading of a inventory represented by an XML.
*/
package com.ija.warehouse.business.map;

import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.goods.GoodsItem;
import com.ija.warehouse.ui.WarehouseUI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class XMLInventoryLoader {

    Map<Integer, Stack<GoodsItem>> inventory;
    WarehouseUI ui;
    WarehouseMap map;
    int default_shelf_capacity;


    public XMLInventoryLoader(WarehouseMap map, WarehouseUI ui) {
        this.inventory = new HashMap<>();
        this.map = map;
        this.ui = ui;
        this.default_shelf_capacity = 35;
    }

    // TODO specify xml structure in documentation.
    /**
     * Loads the inventory form given XML file.
     * @param file xml file containing inventory representation
     */
    public void loadFromFile(File file) throws IOException, SAXException, ParserConfigurationException, RuntimeException {
        Document XMLInventoryDoc = getXmlDocumentFromFile(file);
        Element root = getRootElementFromXmlDoc(XMLInventoryDoc, "inventory");

        Element goodsElement = getElementFromRoot(root, "goods");
        createItemsFromXML(goodsElement);

        Element racksElement = getElementFromRoot(root, "racks");
        this.default_shelf_capacity = Integer.parseInt(racksElement.getAttribute("shelf_capacity"));
        fillMapRacksFromXML(racksElement);
    }

    /**
     * Initial parsing of the file by DOM xml parser, which creates Document from where one can easily request nodes.
     * @param file xml file containing inventory representation
     * @return Dom xml parser Document
     */
    private static Document getXmlDocumentFromFile(File file) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        return docBuilder.parse(file);
    }

    /**
     * Retrieves root element and checks its name with the specified one.
     * @param doc DOM xml parser Document representing the inventory
     * @param expectedName expected root Element name
     * @return the rootElement
     * @throws RuntimeException If expected name of the root doesn't match with the name of the root.
     */
    private Element getRootElementFromXmlDoc(Document doc, String expectedName) throws RuntimeException {
        Element rootElement = doc.getDocumentElement();
        if (!rootElement.getNodeName().equals(expectedName)) {
            throw new RuntimeException("Expected "+expectedName+" as a root element of XML file!");
        }
        return rootElement;
    }

    /**
     * Retrieves specified DOM parser Element (by its name) form root element. The root element must
     * have only one of a kind Elements as children otherwise the method throws RuntimeException!
     * @param root DOM xml parser root Element - the inventory element
     * @param elementName String containing the name of the element one wants to retrieve - "goods" or "racks"
     * @return the DOM xml parser Element, child of the root Element
     * @throws RuntimeException If there is more than one list of goods or if cast of Node to Element failed.
     */
    private Element getElementFromRoot(Element root, String elementName) throws RuntimeException {

        NodeList nodeList = root.getElementsByTagName(elementName); // TODO if not found err

        if (nodeList.getLength() != 1) {
            throw new RuntimeException("Expected just one goods element in the inventory!");
        }

        Node node = nodeList.item(0);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            return (Element) node;
        } else {
            throw new RuntimeException("Couldn't cast Node to Element!");
        }
    }

    /**
     * Crates items specified in the inventory xml file. These items are created and stored in inventory hashmap.
     * @param goodsElement DOM xml parser Element representing the tree of item_type Elements.
     */
    private void createItemsFromXML(Element goodsElement) {
        NodeList itemTypeNodes = goodsElement.getElementsByTagName("item_type");

        // Loop through every item_type xml element (children of goods element)
        for (int n = 0; n < itemTypeNodes.getLength(); n++) {
            Node node = itemTypeNodes.item(n);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element itemTypeElement = (Element) node;

                // Item type attributes:
                String goodsName = itemTypeElement.getAttribute("name");
                int goodsID = Integer.parseInt(itemTypeElement.getAttribute("id"));
                int goodsQuantity = Integer.parseInt(itemTypeElement.getAttribute("quantity"));
                String goodsManufacturer = itemTypeElement.getAttribute("manufacturer");
                String goodsDescription = itemTypeElement.getAttribute("description");

                // Create new item type
                Goods goods = new Goods(goodsName);
                goods.setDescription(goodsDescription);
                goods.setManufacturer(goodsManufacturer);

                // Create specified quantity of items for the item type
                this.inventory.put(goodsID, new Stack<>());
                for (int i = 0; i < goodsQuantity; i++) {
                    this.inventory.get(goodsID).push(goods.createNewItem());
                }
            }
        }
    }

    /**
     * Uses previously parsed inventory containing all the item types with the created items to fill up
     * the specified WarehouseMap accordingly to the representation in the inventory xml file.
     * @param racksElement Dom Element containing the tree of rack Elements from inventory xml file.
     * @throws RuntimeException If the xml representation of tack doesn't match the already imported map.
     */
    private void fillMapRacksFromXML(Element racksElement) {
        NodeList rackNodes = racksElement.getElementsByTagName("rack");

        for (int n = 0; n < rackNodes.getLength(); n++) {
            Node node = rackNodes.item(n);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element rackElem = (Element) node;

                // rack attributes
                int x = Integer.parseInt(rackElem.getAttribute("x"));
                int y = Integer.parseInt(rackElem.getAttribute("y"));
                int height = Integer.parseInt(rackElem.getAttribute("height"));

                Coordinates position = new Coordinates(x,y);
                MapBlock block =  this.map.getBlockAtPosition(position);
                if (block.getBlockType() != BlockTypeManager.getInstance().getType("Rack")) {
                    throw new RuntimeException("Map doesn't contain a rack at x: " + position.getX() + " y: " + position.getY());
                }
                Rack rack = (Rack) block;

                if (rack.getShelfCount() != height) {
                    System.out.println(rack.getShelfCount() + " " + height);
                    throw new RuntimeException("Rack (at x: " + position.getX() + " y: " + position.getY() + ") doesn't match height from inventory XML file!");
                }
                rack.setMaxCapacityOfEachShelf(this.default_shelf_capacity);

                fillRackWithItems(rackElem, rack);
            }
        }
    }

    /**
     * Fills the specified rack accordingly to the specified rack Element form xml inventory file. Takes prepared
     * created items from inventory hashmap.
     * @param rackElem DOM xml parser Element representing a rack
     * @param rack Rack that needs to be filled
     * @throws RuntimeException If a shelf has insufficient capacity.
     */
    private void fillRackWithItems(Element rackElem, Rack rack) {

        NodeList itemTypesList = rackElem.getElementsByTagName("item_type");
        for (int n = 0; n < itemTypesList.getLength() ; n++) {
            Node node = itemTypesList.item(n);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element itemTypeElem = (Element) node;

                // Attributes
                int id = Integer.parseInt(itemTypeElem.getAttribute("id"));
                int quantity = Integer.parseInt(itemTypeElem.getAttribute("quantity"));
                int shelf = Integer.parseInt(itemTypeElem.getAttribute("shelf"));



                // Fill rack
                Stack<GoodsItem> items = this.inventory.get(id);
                for (int i=0; i < quantity; i++) {
                    if (rack.storeItem(items.pop(), shelf) == -1) {
                        throw new RuntimeException("Couldn't store the item! Insufficient capacity of the shelf.");
                    }
                    Coordinates position = rack.getMapBlock().getCoordinates();
                    ui.setRackGoods(position.getX(), position.getY(), shelf-1, true);
                }
            }
        }
    }

}