/*
Authors: Michal Hečko (xhecko02), Peter Močáry (xmocar00)
Purpose: Interface abstracting away the capability of a object to import WarehouseMap from a filesystem (a file).
 */
package com.ija.warehouse.business.map;

import com.ija.warehouse.business.Forklift;

import java.io.File;
import java.util.LinkedList;
import java.util.Map;

public interface MapLoader {

    class MapParsingResult {

        public WarehouseMap map;
        public String error;
        public LinkedList<Forklift> forklifts;

        private MapParsingResult(WarehouseMap map, String error, LinkedList<Forklift> forklifts) {
            this.map = map;
            this.error = error;
            this.forklifts = forklifts;

        }

        public static MapParsingResult failedWithError(String error) {
            return new MapParsingResult(null, error, null);
        }
        public static MapParsingResult success(WarehouseMap map, LinkedList<Forklift> forklifts) {
            return new MapParsingResult(map, null, forklifts);

        }
    }

    MapParsingResult loadFromFile(File file);
}