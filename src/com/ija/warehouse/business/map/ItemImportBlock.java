/*
Author: Michal Hečko (xhecko02)
Purpose: Interface to abstract the away the capability of a block to export items from the warehouse.
*/
package com.ija.warehouse.business.map;

import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.goods.GoodsItem;

import java.util.List;
import java.util.Map;

public interface ItemImportBlock {
    Map<Goods, List<GoodsItem>> getAvailableItems();
    GoodsItem retrieveItem();

    MapBlock getMapBlock();
}
