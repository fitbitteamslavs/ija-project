/*
Authors: Michal Hečko (xhecko02), Dávid Lacko (xlacko09)
Purpose: Interface to abstract away the capability of a block to store and later retrieve items from the internal inventory.
 */
package com.ija.warehouse.business.map;

import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.goods.GoodsItem;
import javafx.util.Pair;

import java.util.List;
import java.util.Map;

public interface ItemStorageBlock {
     MapBlock getMapBlock();
     void hit();
     int storeItem(GoodsItem item);
     int storeItem(GoodsItem item, int height);
     List<Integer> getAvailableCapacityByHeight();
     int getAvailableCapacity();
     Pair<Integer, GoodsItem> retrieveSomeItemOfGoods(Goods item);
     boolean retrieveSpecificGoodsItem(GoodsItem item);
     boolean hasItemOfGoods(Goods item);
     Map<Goods, Integer> describeInventory();
     Map<Goods, List<GoodsItem>> getAvailableItems();

     boolean reserveCapacity(int capacity);
     boolean freeReservedCapacity(int capacity);
}
