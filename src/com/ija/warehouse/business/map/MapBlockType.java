/*
Author: Michal Hečko (xhecko02)
Purpose: A helper class containing information about the block type. Might be removed in the future.
 */
package com.ija.warehouse.business.map;

public class MapBlockType {
    private String typeName;
    private long id;
    public MapBlockType(String typeName, long id) {
        this.typeName = typeName;
        this.id = id;
    }

    public long getId() {
        return this.id;
    }

    public String getTypeName() {
        return this.typeName;
    }
}
