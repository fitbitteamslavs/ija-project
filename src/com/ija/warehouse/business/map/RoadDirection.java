/*
Author: Michal Hečko (xhecko02)
Purpose: Enum containing all possible travel directions when moving on a Road block.
 */
package com.ija.warehouse.business.map;

public enum RoadDirection {
    N(1),
    E(2),
    W(4),
    S(8);
    private final int dir;

    RoadDirection(int dir) {
        this.dir = dir;
    }

    public int get() {
        return this.dir;
    }

}
