/*
Author: Peter Močáry (xmocar00)
Purpose: A request from the user.
 */
package com.ija.warehouse.business;

import com.ija.warehouse.ui.ActionListenerUI;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public class Request {

    private List<RequestPart> request;
    private Request.RequestType actionType;

    public enum RequestType {
        IMPORT,
        EXPORT;

        public static RequestType convert(ActionListenerUI.RequestType tp) {
            if (tp == ActionListenerUI.RequestType.IMPORT){
                return RequestType.IMPORT;
            } else {
                return RequestType.EXPORT;
            }
        }

    }

    public Request(RequestType actionType) {
        request = new ArrayList<>();
        this.actionType = actionType;
    }

    public RequestType getActionType() {
        return actionType;
    }

    public List<RequestPart> getRequest() {
        return this.request;
    }

    public void addRequestPart(RequestPart part) {
        this.request.add(part);
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder.append("Request:\n");

        for (RequestPart part : this.request) {

            String requestedGoodsName = part.getGoods().getName();
            int requestedQuantity = part.getQuantity();

            if (this.getActionType() == RequestType.IMPORT) {
                builder.append(String.format("\t> \u001B[92mImport\u001B[0m goods: '%s' in quantity: %d.\n", requestedGoodsName, requestedQuantity));
            } else {
                builder.append(String.format("\t> \u001B[91mExport\u001B[0m goods: '%s' in quantity: %d.\n", requestedGoodsName, requestedQuantity));
            }
        }
        return builder.toString();
    }

}
