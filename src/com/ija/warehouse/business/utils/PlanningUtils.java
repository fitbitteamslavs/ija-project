/*
Author: Michal Hečko (xhecko02)
Purpose: Utilities for planning a request execution.
 */
package com.ija.warehouse.business.utils;

import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.map.*;
import com.ija.warehouse.business.planning.AbstractForkliftPlan;
import com.ija.warehouse.business.planning.ForkliftPlanStop;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class PlanningUtils {
    public static void visualizeCreatedPlan(BasicWarehouseMap map, AbstractForkliftPlan plan) {
        plan.getStops().forEach(forkliftPlanStop -> visualizePlanStop(map, forkliftPlanStop));
    }
    private static void visualizePlanStop(BasicWarehouseMap map, ForkliftPlanStop stop) {
        char[][] mapSketch = new char[map.getMapHeight()][map.getMapWidth()];

        for (int y = 0; y < map.getMapHeight(); y++) {
            for (int x = 0; x < map.getMapWidth(); x++) {
                var block = map.getBlockAtPosition(new Coordinates(x, y));
                if (block == null) continue;

                if (block instanceof Road) mapSketch[y][x] = '.';
                else if (block instanceof ItemStorageBlock) mapSketch[y][x] = 'R';
                else if (block instanceof Wall) mapSketch[y][x] = 'X';
            }
        }

        Coordinates[] path = stop.getPathFromLastStop();
        for (int i = 0; i < path.length; i++) {
            Coordinates pathCoords = path[i];
            String stepStr = Integer.toString(i);
            mapSketch[pathCoords.getY()][pathCoords.getX()] = stepStr.charAt(stepStr.length() - 1);
        }

        System.out.println("Target block: " + stop.getTargetBlock().getCoordinates());
        for (int y = 0; y < map.getMapHeight(); y++) {
            for (int x = 0; x < map.getMapWidth(); x++) {
                System.out.print(mapSketch[y][x]);
            }
            System.out.print('\n');
        }
        System.out.print('\n');
    }

    public static boolean doesPathContainPoint(Coordinates[] path, Coordinates point) {
        for (Coordinates coordinates : path) {
            if (coordinates == point) return true;
        }
        return false;
    }

    public static Optional<Map<Goods, Integer>> retrievalItemListsDifference(Map<Goods, Integer> availableItems, Map<Goods, Integer> itemsToSubtract) {
        Map<Goods, Integer> remainingGoods = new HashMap<>(availableItems);

        for (Map.Entry<Goods, Integer> subtractedEntry : itemsToSubtract.entrySet()) {
            Goods subtractedGoods = subtractedEntry.getKey();
            int subtractedQuantity = subtractedEntry.getValue();

            if (remainingGoods.containsKey(subtractedGoods)) {
                int availableQuantity = remainingGoods.get(subtractedGoods);
                int remainingQuantity = availableQuantity - subtractedQuantity;

                if (remainingQuantity > 0) {
                    remainingGoods.put(subtractedGoods, remainingQuantity);
                } else if (remainingQuantity == 0) {
                    remainingGoods.remove(subtractedGoods);
                } else {
                    // The remaining quantity is negative - we subtract more items than is available
                    return Optional.empty();
                }
            } else {
                // The subtractFrom does not contain the items we are trying to subtract
                return Optional.empty();
            }
        }

        return Optional.of(remainingGoods);
    }


    public static Map<Goods, Integer> sumRequests(Map<Goods, Integer> requestA, Map<Goods, Integer> requestB) {
        Map<Goods, Integer> sum = new HashMap<>(requestA);
        sumRequestedItemsInPlace(sum, requestB);
        return sum;
    }

    public static void sumRequestedItemsInPlace(
            Map<Goods, Integer> requestA,
            Map<Goods, Integer> requestB) {
        requestB.forEach((goods, retrievedAmount) -> {
            if (requestA.containsKey(goods)) {
                int sumQuantity = requestA.get(goods) + retrievedAmount;
                requestA.put(goods, sumQuantity);
            } else {
                requestA.put(goods, retrievedAmount);
            }
        });
    }
}
