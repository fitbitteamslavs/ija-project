/*
Authors: Michal Hečko (xhecko02), Peter Močáry (xmocar00)
Purpose: A map entity that is able to move around the map and interact with the item storage blocks.
 */
package com.ija.warehouse.business;

import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.goods.GoodsItem;
import com.ija.warehouse.business.map.Coordinates;
import com.ija.warehouse.business.map.ItemExportBlock;
import com.ija.warehouse.business.map.Rack;

import java.util.*;

public class Forklift {
    private Warehouse warehouse;
    private Map<Goods, List<GoodsItem>> items;
    private int maxItemCapacity;
    private Coordinates position;
    private Coordinates homePosition;
    private int id;

    public Forklift(int maxItemCapacity, Coordinates initialPosition, int id) {
        this.maxItemCapacity = maxItemCapacity;
        this.position = initialPosition;
        this.homePosition = initialPosition.copy();
        this.items = new HashMap<>();
        this.id = id;
    }

    public int getMaxItemCapacity() {
        return maxItemCapacity;
    }
    public int getAvailableCapacity() {
        return maxItemCapacity - items.size();
    }
    public int getCurrentItemCount() {
        return this.items.size();
    }

    public Coordinates getActualPosition() {
        return this.position;
    }
    public void setPosition(Coordinates pos) {
        this.position = pos;
    }

    public Coordinates getHomePosition() {
        return this.homePosition;
    }

    /**
     * Get all the items contained within this forklift. After item retrieval the
     * forklift inventory will be emptied as all the items were extracted.
     * @return List of all the items in forklift inventory.
     */
    public Collection<GoodsItem> moveAllItemsOut() {
        Collection<GoodsItem> allItems = new ArrayList<>();
        this.items.values().forEach(allItems::addAll);
        this.items.clear();
        return allItems;
    }

    /**
     * Retrieves the specified amount of items of given goods. Does not perform any checks
     * therefore might cause RuntimeExceptions.
     * @param goods     Goods from which items we would like to retrieve.
     * @param quantity  The quantity of goods we would like to retrieve
     * @return List of items of requested items.
     */
    public Collection<GoodsItem> getItemsOfGoods(Goods goods, int quantity) {
        Collection<GoodsItem> retrievedItems = new ArrayList<>();
        List<GoodsItem> availableItems = this.items.get(goods);

        if (availableItems != null) {
            for (int i = 0; i < quantity; i++) {
                // Remove from list end (so that the list segments will not get shifted every  time)
                retrievedItems.add(availableItems.remove(availableItems.size() - 1));
            }
            if (availableItems.isEmpty()) {
                this.items.remove(goods);
            }
        }
        return retrievedItems;
    }


    public void moveTo(int x, int y) {}
    public void retrieveItemFrom(Rack rack, GoodsItem item) {}
    public void unloadTo(ItemExportBlock consumer) {}

    public Map<Goods, List<GoodsItem>> getItems() {
        return this.items;
    }

    public Map<Goods, Integer> getInventoryDescription() {
        Map<Goods, Integer> inventoryDescription = new HashMap<>();
        this.items.forEach(((goods, goodsItems) -> inventoryDescription.put(goods, goodsItems.size())));
        return inventoryDescription;
    }

    public boolean storeItem(GoodsItem item) {
        Goods itemGoods = item.getGoods();
        if (!this.items.containsKey(itemGoods)) {
            this.items.put(itemGoods, new ArrayList<>());
        }

        return this.items.get(itemGoods).add(item);
    }

    public int getInventoryItemCount() {
        return this.items.values().stream().mapToInt(List::size).sum();
    }

    public int getID() {
        return this.id;
    }

}
