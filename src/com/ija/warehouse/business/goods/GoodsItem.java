/*
Author: Michal Hečko (xhecko02)
Purpose: A class representing one item of specific goods.
 */
package com.ija.warehouse.business.goods;

public class GoodsItem {
    private Goods goods;

    public GoodsItem(Goods goods) {
        this.goods = goods;
    }

    public Goods getGoods() {
        return goods;
    }
}

