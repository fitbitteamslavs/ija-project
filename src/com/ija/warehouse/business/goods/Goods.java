/*
Author: Michal Hečko (xhecko02)
Purpose: A class that represents one goods type in the warehouse.
 */
package com.ija.warehouse.business.goods;

import java.util.Objects;

public class Goods {
    private String name;
    private String manufacturer;
    private String description;

    public Goods(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setManufacturer(String manuf) {
        this.manufacturer = manuf;

    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void  setDescription(String descr) {
        this.description = descr;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Goods goods = (Goods) o;
        return name.equals(goods.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public GoodsItem createNewItem() {
        return new GoodsItem(this);
    }
}