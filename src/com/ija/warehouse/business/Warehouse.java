/*
Author: Michal Hečko (xhecko02), Peter Močáry (xmocar00), Dávid Lacko (xlacko09)
Purpose: A class that represents a controller for the entire warehouse. Contains information about map, forklift (or other
         entity) positions, forwards the planning for a forklift to a specific GoodsRetrievalPlanner and then manages the plan
         execution.
 */
package com.ija.warehouse.business;

import com.ija.warehouse.business.goods.Goods;
import com.ija.warehouse.business.map.*;
import com.ija.warehouse.ui.CoordinatesUI;
import com.ija.warehouse.ui.InfoGetterUI;
import com.ija.warehouse.ui.WarehouseUI;
import javafx.concurrent.Task;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Warehouse implements InfoGetterUI, Runnable {
    private WarehouseMap map;
    private LinkedList<Forklift> forklifts;
    private LinkedList<Request> requests;
    private RequestExecutioner executioner;
    private final WarehouseUI ui;
    private final File mapFile;

    public Warehouse(WarehouseUI ui, File map_file) {

        this.ui = ui;
        this.mapFile = map_file;
    }

    private void setup() throws IOException, SAXException, ParserConfigurationException, RuntimeException, InterruptedException  {
        ui.clean();
        forklifts = new LinkedList<>();
        requests = new LinkedList<>();

        // Load map from XML file
        XMLMapLoader loader = new XMLMapLoader(ui);
        MapLoader.MapParsingResult parsingResult = loader.loadFromFile(mapFile);
        if (parsingResult.error != null) {
            throw new RuntimeException("Map parsing failed! " + parsingResult.error);
        }
        this.map = parsingResult.map;
        this.forklifts = parsingResult.forklifts;

        String filePath = mapFile.getPath();
        String baseMapFile = filePath.substring(0, filePath.length() - 4);

        // Load inventory form XML file
        String inventoryPath = baseMapFile + "_inventory.xml";
        File invFile = new File(inventoryPath);
        if (invFile.exists()) {
            XMLInventoryLoader invLoader = new XMLInventoryLoader(this.map, ui);
            invLoader.loadFromFile(invFile);
        }

        // Load requests from XML file
        String requestsPath = baseMapFile + "_requests.xml";
        File reqFile = new File(requestsPath);
        if (reqFile.exists()) {
            XMLRequestLoader reqLoader = new XMLRequestLoader(this.requests);
            reqLoader.loadFromFile(reqFile);
        }

        // Now we have both racks and forklifts so we can provide information for ui
        ui.registerInfoGetter(this);

        executioner = new RequestExecutioner(map, forklifts, requests, ui);
        ui.registerActionListener(executioner);

    }

    public void dumpMapToStdOut() {
        String fmtString = " %s ";
        int mapWidth = this.map.getMapWidth();
        int mapHeight = this.map.getMapHeight();
        System.out.printf("Map dimensions: %d %d\n", mapWidth, mapHeight);

        BlockTypeManager blockManagerInstance = BlockTypeManager.getInstance();
        Map<Long, String> blockTypeToPrintedName = new HashMap<>();
        blockTypeToPrintedName.put(blockManagerInstance.getType("Rack").getId(), "\u001B[95mRACK\u001B[0m");
        blockTypeToPrintedName.put(blockManagerInstance.getType("Wall").getId(), "\u001B[97mWALL\u001B[0m");
        blockTypeToPrintedName.put(blockManagerInstance.getType("Road").getId(), "\u001B[96mROAD\u001B[0m");
        blockTypeToPrintedName.put(blockManagerInstance.getType("InputPort").getId(), "\u001B[92mPORT\u001B[0m");
        blockTypeToPrintedName.put(blockManagerInstance.getType("OutputPort").getId(), "\u001B[91mPORT\u001B[0m");
        blockTypeToPrintedName.put(blockManagerInstance.getType("InputOutputPort").getId(), "\u001B[93mPORT\u001B[0m");

        String unknownBlockTypeText = "????";
        String currentBlockDisplayStr;

        System.out.println();
        for (int y = 0; y < mapHeight; y++) {
            for (int x = 0; x < mapWidth; x++) {
                Coordinates position = new Coordinates(x,y);
                MapBlock block = this.map.getBlockAtPosition(position);
                if (block == null) {
                    // Its not a static map block, it might be a forklift, however at current the stage, we store no information on the position of forklifts.
                    currentBlockDisplayStr = "\u001B[90mCART\u001B[0m";
                } else {
                    currentBlockDisplayStr = blockTypeToPrintedName.getOrDefault(block.getBlockType().getId(), unknownBlockTypeText);
                }
                System.out.printf(fmtString, currentBlockDisplayStr);
            }
            System.out.println();
        }
//        System.out.println("Type info:");
//        BlockTypeManager.getInstance()
//                .forEachTypeWithId(
//                        (typeName, typeId) -> System.out.printf("%s: %d\n", typeName, typeId)
//                );
    }

    public void dumpRequestsQueueToStdOut() {
        System.out.println("\nRequest queue:");
        // Since the head of the queue is on index 0 - simply loop over the list
        for (Request request: this.requests){
            System.out.println("\t"+request.toString());
        }
    }

    @Override
    public Map<String, Integer> getStorageInfo(int x, int y) {
        Coordinates coords = new Coordinates(x, y);

        MapBlock block = map.getBlockAtPosition(coords);
        if (block == null) {
            System.out.println("\nblock=null");
            return null;
        }
        if (block.getBlockType() != BlockTypeManager.getInstance().getType("Rack") ) {
            System.out.println("\nblock not rack");
            return null;
        }
        Rack rck = (Rack) block;
        Map<String, Integer> map = new HashMap<>();
        rck.describeInventory().forEach((goods, count) -> map.put(goods.getName(), count));

        return map;
    }

    @Override
    public Map<String, Integer> getForkliftContent(int id) {
        for (Forklift forklift : forklifts){
            if (forklift.getID() == id) {
                Map<String, Integer> outputMap = new HashMap<>();
                if (forklift.getItems() != null) {
                    // @Refactored: by codeboy ;)
                    Map<Goods, Integer> forkliftInventoryDescription = forklift.getInventoryDescription();
                    outputMap.put("<<ID>>", forklift.getID());
                    outputMap.put(forklift.getActualPosition().toString(), null);
                    forkliftInventoryDescription.forEach((key, value) -> outputMap.put(key.getName(), value));
                }
                return outputMap;
            }
        }
        System.out.println("Forklift not found!");
        return null;

    }

    @Override
    public InfoGetterUI.ForkliftRoute getForkliftRoute(int id) {
        return executioner.getForkliftRoute(id);
    }

    @Override
    public Map<String, Integer> getGoodsInfo() {
        HashMap<String, Integer> allGoods = new HashMap<>();
        Map<Coordinates, ItemStorageBlock> storageBlocks = map.getItemStorageBlocks();
        for (ItemStorageBlock block : storageBlocks.values()) {
            for (Goods gds : block.getAvailableItems().keySet()) {
                String name = gds.getName();
                if (allGoods.containsKey(name)) {
                    allGoods.replace(name, allGoods.get(name)+block.getAvailableItems().get(gds).size());
                } else {
                    allGoods.put(name, block.getAvailableItems().get(gds).size());
                }
            }
        }
        return allGoods;
    }

    @Override
    public Map<CoordinatesUI, Integer> getHitRate() {
        Map<CoordinatesUI, Integer> rates = new HashMap<>();
        for(ItemStorageBlock block : map.getItemStorageBlocks().values()) {
            if (block.getMapBlock().getBlockType() == Rack.getBlockTypeS()) {
                Rack b = (Rack) block;
                rates.put(b.getCoordinates().toCoordinatesUI() , b.getHitcount());
            }
        }
        return rates;
    }

    @Override
    public Map<String, Integer> getRequestsInfo() {
        Map<String,Integer> map = new HashMap<>();
        System.out.println(requests.size());
        for(int i = 0; i < requests.size();i++) {
            Request request = requests.get(i);
            map.put(String.format("%d %s", i, request.getActionType() == Request.RequestType.EXPORT ? "Export" : "Import"),
                    request.getRequest().size());
        }
        System.out.println(this.requests.size());
        System.out.println(map.size());
        return map;
    }

    @Override
    public void run() {
        try {
            this.setup();
            this.dumpMapToStdOut();
            this.dumpRequestsQueueToStdOut();
            executioner.run();
        } catch(InterruptedException e) {
            System.err.println("Warehouse exited abnormally");
        } catch (IOException | ParserConfigurationException | SAXException e) {
            System.err.println("Loading map failed:");
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
