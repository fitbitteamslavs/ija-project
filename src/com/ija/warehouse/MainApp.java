/*
Author: Dávid Lacko (xlacko09)
Purpose: Launches app GUI and thread for backend
 */
package com.ija.warehouse;

import com.ija.warehouse.business.Warehouse;
import com.ija.warehouse.ui.WarehouseUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
import java.net.URL;
import java.util.concurrent.atomic.AtomicReference;

public class MainApp extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        MenuBar bar = new MenuBar();
        BorderPane masterPane = new BorderPane();
        masterPane.setTop(bar);

        Scene masterScene = new Scene(masterPane, 900,800);
        masterScene.getStylesheets().add(getClass().getResource("/styles/style.css").toExternalForm());

        URL url = getClass().getResource("/com/ija/warehouse/ui/layout.fxml").toURI().toURL();
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        BorderPane p = fxmlLoader.load();

        WarehouseUI wr = fxmlLoader.getController();
        wr.setSize(40,40);
        masterPane.setCenter(p);

        Menu men = new Menu("File");
        MenuItem exitProgram = new MenuItem("Exit");
        MenuItem openFile = new MenuItem("Open...");
        MenuItem loadCss = new MenuItem("Load Css");

        primaryStage.setOnCloseRequest(event -> {
            System.exit(0);
        });

        loadCss.setOnAction(actionEvent -> {
            masterScene.getStylesheets().clear();
            try {
                File style = new File(System.getProperty("user.dir")+"/lib/styles/style.css");
                System.out.println(style.getAbsolutePath());
                masterScene.getStylesheets().add("file:"+style.getAbsolutePath());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        men.getItems().addAll(exitProgram, openFile, loadCss);
        bar.getMenus().addAll(men, wr.getMenu());

        primaryStage.setTitle("IJA Warehouse");
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setScene(masterScene);
        primaryStage.show();

        File mapFile = new File("data/map.xml");

        AtomicReference<Thread> t = new AtomicReference<>(new Thread(new Warehouse(wr, mapFile)));
        t.get().start();

        exitProgram.setOnAction(actionEvent -> {
            System.exit(0);
        });

        openFile.setOnAction(actionEvent -> {
            System.out.println("Opening file");
            FileChooser fc = new FileChooser();
            fc.setTitle("Open XML file with map data.");
            fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Map file", "*.xml"));
            fc.setInitialDirectory(new File(System.getProperty("user.dir")));

            File file = fc.showOpenDialog(primaryStage);
            if (file != null) {
                System.out.println(file);
            } else {
                return;
            }

            t.get().interrupt();

            warehouse = new Warehouse(wr, file);
            t.set(new Thread(warehouse));
            t.get().start();
        });
    }

    public static void showAlert(Alert.AlertType type, String title, String info) {
        Alert alrt = new Alert(type);
        alrt.setHeaderText(title);
        alrt.setContentText(info);
        alrt.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private Warehouse warehouse = null;
}
