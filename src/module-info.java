import com.ija.warehouse.MainApp;

module com.ija.warehouse {

    requires transitive javafx.controls;
    requires transitive javafx.graphics;
    requires transitive javafx.fxml;
    requires jimObjModelImporterJFX;
    requires java.xml;

    exports com.ija.warehouse to javafx.graphics;
    opens com.ija.warehouse.ui to javafx.fxml;
}