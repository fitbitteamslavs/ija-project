#!/bin/bash
LOGIN=xmocar00
TMP_DIRR=/tmp
TMP_DIR="$TMP_DIRR/$LOGIN"
if [[ -d "$TMP_DIR" ]]; then
	rm -rI "$TMP_DIR"
fi

mkdir "$TMP_DIR"
cp -r src "$TMP_DIR"
cp build.xml $TMP_DIR
cp readme.txt $TMP_DIR
mkdir $TMP_DIR/lib
cp lib/get-libs.sh $TMP_DIR/lib

mkdir $TMP_DIR/dest
mkdir $TMP_DIR/build
cp -r data/ $TMP_DIR/data
mkdir $TMP_DIR/doc
#cp -r doc/architektura.pdf $TMP_DIR/doc

pushd $TMP_DIRR
zip "$LOGIN.zip" -r $LOGIN
popd
rm -r $TMP_DIR
echo "Your archive is ready at $TMP_DIRR/$LOGIN.zip"
