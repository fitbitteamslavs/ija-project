# IJA-project
Project for IJA class.
Warehouse storage management system with 3d visualization and file IO.
Authors:
    David Lacko - xlacko09
    Michal Hecko - xhecko02
    Peter Mocary - xmocar00

Uses Java 11 and openjfx.
One external library for loading .obj files.

# Running and building
    1. First go into `./lib` and run `get-libs.sh`.
    2. Run `ant run` and the program should start.
