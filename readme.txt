Project for IJA class.
Warehouse storage management system with 3d visualization and file IO.
Authors:
    David Lacko - xlacko09
    Michal Hecko - xhecko02
    Peter Mocary - xmocar00

Uses Java 11 and openjfx.
One external library for loading .obj files.
